-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 12, 2016 at 07:08 
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koppasv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `v2_admin`
--

CREATE TABLE `v2_admin` (
  `ID` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `real_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_admin`
--

INSERT INTO `v2_admin` (`ID`, `username`, `real_name`, `password`, `created_time`) VALUES
(1, 'webmaster', 'Aang Webmaster', '31d2458a0f39fcbe88699950df2cb098', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `v2_clients`
--

CREATE TABLE `v2_clients` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `address` varchar(255) NOT NULL,
  `idcard` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `created_time` datetime NOT NULL,
  `creator` varchar(100) NOT NULL,
  `type` enum('ANGGOTA','NON ANGGOTA') NOT NULL DEFAULT 'NON ANGGOTA',
  `status` int(1) NOT NULL DEFAULT '1',
  `simpanan_pokok` int(11) NOT NULL,
  `simpanan_wajib` int(11) NOT NULL,
  `simpanan_wajib_khusus` int(11) NOT NULL,
  `simpanan_sukarela` int(11) NOT NULL,
  `simpanan_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_clients`
--

INSERT INTO `v2_clients` (`ID`, `name`, `gender`, `address`, `idcard`, `phone`, `created_time`, `creator`, `type`, `status`, `simpanan_pokok`, `simpanan_wajib`, `simpanan_wajib_khusus`, `simpanan_sukarela`, `simpanan_total`) VALUES
(1, 'Aang Kunaefi', 'P', 'Cempaka Putih Tengah XXXII Jakarta Pusat', '12345678990123456', '081327084383', '2016-09-04 17:51:56', '1', 'NON ANGGOTA', 1, 0, 0, 0, 0, 0),
(2, 'Susi Susanti', 'W', 'Jl. Raya Simatupang DKI Jakarta', '0987654321098765', '08987878899', '2016-09-04 17:54:20', '1', 'ANGGOTA', 1, 5000000, 250000, 270000, 80000, 5600000),
(3, 'Nurcholis AS', 'P', 'Jakarta Selatan', '8767876545678765', '081567686888', '2016-09-12 04:42:26', '1', 'ANGGOTA', 1, 3000000, 425000, 600000, 75000, 4100000);

-- --------------------------------------------------------

--
-- Table structure for table `v2_employees`
--

CREATE TABLE `v2_employees` (
  `name` varchar(100) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `address` varchar(255) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `created_time` datetime NOT NULL,
  `creator` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_employees`
--

INSERT INTO `v2_employees` (`name`, `gender`, `address`, `nip`, `phone`, `created_time`, `creator`, `status`) VALUES
('Fulan 1', 'P', 'Jakarta Selatan', '120001', '08168948848', '2016-09-10 08:09:16', '1', 1),
('Fulanah 1', 'W', 'Depok', '120002', '081327394387', '2016-09-10 08:20:22', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `v2_menus`
--

CREATE TABLE `v2_menus` (
  `ID` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_menus`
--

INSERT INTO `v2_menus` (`ID`, `text`, `link`, `parent`, `sort`) VALUES
(1, 'Kas Besar', '', 0, 0),
(2, 'Tabungan', '', 0, 0),
(3, 'Usaha Simpan Pinjam', '', 0, 0),
(4, 'Penjualan & Jasa', '', 0, 0),
(5, 'Petty Cash', '', 0, 0),
(6, 'Simpanan', '', 3, 0),
(7, 'Pinjaman', '', 3, 0),
(8, 'Biaya-Biaya', '', 3, 0),
(9, 'Pinjaman Anggota', '', 7, 0),
(10, 'Pinjaman Umum', '', 7, 0),
(11, 'Pinjaman Karyawan', '', 7, 0),
(13, 'Data Pinjaman', '#/pinjaman_umum/data_pinjaman', 10, 0),
(14, 'Laporan', '#/pinjaman_umum/laporan', 10, 0),
(15, 'Data Karyawan', '#/pinjaman_karyawan/karyawan', 11, 0),
(16, 'Data Pinjaman', '#/pinjaman_karyawan/data_pinjaman', 11, 0),
(17, 'Laporan', '#/pinjaman_karyawan/laporan', 11, 0),
(18, 'Simpanan Anggota', '#/simpanan_anggota/data_simpanan', 6, 0),
(19, 'Transaksi Simpanan', '#/simpanan_anggota/transaksi_simpanan', 6, 0),
(20, 'Laporan', '#/simpanan_anggota/laporan', 6, 0),
(21, 'Data Pinjaman', '#/pinjaman_anggota/data_pinjaman', 9, 0),
(22, 'Laporan', '#/pinjaman_anggota/laporan', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `v2_pinjaman_anggota`
--

CREATE TABLE `v2_pinjaman_anggota` (
  `ID` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `hash_id` varchar(32) NOT NULL,
  `adm_provisi` int(11) NOT NULL,
  `agunan` int(11) NOT NULL,
  `bunga_perbulan` int(11) NOT NULL,
  `materai` int(11) NOT NULL,
  `nominal_cicilan` int(11) NOT NULL,
  `pembulatan_cicilan` int(11) NOT NULL,
  `pinjaman_diterima` int(11) NOT NULL,
  `piutang_macet` int(11) NOT NULL,
  `piutangpokok_jasa` int(11) NOT NULL,
  `pokok` int(11) NOT NULL,
  `simpanan_wajib_khusus` int(11) NOT NULL,
  `sisa` int(11) NOT NULL,
  `tempo` int(11) NOT NULL,
  `total_bunga` int(11) NOT NULL,
  `total_cicilan` int(11) NOT NULL,
  `type` enum('HARIAN','MINGGUAN','BULANAN') NOT NULL DEFAULT 'BULANAN',
  `creator` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_pinjaman_anggota`
--

INSERT INTO `v2_pinjaman_anggota` (`ID`, `client`, `hash_id`, `adm_provisi`, `agunan`, `bunga_perbulan`, `materai`, `nominal_cicilan`, `pembulatan_cicilan`, `pinjaman_diterima`, `piutang_macet`, `piutangpokok_jasa`, `pokok`, `simpanan_wajib_khusus`, `sisa`, `tempo`, `total_bunga`, `total_cicilan`, `type`, `creator`, `created_time`, `remark`) VALUES
(1, 2, '6cac88b97e2e14a56e0b2b07adfb0a4c', 50000, 0, 150000, 7000, 983333, 984000, 9793000, 50000, 11800000, 10000000, 100000, 8000, 12, 1800000, 12, 'BULANAN', 1, '2016-09-12 19:03:00', 'Pinjaman anggota pertama');

-- --------------------------------------------------------

--
-- Table structure for table `v2_pinjaman_anggota_cicilan`
--

CREATE TABLE `v2_pinjaman_anggota_cicilan` (
  `ID` int(11) NOT NULL,
  `id_pinjaman` int(11) NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `cicilan` int(11) NOT NULL,
  `status` enum('PAID','UNPAID') NOT NULL DEFAULT 'UNPAID',
  `pay_time` datetime NOT NULL,
  `pay_by` varchar(255) NOT NULL,
  `pay_amount` int(11) NOT NULL,
  `pay_user` int(11) NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_pinjaman_anggota_cicilan`
--

INSERT INTO `v2_pinjaman_anggota_cicilan` (`ID`, `id_pinjaman`, `jatuh_tempo`, `cicilan`, `status`, `pay_time`, `pay_by`, `pay_amount`, `pay_user`, `remark`) VALUES
(1, 1, '2016-10-12', 984000, 'PAID', '2016-09-12 19:06:10', 'Susi', 984000, 1, 'Test pembayaran pertama'),
(2, 1, '2016-11-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(3, 1, '2016-12-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(4, 1, '2017-01-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(5, 1, '2017-02-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(6, 1, '2017-03-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(7, 1, '2017-04-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(8, 1, '2017-05-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(9, 1, '2017-06-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(10, 1, '2017-07-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(11, 1, '2017-08-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(12, 1, '2017-09-12', 984000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `v2_pinjaman_karyawan`
--

CREATE TABLE `v2_pinjaman_karyawan` (
  `ID` int(11) NOT NULL,
  `nip` int(11) NOT NULL,
  `hash_id` varchar(32) NOT NULL,
  `adm_provisi` int(11) NOT NULL,
  `agunan` int(11) NOT NULL,
  `bunga_perbulan` int(11) NOT NULL,
  `materai` int(11) NOT NULL,
  `nominal_cicilan` int(11) NOT NULL,
  `pembulatan_cicilan` int(11) NOT NULL,
  `pinjaman_diterima` int(11) NOT NULL,
  `piutang_macet` int(11) NOT NULL,
  `piutangpokok_jasa` int(11) NOT NULL,
  `pokok` int(11) NOT NULL,
  `sisa` int(11) NOT NULL,
  `tempo` int(11) NOT NULL,
  `total_bunga` int(11) NOT NULL,
  `total_cicilan` int(11) NOT NULL,
  `type` enum('HARIAN','MINGGUAN','BULANAN') NOT NULL DEFAULT 'BULANAN',
  `creator` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_pinjaman_karyawan`
--

INSERT INTO `v2_pinjaman_karyawan` (`ID`, `nip`, `hash_id`, `adm_provisi`, `agunan`, `bunga_perbulan`, `materai`, `nominal_cicilan`, `pembulatan_cicilan`, `pinjaman_diterima`, `piutang_macet`, `piutangpokok_jasa`, `pokok`, `sisa`, `tempo`, `total_bunga`, `total_cicilan`, `type`, `creator`, `created_time`, `remark`) VALUES
(1, 120001, '44def0f082b777f4e091beeaa2cf88e6', 15000, 0, 10000, 7000, 93333, 94000, 973000, 5000, 1120000, 1000000, 8000, 12, 120000, 12, 'BULANAN', 1, '2016-09-10 09:03:31', 'Modal Usaha');

-- --------------------------------------------------------

--
-- Table structure for table `v2_pinjaman_karyawan_cicilan`
--

CREATE TABLE `v2_pinjaman_karyawan_cicilan` (
  `ID` int(11) NOT NULL,
  `id_pinjaman` int(11) NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `cicilan` int(11) NOT NULL,
  `status` enum('PAID','UNPAID') NOT NULL DEFAULT 'UNPAID',
  `pay_time` datetime NOT NULL,
  `pay_by` varchar(255) NOT NULL,
  `pay_amount` int(11) NOT NULL,
  `pay_user` int(11) NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_pinjaman_karyawan_cicilan`
--

INSERT INTO `v2_pinjaman_karyawan_cicilan` (`ID`, `id_pinjaman`, `jatuh_tempo`, `cicilan`, `status`, `pay_time`, `pay_by`, `pay_amount`, `pay_user`, `remark`) VALUES
(1, 1, '2016-10-10', 94000, 'PAID', '2016-09-10 09:55:48', 'Fulan', 94000, 1, 'Pembayaran Pertama'),
(2, 1, '2016-11-10', 94000, 'PAID', '2016-09-10 11:39:53', 'Fulan', 94000, 1, 'Testing'),
(3, 1, '2016-12-10', 94000, 'PAID', '2016-09-10 12:05:48', 'Abeng', 94000, 1, 'Bayar ke-3'),
(4, 1, '2017-01-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(5, 1, '2017-02-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(6, 1, '2017-03-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(7, 1, '2017-04-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(8, 1, '2017-05-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(9, 1, '2017-06-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(10, 1, '2017-07-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(11, 1, '2017-08-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(12, 1, '2017-09-10', 94000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `v2_pinjaman_umum`
--

CREATE TABLE `v2_pinjaman_umum` (
  `ID` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `hash_id` varchar(32) NOT NULL,
  `adm_provisi` int(11) NOT NULL,
  `agunan` int(11) NOT NULL,
  `bunga_perbulan` int(11) NOT NULL,
  `materai` int(11) NOT NULL,
  `nominal_cicilan` int(11) NOT NULL,
  `pembulatan_cicilan` int(11) NOT NULL,
  `pinjaman_diterima` int(11) NOT NULL,
  `piutang_macet` int(11) NOT NULL,
  `piutangpokok_jasa` int(11) NOT NULL,
  `pokok` int(11) NOT NULL,
  `sisa` int(11) NOT NULL,
  `tempo` int(11) NOT NULL,
  `total_bunga` int(11) NOT NULL,
  `total_cicilan` int(11) NOT NULL,
  `type` enum('HARIAN','MINGGUAN','BULANAN') NOT NULL DEFAULT 'BULANAN',
  `creator` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_pinjaman_umum`
--

INSERT INTO `v2_pinjaman_umum` (`ID`, `client`, `hash_id`, `adm_provisi`, `agunan`, `bunga_perbulan`, `materai`, `nominal_cicilan`, `pembulatan_cicilan`, `pinjaman_diterima`, `piutang_macet`, `piutangpokok_jasa`, `pokok`, `sisa`, `tempo`, `total_bunga`, `total_cicilan`, `type`, `creator`, `created_time`, `remark`) VALUES
(1, 1, '5da494d93d95f39c94d4c37237a90f69', 7500, 1, 15000, 21000, 6056, 7000, 469000, 2500, 545000, 500000, 85000, 3, 45000, 90, 'HARIAN', 1, '2016-09-05 16:16:45', 'Modal Usaha'),
(2, 1, '7367a11538a2693ed0aa1339f3517949', 52500, 0, 87500, 7000, 379167, 380000, 3423000, 17500, 4550000, 3500000, 10000, 12, 1050000, 12, 'BULANAN', 1, '2016-09-06 18:05:56', 'Modal usaha');

-- --------------------------------------------------------

--
-- Table structure for table `v2_pinjaman_umum_cicilan`
--

CREATE TABLE `v2_pinjaman_umum_cicilan` (
  `ID` int(11) NOT NULL,
  `id_pinjaman` int(11) NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `cicilan` int(11) NOT NULL,
  `status` enum('PAID','UNPAID') NOT NULL DEFAULT 'UNPAID',
  `pay_time` datetime NOT NULL,
  `pay_by` varchar(255) NOT NULL,
  `pay_amount` int(11) NOT NULL,
  `pay_user` int(11) NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_pinjaman_umum_cicilan`
--

INSERT INTO `v2_pinjaman_umum_cicilan` (`ID`, `id_pinjaman`, `jatuh_tempo`, `cicilan`, `status`, `pay_time`, `pay_by`, `pay_amount`, `pay_user`, `remark`) VALUES
(1, 1, '2016-09-06', 7000, 'PAID', '2016-09-05 16:24:33', 'Aang', 7000, 1, 'Pembayaran Pertama'),
(2, 1, '2016-09-07', 7000, 'PAID', '2016-09-10 07:27:06', 'Aang Kunaefi', 7000, 1, 'Oke'),
(3, 1, '2016-09-08', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(4, 1, '2016-09-09', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(5, 1, '2016-09-10', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(6, 1, '2016-09-11', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(7, 1, '2016-09-12', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(8, 1, '2016-09-13', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(9, 1, '2016-09-14', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(10, 1, '2016-09-15', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(11, 1, '2016-09-16', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(12, 1, '2016-09-17', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(13, 1, '2016-09-18', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(14, 1, '2016-09-19', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(15, 1, '2016-09-20', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(16, 1, '2016-09-21', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(17, 1, '2016-09-22', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(18, 1, '2016-09-23', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(19, 1, '2016-09-24', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(20, 1, '2016-09-25', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(21, 1, '2016-09-26', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(22, 1, '2016-09-27', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(23, 1, '2016-09-28', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(24, 1, '2016-09-29', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(25, 1, '2016-09-30', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(26, 1, '2016-10-01', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(27, 1, '2016-10-02', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(28, 1, '2016-10-03', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(29, 1, '2016-10-04', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(30, 1, '2016-10-05', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(31, 1, '2016-10-06', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(32, 1, '2016-10-07', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(33, 1, '2016-10-08', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(34, 1, '2016-10-09', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(35, 1, '2016-10-10', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(36, 1, '2016-10-11', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(37, 1, '2016-10-12', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(38, 1, '2016-10-13', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(39, 1, '2016-10-14', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(40, 1, '2016-10-15', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(41, 1, '2016-10-16', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(42, 1, '2016-10-17', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(43, 1, '2016-10-18', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(44, 1, '2016-10-19', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(45, 1, '2016-10-20', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(46, 1, '2016-10-21', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(47, 1, '2016-10-22', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(48, 1, '2016-10-23', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(49, 1, '2016-10-24', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(50, 1, '2016-10-25', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(51, 1, '2016-10-26', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(52, 1, '2016-10-27', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(53, 1, '2016-10-28', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(54, 1, '2016-10-29', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(55, 1, '2016-10-30', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(56, 1, '2016-10-31', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(57, 1, '2016-11-01', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(58, 1, '2016-11-02', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(59, 1, '2016-11-03', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(60, 1, '2016-11-04', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(61, 1, '2016-11-05', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(62, 1, '2016-11-06', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(63, 1, '2016-11-07', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(64, 1, '2016-11-08', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(65, 1, '2016-11-09', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(66, 1, '2016-11-10', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(67, 1, '2016-11-11', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(68, 1, '2016-11-12', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(69, 1, '2016-11-13', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(70, 1, '2016-11-14', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(71, 1, '2016-11-15', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(72, 1, '2016-11-16', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(73, 1, '2016-11-17', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(74, 1, '2016-11-18', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(75, 1, '2016-11-19', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(76, 1, '2016-11-20', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(77, 1, '2016-11-21', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(78, 1, '2016-11-22', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(79, 1, '2016-11-23', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(80, 1, '2016-11-24', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(81, 1, '2016-11-25', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(82, 1, '2016-11-26', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(83, 1, '2016-11-27', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(84, 1, '2016-11-28', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(85, 1, '2016-11-29', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(86, 1, '2016-11-30', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(87, 1, '2016-12-01', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(88, 1, '2016-12-02', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(89, 1, '2016-12-03', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(90, 1, '2016-12-04', 7000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(91, 2, '2016-10-06', 380000, 'PAID', '2016-09-06 18:07:47', 'Aang', 380000, 1, 'Pembayaran pertama'),
(92, 2, '2016-11-06', 380000, 'PAID', '2016-09-12 04:15:52', 'Aat Supriatna', 380000, 1, 'Pembayaran ke-2'),
(93, 2, '2016-12-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(94, 2, '2017-01-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(95, 2, '2017-02-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(96, 2, '2017-03-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(97, 2, '2017-04-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(98, 2, '2017-05-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(99, 2, '2017-06-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(100, 2, '2017-07-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(101, 2, '2017-08-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, ''),
(102, 2, '2017-09-06', 380000, 'UNPAID', '0000-00-00 00:00:00', '', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `v2_simpanan`
--

CREATE TABLE `v2_simpanan` (
  `ID` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `simpanan_pokok` int(11) NOT NULL,
  `simpanan_wajib` int(11) NOT NULL,
  `simpanan_wajib_khusus` int(11) NOT NULL,
  `simpanan_sukarela` int(11) NOT NULL,
  `remark` text NOT NULL,
  `creator` int(11) NOT NULL,
  `created_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_simpanan`
--

INSERT INTO `v2_simpanan` (`ID`, `client`, `simpanan_pokok`, `simpanan_wajib`, `simpanan_wajib_khusus`, `simpanan_sukarela`, `remark`, `creator`, `created_time`) VALUES
(1, 3, 1000000, 125000, 500000, 25000, 'Simpanan pertama', 1, '2016-09-12 09:33:12'),
(2, 3, 2000000, 300000, 100000, 50000, 'Simpanan kedua', 1, '2016-09-12 09:34:00'),
(3, 2, 5000000, 250000, 150000, 80000, 'Simpanan pertama Susi Susanti', 1, '2016-09-12 10:38:54'),
(6, 2, 0, 0, 100000, 0, 'SWK <strong><a class="text-orange" href="#/pinjaman_anggota/detail/6cac88b97e2e14a56e0b2b07adfb0a4c">PA0000001</a></strong>', 1, '2016-09-12 19:03:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `v2_admin`
--
ALTER TABLE `v2_admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_clients`
--
ALTER TABLE `v2_clients`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_employees`
--
ALTER TABLE `v2_employees`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `v2_menus`
--
ALTER TABLE `v2_menus`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_pinjaman_anggota`
--
ALTER TABLE `v2_pinjaman_anggota`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_pinjaman_anggota_cicilan`
--
ALTER TABLE `v2_pinjaman_anggota_cicilan`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_pinjaman_karyawan`
--
ALTER TABLE `v2_pinjaman_karyawan`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_pinjaman_karyawan_cicilan`
--
ALTER TABLE `v2_pinjaman_karyawan_cicilan`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_pinjaman_umum`
--
ALTER TABLE `v2_pinjaman_umum`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_pinjaman_umum_cicilan`
--
ALTER TABLE `v2_pinjaman_umum_cicilan`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `v2_simpanan`
--
ALTER TABLE `v2_simpanan`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `v2_admin`
--
ALTER TABLE `v2_admin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `v2_clients`
--
ALTER TABLE `v2_clients`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `v2_menus`
--
ALTER TABLE `v2_menus`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `v2_pinjaman_anggota`
--
ALTER TABLE `v2_pinjaman_anggota`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `v2_pinjaman_anggota_cicilan`
--
ALTER TABLE `v2_pinjaman_anggota_cicilan`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `v2_pinjaman_karyawan`
--
ALTER TABLE `v2_pinjaman_karyawan`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `v2_pinjaman_karyawan_cicilan`
--
ALTER TABLE `v2_pinjaman_karyawan_cicilan`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `v2_pinjaman_umum`
--
ALTER TABLE `v2_pinjaman_umum`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `v2_pinjaman_umum_cicilan`
--
ALTER TABLE `v2_pinjaman_umum_cicilan`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `v2_simpanan`
--
ALTER TABLE `v2_simpanan`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
