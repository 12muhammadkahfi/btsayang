<!DOCTYPE html>
<html lang="en">
<head>
	<title>Resend Activation</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" href="<?php echo base_url('assets/logo.png') ?>" type="image/x-icon" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<base href="<?php echo $_SERVER['SERVER_NAME'] === "localhost" ? "http://localhost/btsuici/" : "" ?>">
	<meta name='author' content='Aang Kunaefi'>
	<link href='<?php echo base_url('assets') ?>/dist/admin/adminlte.min.css' rel='stylesheet' media='screen'>
	<link href='<?php echo base_url('assets') ?>/dist/admin/lib.min.css' rel='stylesheet' media='screen'>
	<link href='<?php echo base_url('assets') ?>/dist/admin/app.min.css' rel='stylesheet' media='screen'>
	<link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css">
</head>
<body class="login-page"><div class="login-box">

	<div class="login-logo"><b>APPLICATION</b> MKTREE</div>

	<div class="login-box-body">
		<p class="login-box-msg">Resend Email Activaiton</p>
		<form action="<?php echo base_url('index.php/welcome/do_resend_mail') ?>" method="post" accept-charset="utf-8">
			<div class="form-group">
				<label for="email">Your Email</label>
				<input placeholder="Alamat email yang digunakan untuk pendaftaran" type="email" name="email" id="email"  class="form-control" />
			</div>
			<div class="row">
				<div class="col-md-12">
					<p style="color: red;">
						<?php echo $this->input->post(null,true) ? 'Email atau password yang anda masukan salah' : '' ?>
					</p>
				</div>
				<div class="col-xs-8">
					<div class="checkbox">
						<p style="padding-top: 6px;">
							<a href="<?php echo base_url() ?>">Direct Login</a>
						</p>
					</div>
				</div>
				<div class="col-xs-4">
					<button type="submit"  class="btn btn-primary btn-block btn-flat">Resend Email</button>
				</div>
			</div>
		</form>	
	</div>

</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src='<?php echo base_url('assets') ?>/dist/admin/adminlte.min.js'></script>
	<script src='<?php echo base_url('assets') ?>/dist/admin/lib.min.js'></script>
	<script src='<?php echo base_url('assets') ?>/dist/admin/app.min.js'></script>
	<script type="text/javascript" src="assets/sweetalert-master/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			<?php if($this->session->fail_resend): ?>
			swal({   title: "Gagal",   text: "<?php echo $this->session->fail_resend ?>", imageUrl: "assets/logo.png" });
			<?php endif ?>
		});
	</script>
	</body>
</html>