<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>MK TREE</title>
    <style media="all" type="text/css">
    @media all {
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    
    @media all {
      .btn-secondary a:hover {
        border-color: #34495e !important;
        color: #34495e !important;
      }
    }
    
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] h2 {
        font-size: 22px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] h3 {
        font-size: 16px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
      table[class=body] ul,
      table[class=body] ol,
      table[class=body] td,
      table[class=body] span,
      table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .wrapper,
      table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .header {
        margin-bottom: 10px !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
      table[class=body] .alert td {
        border-radius: 0 !important;
        padding: 10px !important;
      }
      table[class=body] .span-2,
      table[class=body] .span-3 {
        max-width: none !important;
        width: 100% !important;
      }
      table[class=body] .receipt {
        width: 100% !important;
      }
    }
    
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
      .ExternalClass p,
      .ExternalClass span,
      .ExternalClass font,
      .ExternalClass td,
      .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
    }
    </style>
  </head>
  <body class="" style="font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f6f6f6; margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;" width="100%" bgcolor="#f6f6f6">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto !important; max-width: 780px; padding: 10px; width: 780px;" width="580" valign="top">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 780px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <!-- <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">This is preheader text. Some clients will show this text as a preview.</span> -->
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 3px;" width="100%">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                    <tr style="background: #fcf104;">
                      <td width="48" style="width:48px;padding: 10px;">
                        <img width="100%" style="width:100%;" src="https://i.stack.imgur.com/qIXtA.png" />
                      </td>
                      <td valign="top" style="vertical-align: top;padding-top: 6px;padding-bottom: 10px;">
                          <h1 style="margin: 0px;color: #382372;">MK TREE</h1>
                          <h4 style="margin: 5px 0px 0px 0px;color: #382372;">aplication btsayang</h4>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Dear <?php echo $nama ?>,</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><?php echo $pembuka ?></p>
                        <?php if(isset($tracking)): ?>
                        <table rowspacing="0" cellpadding="0" cellspacing="0" class="table table-striped table-hover" style="border-left: 1px solid #CCC;border-top:1px solid #CCC;width:100%;">
                          <thead>
                            <tr>
                              <th style="padding: 5px;text-align: center;border-right: 1px solid #CCC;border-bottom:1px solid #CCC;">#</th>
                              <th style="padding: 5px;text-align: center;border-right: 1px solid #CCC;border-bottom:1px solid #CCC;">Tanggal</th>
                              <th style="padding: 5px;border-right: 1px solid #CCC;border-bottom:1px solid #CCC;">Keterangan</th>
                              <th style="padding: 5px;border-right: 1px solid #CCC;border-bottom:1px solid #CCC;">Author</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($tracking as $key => $value) { ?>
                            <tr>
                              <td style="padding: 5px;font-size: 12px;width: 30px;text-align: center;border-right: 1px solid #CCC;border-bottom:1px solid #CCC;">
                                <?php echo $key+1 ?>
                              </td>
                              <td style="padding: 5px;font-size: 12px;text-align: center;border-right: 1px solid #CCC;border-bottom:1px solid #CCC;"><?php echo date("d/m/Y H:i",strtotime($value->created_time)) ?></td>
                              <td style="padding: 5px;font-size: 12px;border-right: 1px solid #CCC;border-bottom:1px solid #CCC;"><?php echo $value->remark ?></td>
                              <td style="padding: 5px;font-size: 12px;border-right: 1px solid #CCC;border-bottom:1px solid #CCC;"><?php echo $value->nama_lengkap ?></td>
                            </tr>
                          <?php } ?>
                          </tbody>
                        </table>
                        <?php endif ?>
                        <br/>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><?php echo $penutup ?></p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

              <!-- END MAIN CONTENT AREA -->
              </table>

            <!-- START FOOTER -->
            <div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                <tr>
                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-top: 10px; padding-bottom: 10px; font-size: 12px; color: #999999; text-align: center;" valign="top" align="center">
                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">Ini adalah email otomatis yang dikirimkan oleh sistem.</span><br/>
                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">Mohon untuk tidak membalas email ini.</span>
                  </td>
                </tr>
              </table>
            </div>

            <!-- END FOOTER -->
            
<!-- END CENTERED WHITE CONTAINER --></div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>