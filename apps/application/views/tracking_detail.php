<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Pendaftaran Perijinan RUMIJA BBPJN6</title>
  <base href="<?php echo $_SERVER['SERVER_NAME'] === "localhost" ? "http://localhost/perijinan/" : "" ?>">
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" href="http://bbpjn6.net/rumija/apps/assets/logo.png" type="image/x-icon" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/admin/adminlte.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/sweetalert-master/dist/sweetalert.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css">
  <style type="text/css">
    .login-box, .register-box {width: auto;margin: 2% auto !important;}
  </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
  .timeline {
    padding: 0px 0px 20px 0px;
    margin-bottom: -15px !important;
  }
  .timeline:last-child {
    padding-bottom: 0px !important;
  }
  </style>
</head>
<body class="hold-transition register-page">
<form action="index.php/welcome/doregister" method="POST" role="form">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12">
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px;">
    <div class="register-logo">
      <a href=""><b>LACAK STATUS PERIJINAN</b> <?php echo $detail->ID ?></a>
    </div>
  </div>
</div>
<div class="row" style="background: #FFF;">
<div class="col-md-5 col-sm-12">
<div class="register-box">
  <div class="register-box-body">
    <h4>Detail Pengajuan</h4>
    <table style="margin-left: -5px;" class="table table-condensed">
      <tbody>
        <tr>
          <td width=35%">Nomor Pengajuan</td>
          <td align="right"><strong><?php echo $detail->ID ?></strong></td>
        </tr>
        <tr>
          <td>Nama Pemohon</td>
          <td align="right"><strong><?php echo $detail->nama_lengkap ?></strong></td>
        </tr>
        <tr>
          <td>Perihal</td>
          <td align="right"><strong><?php echo $detail->perihal ?></strong></td>
        </tr>
      </tbody>
    </table>
    <br/>
    <br/>
    <a class="btn btn-default" href="<?php echo site_url('tracking') ?>">Lacak Pengajuan Lain</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
</div>
<div class="col-md-7 col-sm-12">
<div class="register-box">
  <div class="register-box-body">
<h4>Tracking Status</h4>
<?php foreach ($tracking as $key => $track) { ?>
  <ul class="timeline" ng-repeat="track in data.tracking">
    <li class="time-label">
          <span class="bg-orange" ng-bind=""><?php echo date("d M Y",strtotime($track->tanggal)) ?></span>
      </li>
      <li>
          <i class="fa bg-blue"><strong ng-bind="$index+1"><?php echo $key+1 ?></strong></i>
          <div class="timeline-item">
              <span class="time" class="text-orange"><i class="fa fa-clock-o text-orange"></i> <span class="text-orange" ng-bind="mytime(track.created_time)"><?php echo date("H:i",strtotime($track->created_time)) ?></span></span>
              <h3 class="timeline-header"><a ng-click="userdetail(track.creator)" href="javascript:void(0)" class="text-blue" ng-bind="track.nama_lengkap"><?php echo $track->nama_lengkap ?></a> </h3>
              <div class="timeline-body" ng-bind-html="toHtml(track.remark)"><?php echo $track->remark ?></div>
          </div>
      </li>
  </ul>
<?php } ?>
</div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/sweetalert-master/dist/sweetalert.min.js') ?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    $("select[name=tipe_organisasi]").change(function(){
      var tipe_organisasi = $(this).val();
      if(tipe_organisasi==="PERORANGAN"){
        $(".organisasi").removeAttr("required").attr("disabled","disabled").css("color","transparent");
      } else {
        $(".organisasi").removeAttr("disabled").attr("required","required").removeAttr("style");
      }
    });
    <?php if(isset($post->tipe_organisasi)): ?>
    $("select[name=tipe_organisasi]").val('<?php echo $post->tipe_organisasi ?>');
    $("select[name=tipe_organisasi]").trigger('change');
    <?php endif ?>
    <?php if($this->session->fail_registration): ?>
    swal({   title: "Registrasi Gagal",   text: "<?php echo $this->session->fail_registration ?>",   imageUrl: "/app_rumija/apps/assets/logo.png" });
    <?php endif ?>
  });
</script>
</div>
</div>
</div>
</div>

</body>
</form>
</html>