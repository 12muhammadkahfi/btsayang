<!DOCTYPE html>
<html lang="en" ng-app="koppas">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<link rel="icon" href="<?php echo base_url('assets/logo.png') ?>" type="image/x-icon" />
	<!-- <base href="http://52.76.231.175/koppas/admin/" /> -->
	<title ng-bind="page_title ? page_title : 'Home'"></title>
	<meta name='author' content='Fihsar'>
	<script type="text/javascript" src="<?php echo base_url('assets')?>/ckeditor/ckeditor.js"></script>

	<link href='<?php echo base_url('assets') ?>/dist/admin/adminlte.min.css' rel='stylesheet' media='screen'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		.btn-warning.hover, .btn-warning:active, .btn-warning:hover{
			background-color: blue !important;
		}
	</style>
	<style type="text/css">
		.skin-red .sidebar a {
		    color: #ffffff;
		}
		.skin-red .sidebar-menu>li.active>a,.skin-red .sidebar-menu>li:hover>a{
			border-left-color:#efb803;
		}c
		._720kb-datepicker-calendar-header:nth-child(odd) {
		    background: #17169e !important;
		}
		._720kb-datepicker-calendar-header a, ._720kb-datepicker-calendar-header a:hover {
			color: rgb(252, 241, 4) !important;
			font-weight: bolder;
		}
		._720kb-datepicker-calendar-month span {
		    font-size: 15px !important;
		    color: rgb(252, 241, 4) !important;
		}
		._720kb-datepicker-calendar-header:nth-child(even) {
		    background: #0f0eb7 !important;
		}
		._720kb-datepicker-calendar-days-header {
		    background: #0f0eb7 !important;
		}
		._720kb-datepicker-calendar-days-header div {
			color: rgb(252, 241, 4) !important;
		}
		._720kb-datepicker-calendar {
			padding: 3px !important;
		}
		.dataTables_length {
			float: left !important;
		}
		.dataTables_filter {
			float: right !important;
		}
	</style>
</head>
<body class="skin-red">
<div class="wrapper">

	<header class="main-header">
		<a style="background: #02018d;" href="" class="logo"><b>MKTREE</b> SOURCE</a>
		<nav style="background: #17169e;" class="navbar navbar-static-top navbar-static-top" role="navigation">
			<a href="javascript:void(0)" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li class="user user-menu">
						<a href="#/profil" >
							<span class="hidden-xs"><i class="fa fa-user"></i> <?php echo $this->session->nama_lengkap?>
							</span>
						</a>
						
					</li>
					<li class="user">
						<a href="index.php/welcome/logout">
							<span class="hidden-xs arf"><i class="fa fa-user"></i> Keluar 
							</span>
						</a>
					</li>
				</ul>
			</div>
			
		</nav>
	</header>
	<aside class="main-sidebar">
		<section class="sidebar">
			<div class="user-panel" style="height:65px">
				<div class="pull-left info" style="left:5px">
					<p><?php echo $this->session->nama_lengkap ?></p>
					<!-- <a href="panel/account"><i class="fa fa-circle text-success"></i> Online</a> -->
				</div>
			</div>
			<ul class="sidebar-menu">
				<li class="header">MAIN NAVIGATION</li>
				<?php foreach ($this->aang->menus() as $key => $menu) { ?>
				<li class="<?php echo isset($menu->sub) ? 'treeview' : '' ?> {{page_location == <?php echo $key ?> ? 'active' : ''}}">
					<a ng-click="page_location=<?php echo $key ?>" href='<?php echo $menu->link ? $menu->link : 'javascript:void(0)' ?>'>
						<?php if(isset($menu->sub)): ?><i style="margin-top: 3px;" class='fa fa-angle-left pull-right'></i><?php endif ?>
						<i class='fa fa-folder'></i> <?php echo $menu->text ?>
					</a>
					<?php if(isset($menu->sub)){ ?>
					<ul class="treeview-menu">
						<?php foreach ($menu->sub as $key1 => $menu1) { ?>
							<li>
								<a href="<?php echo $menu1->link ? $menu1->link : 'javascript:void(0)' ?>">
									<i class="fa fa-circle-o"></i> <?php echo $menu1->text ?>
									<?php if(isset($menu1->sub)): ?><i class='fa fa-angle-left pull-right'></i><?php endif ?>
								</a>
								<?php if(isset($menu1->sub)){ ?>
								<ul class="treeview-menu">
									<?php foreach ($menu1->sub as $key2 => $menu2) { ?>
										<li>
											<a href="<?php echo $menu2->link ? $menu2->link : 'javascript:void(0)' ?>">
												<i class="fa fa-circle-o"></i> <?php echo $menu2->text ?>
												<?php if(isset($menu2->sub)): ?><i class='fa fa-angle-left pull-right'></i><?php endif ?>
											</a>
											<?php if(isset($menu2->sub)){ ?>
											<ul class="treeview-menu">
												<?php foreach ($menu2->sub as $key3 => $menu3) { ?>
													<li>
														<a href="<?php echo $menu3->link ? $menu3->link : 'javascript:void(0)' ?>">
															<i class="fa fa-circle-o"></i> <?php echo $menu3->text ?>
															<?php if(isset($menu3->sub)): ?><i class='fa fa-angle-left pull-right'></i><?php endif ?>
														</a>
													</li>
												<?php } ?>
											</ul>
											<?php } ?>
										</li>
									<?php } ?>
								</ul>
								<?php } ?>
							</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</li>
				<?php } ?>
				<li>
					<a href=""></a>
				</li>
			</ul>		
		</section>
	</aside>

		<div class="content-wrapper">
			<!-- <pre> -->
				<script type="text/javascript">var mysession = <?php echo json_encode($this->session->userdata) ?></script>
			<!-- </pre> -->
			<ng-view></ng-view>
		</div>

		<footer class="main-footer">
		<div class="pull-right hidden-xs">
			MKTREE SOURCE
		</div>
		<strong>&copy; <?php echo date('Y');?></strong> All rights reserved.
		</footer>
</div>
		<div ng-show="NowLoading" id="loader"></div>

		
		<link href='<?php echo base_url('assets') ?>/dist/admin/lib.min.css' rel='stylesheet' media='screen'>
		<link href='<?php echo base_url('assets') ?>/dist/admin/app.min.css' rel='stylesheet' media='screen'>

		
		<link href='<?php echo base_url('assets') ?>/koppas.css' rel='stylesheet' media='screen'>
		<link href='<?php echo base_url('assets') ?>/angular-datepicker-master/dist/angular-datepicker.min.css' rel='stylesheet' media='screen'>
		<script src='<?php echo base_url('assets') ?>/dist/admin/adminlte.min.js'></script>
		<script src='<?php echo base_url('assets') ?>/dist/admin/lib.min.js'></script>
		<script src='<?php echo base_url('assets') ?>/dist/admin/app.min.js'></script>
				
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/masonry/3.1.5/masonry.pkgd.min.js"></script>
		<script type="text/javascript" src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
		<script type="text/javascript" src='https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js'></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
		<?php if($_SERVER['SERVER_NAME']!=="localhost"): ?>
		<?php endif ?>
		<script type="text/javascript" src="<?php echo base_url('assets/Chart.min.js') ?>"></script>
		<script src='<?php echo base_url('assets') ?>/angular.min.js'></script>
		<script src='<?php echo base_url('assets/controllers/ab.js')?>'></script>
		<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/angular-resource/1.5.8/angular-resource.min.js'></script>
		<script type="text/javascript" src="<?php echo base_url('assets/angular-chart.js') ?>"></script>
		<script src='<?php echo base_url('assets') ?>/angular-route.min.js'></script>
		<script src='<?php echo base_url('assets') ?>/ui-bootstrap-tpls-1.3.3.min.js'></script>
		<script type="text/javascript" src="assets/angular-datepicker-master/dist/angular-datepicker.min.js"></script>
		<script type="text/javascript" src="assets/ng-file-upload-master/dist/ng-file-upload-shim.min.js"></script>
		<script type="text/javascript" src="assets/ng-file-upload-master/dist/ng-file-upload.min.js"></script>
		<script type="text/javascript" src="angular-datatables-master/dist/angular-datatables.min.js"></script>

		<script src='<?php echo base_url('assets') ?>/koppas.js'></script>
		<script src='<?php echo base_url('assets') ?>/simpleToastMessage.js'></script><?php
			$path  = 'assets/controllers';
			$_files = scandir($path);
			foreach ($_files as $file) {
				$extension = pathinfo($file, PATHINFO_EXTENSION);
				if(trim($extension)==="js"){
					echo "\n<script type='text/javascript' src='".base_url("assets/controllers/{$file}")."'></script>";
				}
			}
		?>
		
		<script type="text/javascript">
			function base_url(uri){ return "<?php echo base_url() ?>"+uri; }
			function site_url(uri){ return "<?php echo site_url() ?>/"+uri; }
		</script>
		
		<!-- <script type="text/javascript">
		
	var fetch = angular.module('fetch', []);

	fetch.controller('dbCtrl', ['$scope', '$http', function ($scope, $http) {
		$http.get("<?php base_url()?>/welcome/count_masuk")
			.success(function(data){
				$scope.data = data;
			})
			.error(function() {
				$scope.data = "error in fetching data";
			});
	}]);

		</script>  -->

	</body>
</html>