
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Tanda Terima</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Theme style -->
  <link href='<?php echo base_url('assets') ?>/dist/admin/adminlte.min.css' rel='stylesheet'>
  <link href='<?php echo base_url('assets') ?>/dist/admin/adminlte.min.css' rel='stylesheet' media='print'>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    #header {
      /*background: url(<?php echo base_url('assets/logo.png') ?>) no-repeat left center; background-size: auto 100%;*/
    }
    #logonya {
      float: left;
      width: 64px;
    }
    #qrnya {
      float: right;
      width: 128px;
      margin-bottom: -108px;
    }
    .invoice {
      width: 680px;
      margin: 0px auto;
    }
    @media print {
      #qrnya {
        float: right;
        width: 128px;
        margin-bottom: -108px;
      }
      #header {
        /*background: url(<?php echo base_url('assets/logo.png') ?>) no-repeat left center; background-size: auto 100%;*/
      }
      #logonya {
        float: left;
        width: 64px;
      }
      .invoice {
        width: 100%;
        margin: 0px auto;
      }
      a {
        display: none !important;
      }
    }
  </style>
</head>
<!-- <body> -->
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div>
      <table width="100%">
        <tr>
          <td id="header">
            <img id="logonya" src="<?php echo base_url('assets/logo.png') ?>" />
            <center>
              <h3 style="margin: 0px;">Kementerian PUPR</h3>
              <h2 style="margin: 0px;">BBPJN6</h2>
            </center>
          </td>
        </tr>
      </table>
      <hr/>
      <img id="qrnya" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?php echo urlencode("{$detail->ID}\n{$detail->perihal}") ?>" />
      <table width="100%">
        <tbody>
          <tr>
            <td width="20%">Nomor Pengajuan</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->ID : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Nama Pemohon</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->nama_lengkap : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Nomor Surat</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->nomor_surat : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Tanggal Surat</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? date("d/m/Y",strtotime($detail->tanggal_surat)) : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Perihal</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->perihal : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Jenis Permohonan</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->nama_tipe : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Ruas Jalan</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->nama_ruas_jalan : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Provinsi</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->nama_provinsi : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Contact Person</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->contact_person : 'Lorem ipsum' ?></th>
          </tr>
          <tr>
            <td width="20%">Nomor Handphone</td>
            <td width="20" align="center">:</td>
            <th><?php echo isset($detail) ? $detail->telp : 'Lorem ipsum' ?></th>
          </tr>
        </tbody>
      </table>
      <br/>
      <hr/>
      <table width="100%">
        <tbody>
          <tr>
            <td width="33%" align="left">
              <table width="100%">
                <tr>
                  <td>Tanggal Pengajuan</td>
                </tr>
                <tr>
                  <th><?php echo date("d/m/Y",strtotime($detail->tanggal_data)) ?></th>
                </tr>
              </table>
            </td>
            <td valign="top" width="34%" align="center" style="text-align: center;">
              <table width="100%">
                <tr>
                  <td valign="top">Hardcopy Diterima</td>
                </tr>
                <tr>
                  <!-- <th style="text-align: center;">Lorem Ipsum</th> -->
                </tr>
              </table>
            </td>
            <td width="33%" align="right" style="text-align: right;">
              <table width="100%">
                <tr>
                  <td>TTD Pemohon</td>
                </tr>
                <tr>
                  <th style="text-align: right;"><?php echo $detail->nama_lengkap ?></th>
                </tr>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
      <br/>
      <br/>
      <br/>
      <a onclick="window.print()" class="btn btn-primary" href="#">Cetak</a>
      <a class="btn btn-danger" href="<?php echo base_url("#/data_perijinan/detail/{$this->uri->segment(2)}") ?>">Kembali</a>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
