<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<base href="<?php echo $_SERVER['SERVER_NAME'] === "localhost" ? "http://localhost/perijinan/" : "" ?>">
	<link rel="icon" href="http://bbpjn6.net/rumija/apps/assets/logo.png" type="image/x-icon" />
	<title>Tracking</title>

	<meta name='author' content='Aang Kunaefi'>
	<link href='<?php echo base_url('assets') ?>/dist/admin/adminlte.min.css' rel='stylesheet' media='screen'>
	<link href='<?php echo base_url('assets') ?>/dist/admin/lib.min.css' rel='stylesheet' media='screen'>
	<link href='<?php echo base_url('assets') ?>/dist/admin/app.min.css' rel='stylesheet' media='screen'>
	<link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css">
</head>
<body class="login-page">
	<div class="login-box">

	<div class="login-logo"><b>RUMIJA</b> BBPJN6</div>

	<div class="login-box-body">
		<p class="login-box-msg">Lacak status perijinan</p>
		<form action="" method="post" accept-charset="utf-8">
			<div class="form-group">
				<label for="ID">Masukan Nomor Pengajuan</label>
				<input required="" type="text" name="ID" id="ID"  class="form-control" />
			</div>
			<div class="form-group">
				<label for="captcha">Captcha</label>
				<input required="" type="number" name="captcha" id="captcha"  class="form-control" />
			</div>
			<div class="form-group">
				<div style="display: inline-block;vertical-align: bottom;">
					<span id="captchacontainer"><?php echo $image ?></span>
					<img title="Minta kode captcha baru" onclick="recaptcha()" style="cursor: pointer;margin-left: 10px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAHv0lEQVRYR8WXaWxVxxXH/zN3eYufn5+NAbMZDAVqtlYJCQGbgBOlrYKSFlJREswiRELTRUoVqSJItOn2oR/SRomalqYQ40JSUqBNQ5qCmqZgm62OGwiQJmy2weDl2eZtfnedU821LzwItpH6oVc6uvv8f3PmzJkzDP/ng92J/qFtmKEG1BUq2BKX3EnCpXzG4AAsq3B23hbUwBzxdiyC+pnLYQ3VZkMNHqtYiz3+N0MC1NdgpqJqWwOh6OdLyr4QiRSNV7RAEZiaBxDgug7M7DUYyStuvO1MX6bnghCgrTDxUsV6tNwK0lCDLxGwv3ItrusOCtBQq6xVNPXlstkP5sVGTWXyFxIOQA4AF0QEMA5AB+P5gDICrm2gp+240/bxfssV1k5LxaaqJxCXIIdqUKEp6gHXdcLDAtTXYpmmh38/Y/7ysB4uAEgK5jiNHBBZIAlDAsCAsSi4PhnEY4hf3G9fOv0nA4I2EOFjrqt15ZUbI6fe/ymGBDi2HSMEV86XVywvCOePBoFBOC5Mow+u44DxAHS9AFq4GEQ2IJIQThygDIiEZ9IjSmgeHDONs8d+2ZdJXA3PrPoR8ooqcXx31dAAh2v5xlGlM35YOvOhYKLzPF0+d0wY6W4OohvDRSSIq3Zs5HSzeOLcSP6IL3IGE8L8FK7TArIvwbXbPQg9ugKuCEJh18AD5Ti+e/HQAEd2aBfK71tW1na2yU31XkRxiaOQgNl5FW2C8MTC1TgmB6NxCwrMAOYpKl9PpCwpmVLJiyctDCrogpXeB3J7QeSCq2OgF6yB0fMyIhP+jOO7KwcHaNyCsBMJdBWPn6N3X2nC5FmuCgH73EdIkYGpC9aj53ZTrG4nCpngTyuMPzdx1iPBwgkPqU7yj7CzRzwIGUOMhxGd/C8c3VVOlWsgo9c7bpoFctpxLXJKgSPKZhhnFR091+LQO1uRnr8Ki4fLGfVbMVbR2atauOzhmQ+8CLI+QDb+M2/myLiITjmJY7vKjIrVCN0W4PBrmC0YTkZi+LR0CvYTQ6LnKsbF29E6fxWeHw5Avj+8nW8OREs3z6r6hWb2vgQ7c8ibJRKgYOpZHH2jZHCAg6+iXNFwpqQUP4mVoIkEEn29UC6fB69YiwPDAdTX4pvh/HEvlC/6edhJvg4rc8Bzv8xajEcRm9aKI6/HBgdo3ALNCMDKK8K9o8fi9Ni56BtO1H/fsB1f1UMj/zDnKzuCXJEelolDBXgQjIXAlBiYUoTDO4JG5WBDIBs7uA0TFq3DpTsVlt81bMMDxPHenf4zZCKqrwFxzrN+Y0SkEpF2u8aFwKj716HrToXffBPKaAOli1bj4qCLkQSoWCMAmeXkgkeWl+VkJMusaKZO4MRfv54UZN29YBXODSX+/q8Qqfo20v43dduxlBH2DuuBitUpiNSvATJvtK/fBeHm4d/7Hs2YdvJBPyENBiDjSQQwn4fRPHc5WvtnCJYKwt6gidjcDUh8Jg/IB54HPIBX+nsvD7UcpExC01+WCNvoWFaxBm8N53bp7gkONupBfGBZaNESaDFD+C0JrATHxMrV/VCfWY49gFUJiPRvBgA4WHgpbLMLJ959HLbRsctS3e/4y+xgIHJRg46t0ULUWQZajDQMwdTdru0ESEPRwpXoHQKgGyL9Ow9ArvvSmPY5QJmI+Pla++KHW7Mk6AdcEa/dV43k7SCObMf6vAJ8Ny+GxmwK8dQ1/mS0WM9PxI3sglWIDh2E1R0QmZ1ggXkQ2QaQSA2s+QqglMAykmg/+3a2s/moEAx7LcutvZZA49LvIdG4BVEzjGc48P3S6ch0t8NKJ3hJfqFO+UW22n7BPXJfNRYMA9AGcjvhWH1QtADIOAphNUFY57wagPFiMH06hAv0XDkhui6dymQS7QHOmX7dG4xI1imBSNAdOUZR8/JNtH7ipDIp+lbFGuwYGmBlMzLd/6AP/7aOFZcuzk6++5kQ3JMQff8EibRXCclVTmY48BEAz/eGycx0wjJ6IBwDnNvQQ3IKpwCRRbLXQdsFcTUWxKTcwjU3CL0Cr74G2bmP7kHTO48n9/zdeuSuGXxR2TjtubLZj2lF4+9RyT4NYZ4BieRASSbLNQlEXp6QDQoyQMIciCEb2bSDlk/c7IVmfK16M+oADCSZG7Ogv7r0AFhC08OZ945nqje9iDPy2ZKFGLdhufLsqOLwwgnTFmmFY6cpDD0gp80rx4RIAsL0hscrViED1wW5WXR3WKLrsps9dQ5PP/Vj7Ae8st03x/eATLUSIFBfg+7/NOPJ9c/joA/lv1tyP8qqH2bfGF/Cq/IKx1HxmJJgKC/EtIACrhiAyMA2EzCNJFLdaTfeYWf7svTRzrewqWYfmgeEZXa7DnErgL73BTy17Fm8cYu45x3figoQWfFlzLlnFrt37Gg2LRSgUQzQGEgRxNKWjbbWDjr4Th3e3XPAy/tS8CZh/94HkOebRAbufc/Is29q/zoLJSeRSb/L2lxuGqT5YyyF5bXf49xr+cy9NRNKCF9IXkshee+fpai8ljEjzf+/f+DljuWG5YJIYd98GG+ncbudkXyWK5zbYwngi18vLAfmtO8F6QkfxPeIf5bi17c4gwHcmllze+0DSEjf5Pd+oznbJG9IZK/l+SbRXIE72h0Pt/L9L+//C5p4sk8eO8anAAAAAElFTkSuQmCC" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p style="color: red;">
						<?php echo $this->input->post(null,true) ? 'Email atau password yang anda masukan salah' : '' ?>
					</p>
				</div>
				<div class="col-xs-8">
					<div class="checkbox">
						<p style="padding-top: 6px;">
							<a href="http://bbpjn6.net/rumija">Kembali ke halaman depan</a>
						</p>
					</div>
				</div>
				<div class="col-xs-4">
					<button type="submit"  class="btn btn-primary btn-block btn-flat">Tracking</button>
				</div>
			</div>
		</form>	
	</div>

</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src='<?php echo base_url('assets') ?>/dist/admin/adminlte.min.js'></script>
	<script src='<?php echo base_url('assets') ?>/dist/admin/lib.min.js'></script>
	<script src='<?php echo base_url('assets') ?>/dist/admin/app.min.js'></script>
	<script type="text/javascript" src="assets/sweetalert-master/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			<?php if($this->session->fail_captcha): ?>
			swal({   title: "Akses Ditolak",   text: "Kode captcha yang anda masukan salah", imageUrl: "assets/logo.png" });
			<?php endif ?>
			<?php if($this->session->fail_ID): ?>
			swal({   title: "Tidak Ditemukan",   text: "Nomor pengajuan yang anda masukan tidak dikenali oleh sistem, silahkan coba kembali dan pastikan nomor pengajuan yang anda masukan benar.", imageUrl: "assets/logo.png" });
			<?php endif ?>
			<?php if($this->session->aktivasi_sukses): ?>
			swal({   title: "Aktivasi Berhasil",   text: "Akun anda berhasil diaktivasi. Silahkan login untuk masuk ke halaman dashboard akun Anda.", imageUrl: "assets/logo.png" });
			<?php endif ?>
		});
		function recaptcha(){
			jQuery.getJSON('<?php echo site_url('welcome/recaptcha') ?>',function(r){
				jQuery('#captchacontainer').find('img').remove();
				jQuery('#captchacontainer').html(r.image);
			});
		}
	</script>
	</body>
</html>