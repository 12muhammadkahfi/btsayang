<!DOCTYPE html>
<!-- saved from url=(0057)https://blackrockdigital.github.io/startbootstrap-agency/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
  <link rel="icon" href="http://bbpjn6.net/rumija/apps/assets/logo.png" type="image/x-icon" />
    <title>Sistem Perijinan RUMIJA</title>

    <!-- Bootstrap Core CSS -->
    <link href="http://localhost/rumija/sources/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://localhost/rumija/sources/css" rel="stylesheet" type="text/css">
    <link href="http://localhost/rumija/sources/css(1)" rel="stylesheet" type="text/css">
    <link href="http://localhost/rumija/sources/css(2)" rel="stylesheet" type="text/css">
    <link href="http://localhost/rumija/sources/css(3)" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="http://localhost/rumija/sources/agency.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        @charset "UTF-8";
        .navbar-custom .navbar-toggle {
            background: none !important;
            background-color: none !important;
            border: none !important;
            border-color: none !important;
            font-size: 16px;
            margin: 5px 5px 0px 0px;
        }
        .navbar-brand {
            font-family: 'Lato', sans-serif !important;
            background: url(sources/logo.png) no-repeat left 10px center;
            background-size: auto 64%;
            padding-left: 48px;
        }
        .navbar-custom .navbar-nav>.active>a {
            border-radius: 0px !important;
        }
        @media (max-width: 768px){
          .navbar-custom {
              background-color: #382372 !important;
              padding: 3px 0px;
          }
        }
        @media (min-width: 768px){
            .navbar.affix-top .navbar-right {
                background-color: #fed136;
            }
            .navbar.affix {
                background-color: #382372 !important;
                padding: 3px 0px;
            }
            .navbar.affix-top .navbar-right li a:hover, .navbar.affix-top .navbar-right li.active a {
                color: #382372 !important;
            }
            .navbar.affix-top .navbar-brand {
                font-family: 'Lato', sans-serif !important;
                background: url(sources/logo.png) no-repeat left center;
                background-size: auto 100%;
                padding-left: 60px;
            }
        }
        header {
            background: url(sources/jalan-raya.jpg) no-repeat top center;
            background-size: 100% auto;
            height: 320px;
            padding-top: 10%;
            margin-bottom: -100px;
        }
        /*================      Fonts    =================*/
        .search-form {
          display: block;
          position: relative;
          width: 48.8%;
          margin-top: 53px;
          z-index: 3;
          margin-left: auto;
          margin-right: auto;
        }
        .search-form:before, .search-form:after {
          display: table;
          content: "";
          line-height: 0;
        }
        .search-form:after {
          clear: both;
        }
        @media (max-width: 991px) {
          .search-form {
            width: 90%;
          }
        }
        .search-form_label {
          width: 84%;
          display: block;
          padding: 10px 17px 10px 67px;
          background: #fff;
          border: 1px solid transparent;
          float: left;
        }
        @media (max-width: 1199px) {
          .search-form_label {
            width: 75%;
            text-align: center;
          }
        }
        @media (max-width: 991px) {
          .search-form_label {
            width: 100%;
            padding-left: 20px;
            padding-right: 67px;
          }
          .search-form_label:before, .search-form_label:after {
            display: none;
          }
        }
        .search-form_label:before {
          content: "";
          position: absolute;
          top: 9px;
          left: 9px;
          height: 22px;
          background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAEUElEQVRIS62WTYgcVRDHq+r17IdRGYzLqrtOv8x01sTRXBYliuKKBBSi5iBBEr3lEgQ/IEYCOShBUCMBPXjxJCyKGFB3zcVDGBQEA4uozOqG+ejeLy8GQyBusjNVJe9NT5ydzG5WcKBh+uPV79W//lXdCJv4TQAEJYCmezSKolu10bgPlUaENCDGCyCrs5WlpcU0lAEAAQBth8YbMNx9d0g+nx/DphwD1L2IdHt7oYukqlcA4AcFPVVLkjMdMHb/N4JcixNZ+5oqnCIiEHGbBED8d6mq+nN3sPBpymReqFQqVwHAZcUbQfwDhTA8aUxwhFmaABqkoD8AYE4VVgkhB4g7HNhlZMgMsPBPjPBwHMcuQ+oJadcgCrcdIkMfM/MKIg6qaiIqRxoiZxYXF1faUhdyuXFAOkFET4nI34bMTcw8VZ2Pn10P4sAa3RENaV+jBog3e2lEz12R5kRHcErl9rq7X8HadwnpqIisENGgsu6vzNe/uC6TdhaFcNtxIjwhIq62FzPNxvbfl5YuFIvFvnK53Oh0DwAEqaMkb+0UAT7toKL6Yy2Jd/eSy2dSsPYcAj7gi6lyrBbH70RR1J8WtJcpHaiZH83fj0Z+STehamhnN8QDdoyMbG0EmQoAZH1BmXbVFmu/On3THa/n/NYGQ/szIu5yJ6zyXDfEBxnL5fKM9Bsi9qnqRdNY3X5+efnPtAbXmqwHyTuyLVmqwuGekHw+n0OWOQAYUIDLDeGxhYWF5f8A+ZYA9ziICh/qKdfo6OhgvwmcXHf5B1WeqCbJ2XZzraOVl8paO2AU3NoRt1YE9/QqvE85CsNpRLPXjSBR/ayaxAdSZ632grRNEeW27UeDn6euvJRpNgrrQgrW7kPAL1WkiUQBgO6rxPHX4+PjmVtmZrTUGoJ+whSLRVMul1ettVmjMAsAd7rJwMKTtSR5sRfEW3F4eHjLloHBeQS4LQ3GqPh8Zb5+ulcmqVmmEfFeUXXzyqihe2q12vluiJfKd29ozyLi46ra3jH5zlf5hlU/MQCzIkEDDd8Nis8AwuHUjVeNMf3CzZcqSfKRa+5OSCegRESPiagbiu66IiKpQBPJLUIQFQVF9ud++goTomkVm49Xk+Tt7incBphCGJaIzCMi4grc5/rGA1Rd3+xsue36VmkFlwQFX64s1Kc6G9dl0gGw3xPRQx2AhiHKMMsH1fn41cjaCVU9oIAPIsBQGugSAMyCwlfZoa2fzszMuLnm69r9ZgwKof2uJ0Dlw2ocv9JdbGeMbDZLc3Nzl7tGzRpA+81IBWtLhPRomkEGVNkYE7Dw+9UkeT3dsdOIJgCw1DJHp2ZODT+quq63PB6F4UGiYJKF3evS1aDpJRI+WU2Sox2A7kJ0vuY3/FTAfBgeDEwwycyu0BlDBln5vWocv7EB4AbfH2tvu924ekwT4pPqRgjAW7U4fvP/Aqz5Woms3c2If9XrdTd9Ny3FZlL6B5sWGWWPcjj3AAAAAElFTkSuQmCC) no-repeat center;
          color: #a3a3a3;
          width: 32px;
          height: 32px;
          background-size: 16px;
        }
        .search-form_label:after {
          content: '';
          position: absolute;
          width: 1px;
          height: 50px;
          background: #a3a3a3;
          top: 0px;
          left: 50px;
        }
        .search-form_input {
          outline: none;
          background-color: transparent;
          border: none;
          -webkit-appearance: none;
          border-radius: 0;
          vertical-align: baseline;
          box-shadow: none;
          color: #000000;
          display: block;
          width: 100%;
          font-size: 15px;
          line-height: 16px;
          height: 28px;
          font-family: "PT Sans", sans-serif;
        }
        .search-form_submit {
          -moz-transition: 0.3s all ease;
          -o-transition: 0.3s all ease;
          -webkit-transition: 0.3s all ease;
          transition: 0.3s all ease;
          width: 16%;
          text-align: center;
          display: inline-block;
          font-size: 20px;
          font-weight: 700;
          padding: 10px 0;
          margin-top: 0;
          font-family: "PT Sans", sans-serif;
          border-radius: 0px !important;
        }
        .search-form_submit:after {
          display: none;
        }
        @media (max-width: 1199px) {
          .search-form_submit {
            width: 25%;
          }
        }
        @media (max-width: 991px) {
          .search-form_submit {
            padding: 0;
            position: absolute;
            font-size: 0;
            top: 0;
            right: 0;
            width: 47px;
            height: 50px;
          }
          .search-form_submit:before {
            content: "";
            position: absolute;
            top: 16px;
            right: 14px;
            height: 22px;
            color: #fff;
            font: 400 16px/16px 'FontAwesome', sans-serif;
            -moz-transition: 0.3s all ease;
            -o-transition: 0.3s all ease;
            -webkit-transition: 0.3s all ease;
            transition: 0.3s all ease;
          }
        }

        .search-form_toggle {
          float: right;
          display: inline-block;
          color: #fff;
          font: 400 44px/44px "FontAwesome";
        }
        .search-form_toggle:before {
          content: "";
        }
        .search-form_toggle.active, .search-form_toggle:hover {
          color: #5BC8A5;
        }
        .search-form_toggle.active:before {
          content: "";
        }

        .search-form_liveout {
          display: block;
          position: absolute;
          top: 100%;
          left: 0;
          right: 0;
          opacity: 0;
          -moz-transition: 0.3s all ease;
          -o-transition: 0.3s all ease;
          -webkit-transition: 0.3s all ease;
          transition: 0.3s all ease;
        }
        .lt-ie9 .search-form_liveout {
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
          filter: alpha(opacity=0);
        }
        .search-form_liveout .search_list {
          font-size: 14px;
          line-height: 24px;
          background: #FFF;
          -webkit-box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.5);
          -moz-box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.5);
          box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.5);
        }
        .search-form_liveout .search_list li + li {
          margin-top: 2px;
        }
        .search-form_liveout .search_link {
          display: block;
          padding: 5px 10px;
          background: #FFF;
          color: #999;
        }
        .search-form_liveout .search_link .search_title {
          color: #5BC8A5;
          text-transform: uppercase;
        }
        .search-form_liveout .search_link:hover {
          background: #5BC8A5;
          color: #f4fbf9;
        }
        .search-form_liveout .search_link:hover .search_title {
          color: #FFF;
        }
        .search-form_liveout .search_error {
          display: block;
          color: #5BC8A5;
          background: #fafafa;
          padding: 10px 10px;
          overflow: hidden;
        }
        .search-form_liveout .match {
          display: none;
        }
        .search-form_liveout button {
          display: block;
          width: 100%;
          color: #5BC8A5;
          background: #fafafa;
          padding: 10px 10px;
        }
        .search-form_liveout button:hover {
          background: #5BC8A5;
          color: #FFF;
        }
        input:focus + .search-form_liveout {
          opacity: 1;
        }
        .lt-ie9 input:focus + .search-form_liveout {
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=1)";
          filter: alpha(opacity=100);
        }
        .search-form_liveout .tablet, .mobile .search-form_liveout {
          display: none !important;
        }

        .search-frame, .search-frame body {
          width: 100%;
          height: auto;
          background: transparent;
        }
        .search-frame, .search-frame body, .search-frame h1, .search-frame h2, .search-frame h3, .search-frame h4, .search-frame h5, .search-frame h6, .search-frame p, .search-frame em {
          margin: 0;
          padding: 0;
          border: 0;
          font: inherit;
          vertical-align: top;
        }
        .search-frame img {
          max-width: 100%;
          height: auto;
        }
        .search-frame a {
          font: inherit;
          color: inherit;
          text-decoration: none;
          transition: 0.3s all ease;
        }
        .search-frame a:active {
          background: transparent;
        }
        .search-frame a:focus {
          outline: none;
        }
        .search-frame h4 {
          font-family: "PT Sans", sans-serif;
          font-size: 18px;
          font-weight: 700;
          text-transform: none;
        }
        @media (max-width: 991px) {
          .search-frame h4 {
            font-size: 16px;
            line-height: 16px;
          }
        }
        .search-frame h4 a:hover {
          text-decoration: underline;
        }
        .search-frame .search_list {
          margin: 0;
          padding: 0;
          border: 0;
          font: inherit;
          vertical-align: top;
          list-style-type: none;
          counter-reset: num1;
        }
        .search-frame .search_list p {
          font: 300 15px/22px "PT Sans", sans-serif;
        }
        .search-frame .search_list .match {
          color: #000;
          font-size: 12px;
          display: block;
        }
        .search-frame .search_list li {
          padding-top: 20px;
          padding-left: 50px;
          position: relative;
          padding-bottom: 20px;
          transition: 0.3s all ease;
        }
        .search-frame .search_list li:hover {
          background: rgba(204, 204, 204, 0.3);
        }
        .search-frame .search_list li:before {
          content: counter(num1) ".";
          counter-increment: num1;
          position: absolute;
          left: 25px;
          top: 21px;
          width: 24px;
          height: 24px;
          font-size: 18px;
          line-height: inherit;
        }
        .search-frame .search_list li + li {
          border-top: 5px solid #cccccc;
        }
        .search-frame .search {
          color: #000;
          font-weight: 700;
        }
        .search-frame .match {
          font-size: 12px;
          display: block;
          font-weight: 700;
        }
        .search-frame * + p {
          margin-top: 16px;
        }

        body.search_body {
          background: #1b1b1d;
        }

        .content {
          padding-top: 40px;
          padding-bottom: 40px;
        }

        .search_head {
          padding-bottom: 25px;
        }
        .thumbnail {
            max-width: 420px;
            margin-top: 10px;
            margin-left: auto;
            margin-right: auto;
        }
        .thumb {
            display: block;
            position: relative;
            overflow: hidden;
        }
        .thumb_overlay {
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            opacity: 0;
            -moz-transition: 0.3s all ease;
            -o-transition: 0.3s all ease;
            -webkit-transition: 0.3s all ease;
            transition: 0.3s all ease;
            color: #fff;
            background: transparent;
        }
        #tracking tbody tr td {
          font-size: 12px;
          vertical-align: top;
        }
        .htitle {
            margin: 0px 0px 10px 0px;
            padding: 0px;
        }
        .h1.clr-white {
            display: inline-block;
            background: rgba(56, 35, 114, 0.54);
            padding: 10px 20px;
            margin-bottom: -10px;
        }
        .fa-stack.fa-4x .fa-circle { color: #382372; }
        .fa-stack.fa-4x .fa-inverse { color: #fed136; }
    </style>
</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top affix-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top"><span class="hidden-xs hidden-sm">Sistem Perijinan RUMIJA </span>BBPJN VI</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden active">
                        <a href="https://blackrockdigital.github.io/startbootstrap-agency/#page-top"></a>
                    </li>
                    <li class="">
                        <a class="page-scroll" href="#tentang">Tentang</a>
                    </li>
                    <li class="">
                        <a class="page-scroll" href="#panduan">Panduan</a>
                    </li>
                    <li class="">
                        <a href="<?php echo $_SERVER['SERVER_NAME'] !== "localhost" ? "apps/index.php/tracking" : "/rumija/apps/index.php/tracking" ?>" class="page-scroll">Lacak</a>
                    </li>
                    <li class="">
                        <a class="page-scroll" href="<?php echo $_SERVER['SERVER_NAME'] !== "localhost" ? "apps" : "/perijinan" ?>">Login</a>
                    </li>
                    <li class="">
                        <a class="page-scroll" href="<?php echo $_SERVER['SERVER_NAME'] !== "localhost" ? "apps/index.php/register" : "/rumija/apps/index.php/register" ?>">Daftar</a>
                    </li>
                    <li class="">
                        <a class="page-scroll" href="<?php echo $_SERVER['SERVER_NAME'] !== "localhost" ? "apps/index.php/instruksi" : "/rumija/apps/index.php/instruksi" ?>">instruksi</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container" id="hd-home">
            <div class="h1 clr-white text-center">
                <span class="hidden-xs hidden-sm">Balai Besar Pekerjaan Jalan Nasional VI</span><span class="hidden-md hidden-lg">BBPJN VI</span><br/>
                <small style="color: #e2d136;">Sistem Perijinan Pemanfaatan Ruang Milik Jalan</small>
            </div>

            <!-- <form id="formutama" class="search-form" accept-charset="utf-8">
              <label class="search-form_label">
                <input id="ID" name="ID" required="required" autofocus="true" class="search-form_input" type="number" name="s" autocomplete="off" placeholder="Masukan nomor pengajuan (10 angka)">
                <span class="search-form_liveout"></span>
              </label>
              <button class="search-form_submit btn btn-primary" type="submit">Cari</button>
            </form> -->
        </div>
    </header>
    <section id="tentang" style="margin-bottom: -30px !important;padding-bottom: 0px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Tentang Sistem Perijinan RUMIJA BBPJN VI</h2>
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                            <p>
                              
                            </p>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>
    <section id="panduan">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Panduan</h2>
                    <h3 class="section-subheading text-muted">
                        Panduan pengajuan perijinan pemanfaatan ruang milik jalan di BBPJN VI
                    </h3>
                </div>
            </div>
            <div class="row text-center" style="margin-bottom: 30px;">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                    <?php foreach ($manualinstruksi as $key => $jdl) { ?>
                    <h4 ng-repeat="jdl in data.manualinstruksi" ng-bind="jdl.judul" class="service-heading"><?php echo $jdl->judul ?></h4>
                    <p class="text-muted" ng-bind="jdl.ket"><?php echo $jdl->ket ?>
                    </p>
                  <?php } ?>
                </div>
                
            
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright © BBPJN VI</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://blackrockdigital.github.io/startbootstrap-agency/#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="https://blackrockdigital.github.io/startbootstrap-agency/#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://blackrockdigital.github.io/startbootstrap-agency/#"><i class="fa fa-google-plus"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="<?php echo $_SERVER['SERVER_NAME'] !== "localhost" ? "apps" : "/perijinan" ?>">Login</a>
                        </li>
                        <li><a href="<?php echo $_SERVER['SERVER_NAME'] !== "localhost" ? "apps/index.php/register" : "/perijinan/index.php/register" ?>">Pendaftaran</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->

    <!-- Portfolio Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-centered" src="sources/roundicons-free.png" alt="">
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <p>
                                    <strong>Want these icons in this portfolio item sample?</strong>You can download 60 of them for free, courtesy of <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">RoundIcons.com</a>, or you can purchase the 1500 icon set <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">here</a>.</p>
                                <ul class="list-inline">
                                    <li>Date: July 2014</li>
                                    <li>Client: Round Icons</li>
                                    <li>Category: Graphic Design</li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
   
    <!-- jQuery -->
    <script src="http://localhost/rumija/sources/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://localhost/rumija/sources/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://localhost/rumija/sources/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="http://localhost/rumija/sources/jqBootstrapValidation.js"></script>
    <script src="http://localhost/rumija/sources/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="http://localhost/rumija/sources/agency.min.js"></script>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">
              Lacak Status Pengajuan
            </h4>
          </div>
          <div class="modal-body">
            <center id="loader"><img src="http://www.kurryworld.ch/wp-content/themes/Kurryworld2014-child/images/loader.gif" /></center>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      jQuery(document).ready(function($){
        $("#formutama").submit(function(){
          $(".bs-example-modal-lg").find(".modal-body").find("div,table").remove();
          $(".bs-example-modal-lg").modal("show");
          $("#loader").show();
          var request = $.ajax({
            url: "<?php echo $_SERVER['SERVER_NAME'] === "localhost" ? "/perijinan/index.php/perijinan/tracking" : "apps/index.php/perijinan/tracking" ?>",
            type: "POST",
            data: {ID : $("#ID").val()},
            dataType: "json"
          });

          request.done(function(resp) {
            console.log(resp);
            $("#loader").hide();
            if(!resp.status){
              $("<div/>").css({
                'color':'red',
                'text-align':'center',
                'font-size':'16px'
              }).text("Nomor pengajuan yang anda masukan tidak dikenali").appendTo($(".bs-example-modal-lg").find(".modal-body"));
            } else {
              $(".bs-example-modal-lg").find(".modal-body").append("\
                <table id=\"tracking\" class=\"table table-condensed table-bordered table-striped table-hover\">\
                  <thead>\
                    <tr>\
                      <th style='width: 30px;text-align: center;'>#</th>\
                      <th style='width: 120px;text-align: center;'>Tanggal</th>\
                      <th style=''>Keterangan</th>\
                      <th style=''>Author</th>\
                    </tr>\
                  </thead>\
                  <tbody></tbody>\
                </table>");
              $.each(resp.data.tracking,function(key,value){
                $("<tr>\
                  <td align=\"center\">"+(key+1)+"</td>\
                  <td align=\"center\">"+(value.created_time)+"</td>\
                  <td>"+(value.remark)+"</td>\
                  <td>"+(value.nama_lengkap)+"</td>\
                  </tr>").appendTo($("#tracking").find("tbody"));
              });
              $("#ID").val('');
            }
          });

          request.fail(function(jqXHR, textStatus) {
            console.log(jqXHR, textStatus);
            alert( "Gagal terhubung dengan server" );
            $(".bs-example-modal-lg").modal("hide");
          });
          return false;
        });
      });
    </script>
</body>
</html>