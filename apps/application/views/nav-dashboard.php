	<header class="main-header">
		<a style="background: #02018d;" href="" class="logo"><img style="margin-top: -3px;" width="32" src="assets/logo.png" /> <b>RUMIJA</b> BBPJN6</a>
		<nav style="background: #17169e;" class="navbar navbar-static-top navbar-static-top" role="navigation">
			<a href="javascript:void(0)" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					
					
					<li>
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
							<span class="hidden-xs"><i class="fa fa-files-o" ></i>  Surat Masuk - <?php foreach ($this->mks->subKategori() as $ke => $sub) { ?> <?php echo $sub->ID ?> <?php } ?></span>
							
						</a>
						
					</li>
					
					<li class="dropdown user user-menu" ng-show="mysession.level!=='5'">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
							<span class="hidden-xs arf"><i class="fa fa-files-o"></i> Peringatan Surat - <?php foreach ($this->mks->subKategori() as $ke => $sub) { ?> <?php echo $sub->ID ?> <?php } ?>
							</span>
						</a>
						<ul class="dropdown-menu" style="width:20px;">
<!-- 							<li class="user-header">
								<p><?php echo $this->session->nama_lengkap ?></p>
							</li> -->
								
								<li>
									<a href="#/terima" class="btn btn-default btn-flat"><i class="fa fa-files-o"></i> Surat Masuk - <?php foreach ($this->mks->subTerima() as $key => $value) {?> <?php echo $value->ID ?> <?php } ?>
									</a>
								</li>
								<li>
									<a href="#/ekspose" class="btn btn-default btn-flat"><i class="fa fa-files-o"></i> Surat Ekspos - <?php foreach ($this->mks->subEkspos() as $key => $value) {?> <?php echo $value->ID ?> <?php } ?></a>
								</li>
								<li>
									<a href="#/survei" class="btn btn-default btn-flat"><i class="fa fa-files-o"></i> Surat Survei - <?php foreach ($this->mks->subSurvei() as $key => $value) {?> <?php echo $value->ID ?> <?php } ?></a>
								</li>
								<li>
									<a href="#/rapat" class="btn btn-default btn-flat"><i class="fa fa-files-o"></i> Hasil Rapat - <?php foreach ($this->mks->subHasilRapat() as $key => $value) {?> <?php echo $value->ID ?> <?php } ?></a>
								</li>
								<li>
									<a href="#/persetujuan" class="btn btn-default btn-flat"><i class="fa fa-files-o"></i> Persetujuan - <?php foreach ($this->mks->subPersetujuan() as $key => $value) {?> <?php echo $value->ID ?> <?php } ?></a>
								</li>
								<li>
									<a href="#/rekomtek" class="btn btn-default btn-flat"><i class="fa fa-files-o"></i> Rekomtek - <?php foreach ($this->mks->subRekomtek() as $key => $value) {?> <?php echo $value->ID ?> <?php } ?></a>
								</li>
						</ul>
					</li>
					<li class="user user-menu">
						<a href="#/profil" >
							<span class="hidden-xs"><i class="fa fa-user"></i> <?php echo $this->session->nama_lengkap?>
							</span>
						</a>
						
					</li>
					<li class="user">
						<a href="index.php/welcome/logout">
							<span class="hidden-xs arf"><i class="fa fa-user"></i> Keluar 
							</span>
						</a>
					</li>
				</ul>
			</div>
			
		</nav>
	</header>