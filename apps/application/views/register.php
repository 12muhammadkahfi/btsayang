<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Pendaftaran Perijinan btsuici BBPJN6</title>
  <link rel="icon" href="<?php echo base_url('assets/logo.png') ?>" type="image/x-icon" />
  <base href="<?php echo $_SERVER['SERVER_NAME'] === "localhost" ? "http://localhost/btsuici/apps/" : "" ?>">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/btsuici/apps/assets/dist/admin/adminlte.min.css">
  <link rel="stylesheet" type="text/css" href="/btsuici/apps/assets/sweetalert-master/dist/sweetalert.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css">
  <style type="text/css">
    .login-box, .register-box {width: auto;margin: 2% auto !important;}
  </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition register-page">
<form action="/btsuici/apps/index.php/welcome/doregister" method="POST" role="form">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px;">
    <div class="register-logo">
      <a href=""><b>SIGNUP APPLICATION</b> MKTREE</a>
    </div>
  </div>
</div>
<div class="row" style="background: #FFF;">
<?php if(!$this->session->success_registration): ?>
<!-- <div class="col-md-6 col-sm-12">
<div class="register-box">
  <div class="register-box-body">
    <h4>Info Pendaftaran</h4>
 -->
      <!-- <div class="form-group has-feedback">
        <select name="tipe_organisasi" id="inputTipe_organisasi" class="form-control" required="required">
          <option value="">Pilih tipe organisasi</option>
          <option>PERORANGAN</option>
          <option>KELOMPOK MASYARAKAT</option>
          <option>ORGANISASI</option>
          <option>BADAN USAHA</option>
          <option>BADAN HUKUM</option>
          <option>INSTANSI PEMERINTAH</option>
        </select>
      </div> -->
<!--       <div class="form-group has-feedback">
        <input value="<?php echo isset($post->nama_organisasi) ? $post->nama_organisasi : '' ?>" required="required" name="nama_organisasi" type="text" class="organisasi form-control" placeholder="Nama organisasi">
        <span class="glyphicon glyphicon-home form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->pimpinan_organisasi) ? $post->pimpinan_organisasi : '' ?>" required="required" name="pimpinan_organisasi" type="text" class="organisasi form-control" placeholder="Nama pimpinan organisasi">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->alamat_organisasi) ? $post->alamat_organisasi : '' ?>" required="required" name="alamat_organisasi" type="text" class="organisasi form-control" placeholder="Alamat organisasi">
        <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->telp_organisasi) ? $post->telp_organisasi : '' ?>" required="required" name="telp_organisasi" type="text" class="organisasi form-control" placeholder="No. Telp organisasi">
        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->fax_organisasi) ? $post->fax_organisasi : '' ?>" required="required" name="fax_organisasi" type="text" class="organisasi form-control" placeholder="No. Fax organisasi">
        <span class="glyphicon glyphicon-print form-control-feedback"></span>
      </div>
  </div>
 -->  <!-- /.form-box -->
<!-- </div>
 --><!-- /.register-box -->
<!-- </div> -->
<div class="col-md-12 col-sm-12">
<div class="register-box">
  <div class="register-box-body">
    <h4>Personal Information and Login Account</h4>

      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->nama_lengkap) ? $post->nama_lengkap : '' ?>" required="required" name="nama_lengkap" type="text" class="form-control" placeholder="Nama lengkap anda">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->jabatan) ? $post->jabatan : '' ?>" required="required" name="jabatan" type="text" class="organisasi form-control" placeholder="Jabatan dalam organisasi">
        <span class="glyphicon glyphicon-credit-card form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->email) ? $post->email : '' ?>" required="required" name="email" type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->password) ? $post->password : '' ?>" required="required" name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input value="<?php echo isset($post->repassword) ? $post->repassword : '' ?>" required="required" name="repassword" type="password" class="form-control" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Sudah punya akun? <a href="/btsuici/apps">Login Disini</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Daftar</button>
        </div>
        <!-- /.col -->
      </div>

  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
</div>
<?php else: ?>
<div class="col-sm-12">
  <div class="register-box">
    <div class="register-box-body">
      <h4><img style="margin: -4px 2px 0px 0px;" width="18" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADOUlEQVRYR+WWTUgUYRyHn9liPHQqKEoD10MR2hdkaEVRaWVGRTcPXSKo0CSxMvuwQ9r6EYkEidLBSwdvUVFpGom6utIWZip+ELuSfZe3Dhvqxvvurs7Mzm67tlLQnHZm33mf5/973//MKPzlQ/nLfP4LAXVvJR6R9NMLxAE/tanPdwLqThuee4W3JfNIdR5tl/QS8ymg7rLhaSyo5n5roRQ4nFlNTk0hzzUS8yUg4XcLynjcegWvn6J4ITuzjKM1V2Yk5kNAwhvOXKal7bqsXLH4Vt3rBYsXsjJKOH6rlKYi4mItIOF38s/S1nlTVq5oCOL3QgXcE+B4B0/OsSSWAupuG57a/Fw6u2qD4BYBt4BrArpc0HyedGDEXMCLgoI3ioeUhN86fYzungZz+AIY+w72txK+A3ABX8wE1H3+vm026VsTKQm/mZuD81Wj72/NrLJyAf8G9lFoLpqFi2eCUUDNsOGpyDso5ym+/ZBnhr41CEh4ee4h+nofhIZ/BftIMNzgioSXntrDYH+LnCx57R5K6lpCScyOH2iRO9y08i9gHzaHawXUTBuekpPbGR3q8E0m5lNg1ZrtXKvvMEpI+FXteLPYP4N9KDQ8IKDur8JTfCKVt8NOpqb1IYsWtq5OpbzeSatvORCyF0+m4h5xYhiOXPOFMPYJ7IPh4QGBxdk3mEhLButymJyCacP+F5PGJ6VQVTcg7YpOpfDBNRA8zuLbcK6P4OiHpgv6DWfWVSK4xcDqrEoc6evAugImJ4MlxMClK5PkHF/HXUE9GqjcLeBvIoPPLAGwDEjKqqQ9fT1Y4/1JGPMNJGPoHYu/cvcHcPRFDtdtQq3Elg2QmBBCwpBjAD72HrpfRwcPakOdxEZIXBleYgY+Dt290cONAuJc1UlsCp2ErvKXc4ObCQRLpAYnoavcOXd4KAG9RAXtaZvBmujrDnGIPnePQc8LaCr+fauZtV/gWrjX8exyVNCengZWq+82txscPX8OD5dAQFAnsXWb73KXPTbwSAR0y3HgBu3iwiPN+9z4mR0u7lBPwkjuCSSR4B/8XnxM/Ck80gS0y7HIf/IjFvBoBSJJKuoxsfwojRr+TyTwC85JXV6wz8DQAAAAAElFTkSuQmCC"> Pendaftaran Berhasil</h4>
      <p style="margin-bottom: 30px;">
        Kami telah mengirimkan email yang berisikan link aktivasi pendaftaran akun Anda ke alamat email <strong><?php echo $post->email ?></strong>. Mohon klik / kunjungi link tersebut untuk melakukan aktivasi akun Anda. Anda tidak dapat melakukan login ke dalam akun yang anda buat sebelum aktivasi dilakukan.<br/><br/>Salam, Admin SIbtsuici BBPJN6 Jakarta.
      </p>
      <hr/>
      <h4 style="margin-top: 30px;">
        Belum menerima email dari kami?
      </h4>
      <strong>Solusi 1</strong>
      <p>
        Jika anda tidak menemukan email kami di folder inbox email anda, periksalah folder junk/spam email anda.
      </p>
      <strong>Solusi 2</strong>
      <p>
        Request pengiriman ulang email aktivasi. <a href="/btsuici/apps/index.php/resend_mail">Kirim ulang email</a>.
      </p>
      <hr/>
      <a href="/btsuici/apps" class="btn btn-warning btn-sm">Halaman Login</a>
    </div>
  </div>
</div>
<?php endif ?>
<!-- jQuery 2.2.3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script type="text/javascript" src="/btsuici/apps/assets/sweetalert-master/dist/sweetalert.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    $("select[name=tipe_organisasi]").change(function(){
      var tipe_organisasi = $(this).val();
      if(tipe_organisasi==="PERORANGAN"){
        $(".organisasi").removeAttr("required").attr("disabled","disabled").css("color","transparent");
      } else {
        $(".organisasi").removeAttr("disabled").attr("required","required").removeAttr("style");
      }
    });
    <?php if(isset($post->tipe_organisasi)): ?>
    $("select[name=tipe_organisasi]").val('<?php echo $post->tipe_organisasi ?>');
    $("select[name=tipe_organisasi]").trigger('change');
    <?php endif ?>
    <?php if($this->session->fail_registration): ?>
    swal({   title: "Registrasi Gagal",   text: "<?php echo $this->session->fail_registration ?>",   imageUrl: "/btsuici/apps/assets/logo.png" });
    <?php endif ?>
  });
</script>
</div>
</div>
</div>
</div>

</body>
</form>
</html>