<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perijinan extends CI_Controller {

	public function uploadfiles()
	{
		foreach ($_FILES['file']['name'] as $key => $value) {
			$arrtype = explode('/', strtolower($_FILES['file']['type'][$key]));
			if($_FILES['file']['size'][$key]<=20000000&&($arrtype[0]==="image"||$arrtype[1]==="pdf")){
				move_uploaded_file($_FILES['file']['tmp_name'][$key], "tmps/{$this->session->ID}/{$value}");
				$_FILES['file']['type'][$key] = $arrtype;
			} else {
				unset($_FILES['file']['name'][$key]);
				unset($_FILES['file']['type'][$key]);
				unset($_FILES['file']['size'][$key]);
				unset($_FILES['file']['tmp_name'][$key]);
				unset($_FILES['file']['error'][$key]);
			}
		}
		exit(json_encode($_FILES));
		// file_put_contents('tmps/debug.txt', json_encode($_FILES['file']['name']));
	}

	public function clear_tmps()
	{
		if(!file_exists("tmps/{$this->session->ID}")){
			mkdir("tmps/{$this->session->ID}");
			chmod("tmps/{$this->session->ID}", 0777);
		}
		array_map('unlink', glob("tmps/{$this->session->ID}/*"));
	}

	
	public function uploaddokumen()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->uploaddokumen();
		if($data) exit(json_encode(array('status'=>true,'ID'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Permohonan anda tidak dapat kami terima.')));
	}
	public function ajukanpermohonan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->ajukanpermohonan();
		if($data) exit(json_encode(array('status'=>true,'ID'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Permohonan anda tidak dapat kami terima.')));
	}
	public function submitpermohonan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->submitpermohonan();
		if($data) exit(json_encode(array('status'=>true,'ID'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Permohonan anda tidak dapat kami terima.')));
	}
	public function groupperijinan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->groupperijinan();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function data_perijinan_group()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->data_perijinan_group();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Belum ada data untuk ditampilkan.')));
	}
	public function data_perijinan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->data_perijinan();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Belum ada data untuk ditampilkan.')));
	}
	public function uploaddok()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->uploaddok();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Belum ada data untuk ditampilkan.')));
	}

	public function detail()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->detail();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'URL tidak valid.')));
	}

	public function detaildokumen()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->detaildokumen();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'URL tidak valid.')));
	}

	public function tracking()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->tracking();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'URL tidak valid.')));
	}

	public function submit_penerimaan_berkas()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->submit_penerimaan_berkas();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Proses penerimaan berkas berhasil.')));
		else exit(json_encode(array('status'=>false,'message'=>'Gagal memproses penerimaan berkas.')));
	}

	public function submit_berkas_ekspose()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->submit_berkas_ekspose();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Proses penerimaan berkas berhasil.')));
		else exit(json_encode(array('status'=>false,'message'=>'Gagal memproses penerimaan berkas.')));
	}

	public function submit_status()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->submit_status();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Berhasil mengupdate status.')));
		else exit(json_encode(array('status'=>false,'message'=>'Gagal mengupdate status.')));
	}
	public function submit_status_survei()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->submit_status_survei();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Berhasil mengupdate status.')));
		else exit(json_encode(array('status'=>false,'message'=>'Gagal mengupdate status.')));
	}
	public function submit_status_rapat()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->submit_status_rapat();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Berhasil mengupdate status.')));
		else exit(json_encode(array('status'=>false,'message'=>'Gagal mengupdate status.')));
	}
	public function submit_status_perijinan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->submit_status_perijinan();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Berhasil mengupdate status.')));
		else exit(json_encode(array('status'=>false,'message'=>'Gagal mengupdate status.')));
	}

	public function terima_permohonan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->terima_permohonan();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Penerimaan permohonan berhasil diproses.')));
		else exit(json_encode(array('status'=>false,'message'=>'Penerimaan permohonan gagal diproses.')));
	}

	public function tolak_permohonan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->tolak_permohonan();
		if($data) exit(json_encode(array('status'=>true,'data'=>$data,'message'=>'Penolakan permohonan berhasil diproses.')));
		else exit(json_encode(array('status'=>false,'message'=>'Penolakan permohonan gagal diproses.')));
	}

	public function cetak($ID)
	{
		$this->load->model('Permohonan_model','pm');
		$data['detail'] = $this->pm->detail_print($ID);
		$data['jenis_permohonan'] = array('','Utilitas Kabel Fiber Optik','Utilitas Pipa Gas / Air','Akses Jalan Keluar - Masuk','Utilitas Kabel PLN','Pembukaan Median Jalan','Pemotongan Pohon','Media Iklan Jalan');
		$this->load->view('print_perijinan',$data);
	}

	public function hapus_ijin()
	{
		if(intval($this->session->level)!=1) exit(json_encode(array('status'=>false,'message'=>'Access denied')));
		$this->load->model('Permohonan_model','pm');
		$hapus = $this->pm->hapus_ijin();
		if($hapus) exit(json_encode(array('status'=>true,'message'=>'Berhasil dihapus')));
		else exit(json_encode(array('status'=>false,'message'=>'Gagal dihapus')));
	}

	public function export_excel()
	{
		$filename="data-pengajuan.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->data_perijinan();
		$this->load->library('table');
		$datatable = array(array('#', 'ID Pengajuan', 'Tanggal Pengajuan','Pembuat Pengajuan','Email','Perihal','Ruas Jalan','Provinsi','Contact Person','Telp','Status'));
		foreach ($data as $key => $value) {
			switch ($value->status) {
				case 'PENDING':
					$status = "Menunggu Berkas";
					break;
				
				case 'PROCESSING':
					$status = "Dalam Proses";
					break;
				
				case 'ACCEPT':
					$status = "Rekomtek diterbitkan";
					break;
				
				case 'REJECT':
					$status = "Ditolak";
					break;
				
				default:
					$status = "";
					break;
			}
			$datatable[] = array(
				$key+1,
				$value->ID,
				date("d/m/Y",strtotime($value->tanggal_data)),
				$value->nama_lengkap,
				$value->email,
				$value->perihal,
				$value->nama_ruas_jalan,
				$value->nama_provinsi,
				$value->contact_person,
				"{$value->telp}",
				$status
				);
			$tracking = $this->pm->tracking_only($value->ID);
			foreach ($tracking as $skey => $svalue) {
				$datatable[] = array(
				'',
				'',
				date("d/m/Y",strtotime($svalue->tanggal)),
				$svalue->nama_lengkap,
				$svalue->email,
				$svalue->remark
				);
			}
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}

public function export_excel_single()
	{
		$filename="data-pengajuan.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->data_perijinan();
		$this->load->library('table');
		$datatable = array(array('#', 'ID Pengajuan', 'Tanggal Pengajuan','Pembuat Pengajuan','Email','Perihal','Ruas Jalan','Provinsi','Contact Person','Telp','Status','Tahapan Proses','Satuan Unit Kerja','PPK','Jenis Permohonan'));
		foreach ($data as $key => $value) {
			switch ($value->status) {
				case 'PENDING':
					$status = "Menunggu Berkas";
					break;
				
				case 'PROCESSING':
					$status = "Dalam Proses";
					break;
				
				case 'ACCEPT':
					$status = "Rekomtek diterbitkan";
					break;
				
				case 'REJECT':
					$status = "Ditolak";
					break;
				
				default:
					$status = "";
					break;
			}
			$datatable[] = array(
				$key+1,
				$value->ID,
				date("d/m/Y",strtotime($value->tanggal_data)),
				$value->nama_lengkap,
				$value->email,
				$value->perihal,
				$value->nama_ruas_jalan,
				$value->nama_provinsi,
				$value->contact_person,
				"{$value->telp}",
				$status,
				$value->apv_status,
				$value->nama_unit,
				$value->nama_ppk,
				$value->nama_tipe
				);
			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}

	public function export_excel_tipe($data)
	{
		$filename="data-pengajuan-tipe.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->data_perijinan_laporan_tipe($data);
		$this->load->library('table');
		$datatable = array(array('#', 'ID Pengajuan', 'Tanggal Pengajuan','Pembuat Pengajuan','Email','Perihal','Ruas Jalan','Provinsi','Contact Person','Telp','Status','Tahapan Proses','Satuan Unit Kerja','PPK','Jenis Permohonan'));
		foreach ($data as $key => $value) {
			switch ($value->status) {
				case 'PENDING':
					$status = "Menunggu Berkas";
					break;
				
				case 'PROCESSING':
					$status = "Dalam Proses";
					break;
				
				case 'ACCEPT':
					$status = "Rekomtek diterbitkan";
					break;
				
				case 'REJECT':
					$status = "Ditolak";
					break;
				
				default:
					$status = "";
					break;
			}
			$datatable[] = array(
				$key+1,
				$value->ID,
				date("d/m/Y",strtotime($value->tanggal_data)),
				$value->nama_lengkap,
				$value->email,
				$value->perihal,
				$value->nama_ruas_jalan,
				$value->nama_provinsi,
				$value->contact_person,
				"{$value->telp}",
				$status,
				$value->apv_status,
				$value->nama_unit,
				$value->nama_ppk,
				$value->nama_tipe
				);
			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}

	public function export_excel_double()
	{
		$filename="data-pengajuan.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->data_tracking();
		$this->load->library('table');
		$datatable = array(array('#', 'ID Pengajuan', 'Tanggal Proses Tahapan','Pembuat Proses Tahapan','Status Tahapan','Keterangan'));
		foreach ($data as $key => $value) {
			
			$datatable[] = array(
				$key+1,
				$value->ID,
				date("d/m/Y",strtotime($value->tanggal)),
				$value->nama_lengkap,
				$value->wrstatus,
				$value->remark
				);

		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}

}
