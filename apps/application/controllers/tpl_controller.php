<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class tpl_controller extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);

	}
	public function provinsi()
	{
		$this->load->model('tpl_provinsi','mdm');
		$data = $this->mdm->provinsi();
		exit(json_encode(array('status' => true,'data'=>$data)));
	}
	public function kabupaten()
	{
		$this->load->model('tpl_master_kabupaten','tmk');
		$data = $this->tmk->kabupaten();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function kecamatan()
	{
		$this->load->model('tpl_master_kecamatan','tmk');
		$data = $this->tmk->kecamatan();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function kelurahan()
	{
		$this->load->model('tpl_master_kelurahan','tml');
		$data = $this->tml->kelurahan();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function realisasi_bts()
	{
		$this->load->model('tpl_master_realisasi','tmk');
		$data = $this->tmk->realisasi_bts();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function planning_bts()
	{
		$this->load->model('tpl_master_planning','tmk');
		$data = $this->tmk->planning_bts();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function planning_bts_kemendagri()
	{
		$this->load->model('tpl_master_planning','tmk');
		$data = $this->tmk->planning_bts_kemendagri();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function realisasi_bts_kemendagri()
	{
		$this->load->model('tpl_master_realisasi','tmk');
		$data = $this->tmk->realisasi_bts_kemendagri();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function cari_provinsi(){
		$this->load->model('tpl_master_provinsi','tmp');
		$data = $this->tmp->cari_provinsi();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function cari_kabupaten()
	{
		$this->load->model('tpl_master_kabupaten','tmk');
		$data = $this->tmk->cari_kabupaten();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function cari_kecamatan()
	{
		$this->load->model('tpl_master_kecamatan','tmk');
		$data = $this->tmk->cari_kecamatan();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function sub_kabupaten()
	{
		$this->load->model('tpl_master_kabupaten','tmk');
		$data = $this->tmk->cari_kabupaten();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function sub_kecamatan()
	{
		$this->load->model('tpl_master_kecamatan','tmk');
		$data = $this->tmk->sub_kecamatan();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	public function sub_keluarahan()
	{
		$this->load->model('tpl_master_kelurahan','tmk');
		$data = $this->tmk->cari_kelurahan();
		exit(json_encode(array('status' => true,'data'=>$data)));	
	}
	// public function submit_provinsi()
	// {
	// 	$this->load->model('Master_data_model','mdm');
	// 	$do = $this->mdm->submit_provinsi();
	// 	if($do) exit(json_encode(array(
	// 		'status'=>true,
	// 		'message'=>"Provinsi {$this->params->nama_provinsi} berhasil disimpan"
	// 		)));
	// 	else exit(json_encode(array(
	// 		'status'=>false,
	// 		'message'=>"Provinsi {$this->params->nama_provinsi} gagal disimpan"
	// 		)));
	// }

	// public function hapus_provinsi()
	// {
	// 	$this->load->model('Master_data_model','mdm');
	// 	$do = $this->mdm->submit_provinsi();
	// 	if($do) exit(json_encode(array(
	// 		'status'=>true,
	// 		'message'=>"Provinsi {$this->params->nama_provinsi} berhasil dihapus"
	// 		)));
	// 	else exit(json_encode(array(
	// 		'status'=>false,
	// 		'message'=>"Provinsi {$this->params->nama_provinsi} gagal dihapus"
	// 		)));
	// }

}
