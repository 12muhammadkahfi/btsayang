<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_terima extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}

	public function terima()
	{
		$this->load->model('Master_terima_model','mdm');
		$data = $this->mdm->terima();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function terimaClient()
	{
		$this->load->model('Master_terima_model','mdm');
		$data = $this->mdm->terimaClient();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function terimaUser()
	{
		$this->load->model('Master_terima_model','mdm');
		$data = $this->mdm->terimaUser();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function survei()
	{
		$this->load->model('Master_terima_model','mdm');
		$data = $this->mdm->survei();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function ekspose()
	{
		$this->load->model('Master_terima_model','mdm');
		$data = $this->mdm->ekspose();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function hasil_rapat()
	{
		$this->load->model('Master_terima_model','mdm');
		$data = $this->mdm->hasil_rapat();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function persetujuan()
	{
		$this->load->model('Master_terima_model','mdm');
		$data = $this->mdm->persetujuan();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function rekomtek()
	{
		$this->load->model('Master_terima_model','mdm');
		$data = $this->mdm->rekomtek();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	public function tipe_search()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->tipe_search();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function hapus_tipe()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_tipe();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} gagal dihapus"
			)));
	}

	public function hapus_tipe_pembangunan()
	{
		$this->load->model('Master_terima_model','mdm');
		$do = $this->mdm->submit_tipe_pembangunan();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} gagal dihapus"
			)));
	}

	public function submit_tipe()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_tipe();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} gagal disimpan"
			)));
	}

	public function provinsi()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->provinsi();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	public function submit_provinsi()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_provinsi();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Provinsi {$this->params->nama_provinsi} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Provinsi {$this->params->nama_provinsi} gagal disimpan"
			)));
	}

	public function hapus_provinsi()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_provinsi();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Provinsi {$this->params->nama_provinsi} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Provinsi {$this->params->nama_provinsi} gagal dihapus"
			)));
	}

	public function kota()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->kota();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	

	public function hapus_kota()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_kota();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Kota {$this->params->nama_kota} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Kota {$this->params->nama_kota} gagal dihapus"
			)));
	}


	public function submit_ruas_jalan()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_ruas_jalan();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Ruas jalan {$this->params->nama_ruas_jalan} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Ruas jalan {$this->params->nama_ruas_jalan} gagal disimpan"
			)));
	}

	public function hapus_ruas_jalan()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_ruas_jalan();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Ruas jalan {$this->params->nama_ruas_jalan} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Ruas jalan {$this->params->nama_ruas_jalan} gagal dihapus"
			)));
	}

}
