<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bts_report extends CI_Controller {

	public function export_excel_report_planning()
	{
		$filename="data-rekap_total_expand.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('report_model','lov3');
		$data = $this->lov3->entities_report_provinsi();
		$this->load->library('table');
		$datatable = array(array('#', 'Provinsi', 'Kabupaten', 'Kecamatan', 'Total Desa BTS Planning', 'Data Total'));
		foreach ($data as $key => $value) {
		   //$tls = $this->lov3->entities_report_total_kabupaten($value->lprov);
		   //foreach ($tls as $tlm => $yaxis) {
			$datatable[] = array(
				$key+1,
				$value->stat_prov,
				$value->dt,
				'Kabupaten',
				$value->dt,
				'Kecamatan',
				'0'
			);
			$total=0;
			$track = $this->lov3->entities_report_kabupaten($value->lprov);
			foreach ($track as $ykey => $yvalue) {
				$tracking = $this->lov3->entities_report_kecamatan($yvalue->lprov, $yvalue->lkab);
				foreach ($tracking as $skey => $svalue) {
					// $total = $skey;
					// if($value->lprov == $yvalue->lprov) { 
						// $total += $skey; 
					// } 
					$tracker = $this->lov3->entities_report_kelurahan($svalue->lprov, $svalue->lkab, $svalue->lkec);
					foreach ($tracker as $tkey => $tvalue) {
						$total_kec += $skey;
						$datatable[] = array(
						 '',
						 $ykey,
						 $yvalue->stat_kab,
						 $total_kec, 
					  	 $svalue->stat_kec,
					  	 $tvalue->itung
						);
					}				
				}
			}			
		}
		//}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
}
