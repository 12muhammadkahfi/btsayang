<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_data extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);

	}
  public function instruksi()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->instruksi();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function tipe()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->tipe();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	public function dp_key()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->dp_key();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function running()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->running();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function dp_warna()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->dp_warna();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	public function tipe_search()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->tipe_search();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function bts()
	{
		$this->load->model('Master_model','mdm');
		$data = $this->mdm->bts();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function cek_data_bts()
	{
		$this->load->model('Master_model','mdm');
		$data = $this->mdm->cek_data_bts();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function get_fails()
	{
		$this->load->model('Master_model','mdm');
		$data = $this->mdm->get_fails();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function list_data_bts()
	{
		$this->load->model('Master_model','mdm');
		$data = $this->mdm->list_data_bts();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function max_data_bts()
	{
		$this->load->model('Master_model','mdm');
		$data = $this->mdm->max_data_bts();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function submit_bts()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_bts();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"BTS {$this->params->id_desa} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"BTS {$this->params->id_desa} gagal disimpan"
			)));
	}
	public function hapus_tipe_x()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_tipe_x();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} gagal dihapus"
			)));
	}
	public function hapus_warna()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->hapus_warna();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Warna {$this->params->tipe_warna} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Warna {$this->params->tipe_warna} gagal dihapus"
			)));
	}
	public function submit_tipe_x()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_tipe_x();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Tipe Pembangunan {$this->params->nama_tipe} gagal disimpan"
			)));
	}

	public function ppk()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->ppk();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	public function submit_ppk()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_ppk();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"PPK {$this->params->nama_ppk} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"PPK {$this->params->nama_ppk} gagal disimpan"
			)));
	}

	public function hapus_ppk()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_ppk();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"PPK {$this->params->nama_ppk} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"PPK {$this->params->nama_ppk} gagal disimpan"
			)));
	}

	public function submit_unitkerja()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_unitkerja();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Unit Kerja {$this->params->nama_unit} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Unit Kerja {$this->params->nama_unit} gagal disimpan"
			)));
	}

	public function submit_key()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_key();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Key {$this->params->key} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Key {$this->params->key} gagal disimpan"
			)));
	}

	public function submit_running()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_running();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Key {$this->params->ket} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Key {$this->params->ket} gagal disimpan"
			)));
	}

	public function submit_warna()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_warna();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Key {$this->params->ket} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Key {$this->params->ket} gagal disimpan"
			)));
	}


	public function hapus_unitkerja()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_unitkerja();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Unit Kerja {$this->params->nama_unit} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Unit Kerja {$this->params->nama_unit} gagal disimpan"
			)));
	}

	public function submit_instruksi()
	{

		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_instruksi();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Deksripsi Instruksi {$this->params->ket} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Deksripsi Instruksi {$this->params->ket} gagal disimpan"
			)));
	}

	public function provinsi()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->provinsi();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	
	
	public function unitkerja()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->unitkerja();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	public function submit_provinsi()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_provinsi();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Provinsi {$this->params->nama_provinsi} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Provinsi {$this->params->nama_provinsi} gagal disimpan"
			)));
	}

	public function hapus_provinsi()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_provinsi();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Provinsi {$this->params->nama_provinsi} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Provinsi {$this->params->nama_provinsi} gagal dihapus"
			)));
	}

	public function kota()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->kota();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	public function submit_kota()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_kota();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Kota {$this->params->nama_kota} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Kota {$this->params->nama_kota} gagal disimpan"
			)));
	}

	public function hapus_kota()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_kota();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Kota {$this->params->nama_kota} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Kota {$this->params->nama_kota} gagal dihapus"
			)));
	}

	public function ruas_jalan()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->ruas_jalan();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function ruas_jalan_provinsi()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->ruas_jalan_provinsi();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function ruas_jalan_provinsi_ppk()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->ruas_jalan_provinsi_ppk();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function unit_kerja_isi()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->unit_kerja_isi();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function ppk_isi()
	{
		$this->load->model('Master_data_model','mdm');
		$data = $this->mdm->ppk_isi();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}

	public function submit_ruas_jalan()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_ruas_jalan();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Ruas jalan {$this->params->nama_ruas_jalan} berhasil disimpan"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Ruas jalan {$this->params->nama_ruas_jalan} gagal disimpan"
			)));
	}

	public function hapus_ruas_jalan()
	{
		$this->load->model('Master_data_model','mdm');
		$do = $this->mdm->submit_ruas_jalan();
		if($do) exit(json_encode(array(
			'status'=>true,
			'message'=>"Ruas jalan {$this->params->nama_ruas_jalan} berhasil dihapus"
			)));
		else exit(json_encode(array(
			'status'=>false,
			'message'=>"Ruas jalan {$this->params->nama_ruas_jalan} gagal dihapus"
			)));
	}

}
