<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	public function data_pengguna()
	{
		$this->load->model('user_model');
		$pengguna = $this->user_model->data_pengguna();
		exit(json_encode(array(
			'status'=>true,
			'data'=>$pengguna
			)));
	}

	public function data_administrator()
	{
		$this->load->model('user_model');
		$pengguna = $this->user_model->data_administrator();
		exit(json_encode(array(
			'status'=>true,
			'data'=>$pengguna
			)));
	}

	public function detail()
	{
		$this->load->model('user_model');
		$pengguna = $this->user_model->detail();
		exit(json_encode(array(
			'status'=>true,
			'data'=>$pengguna
			)));
	}

	public function pulihkan_akun($enc)
	{
		$this->load->model('user_model');
		$pulih = $this->user_model->pulihkan_akun($enc);
		if($pulih){
			redirect('../?pulih=1');
		} else {
			exit("Url tidak lagi sah!");
		}
	}

	public function buat_admin()
	{
		$this->load->model('user_model');
		exit(json_encode($this->user_model->buat_admin()));
	}

	public function forgot_password()
	{
		$this->load->model('user_model');
		exit(json_encode($this->user_model->forgot_password()));
	}

	public function ganti_password()
	{
		$this->load->model('user_model');
		exit(json_encode($this->user_model->ganti_password()));
	}

	public function dashboard()
	{
		$this->load->model('user_model');
		exit(json_encode($this->user_model->dashboard()));
	}

	public function hapus()
	{
		$this->load->model('user_model');
		exit(json_encode($this->user_model->hapus()));
	}

}