<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	// public function index()
	// {

	// 	if(!$this->input->post('email')){
	// 		if(!$this->session->email)
	// 			$this->load->view('welcome_message');
	// 		else				
	// 		$this->load->view('dashboard');
	// 		$this->session->fail_login = false;
	// 		$this->session->aktivasi_sukses = false;
	// 	} else {
	// 		if(strlen(trim($this->input->post('password')))<6){
	// 			$this->session->set_userdata('fail login','Panjang password minimal 6 karakter!');
	// 		//return false;
	// 		}
	// 		$this->load->model('user_model');
	// 		$user = $this->user_model->dologin();
	// 		//$this->session->set_userdata('fail_login',$user->email.'-'.$user->password);
	// 		if($user){
	// 			if(intval($user->pin)==0)
	// 				$this->session->set_userdata((array) $user);
	// 			else
	// 				$this->session->set_userdata('fail_login','Anda belum melakukan aktivasi akun anda, silahkan lakukan aktivasi akun anda terlebih dahulu.');
	// 		} else {
	// 			$this->session->set_userdata('fail_login','Email atau password salah');
	// 		}
	// 		redirect('../');
	// 	}
	// }
	// public function index()
	// {

	// 	if(!$this->input->post('email')){
	// 		if(!$this->session->email)
	// 			$this->load->view('welcome_message');
	// 		else
	// 			$this->load->view('dashboard');
	// 		$this->session->fail_login = false;
	// 		$this->session->aktivasi_sukses = false;
	// 	} else {
	// 		$this->load->model('user_model');
	// 		$user = $this->user_model->dologin();
	// 		if($user){
	// 			if(intval($user->pin)==0)
	// 				$this->session->set_userdata((array) $user);
	// 			else
	// 				$this->session->set_userdata('fail_login','Anda belum melakukan aktivasi akun anda, silahkan lakukan aktivasi akun anda terlebih dahulu.');
	// 		} else {
	// 			$this->session->set_userdata('fail_login','Email atau password salah');
	// 		}
	// 		redirect('../');
	// 	}
	// }
	public function index()
	{
		if(!$this->input->post('email')){
			$this->load->view('welcome_message');
		}else{
			$this->load->model('user_model');
	 		$user = $this->user_model->dologin();
	 		if($user){
	 			$_SESSION['Key'] = $user->ID;
	 			$this->session->set_userdata((array) $user);
	 			$this->session->fail_login = false;
	 			$this->session->aktivasi_sukses = false;	 			
	 			$this->load->view('dashboard');
	 		}else{
	 			redirect('../');
	 		}
		}
	}
	public function testapi()
	{
		$url = "http://bpsdm.pu.go.id/center/pelatihan/api/index.php";
		$private_key ='jfkdaso&^^78932789HKDShudohusdia52719';

		$action = 'notif_pelatihan';

		$params = array(
			'action' => $action,
			'nip' => "196303231989031020",
		);

		exit(json_encode($params));
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('../');
	}

	public function doregister()
	{
		$this->load->model('user_model');
		$this->user_model->doregister();
		redirect('register');
	}

	public function register()
	{
		$data['post'] = $this->session->postdata ? (object) unserialize($this->session->postdata) : false;
		$this->load->view('register',$data);
		if(!$this->session->success_registration) $this->session->sess_destroy();
	}

	public function resend_mail()
	{
		$data['post'] = $this->session->postdata ? (object) unserialize($this->session->postdata) : false;
		$this->load->view('resend_mail',$data);
		$this->session->fail_resend = false;
	}

	public function aktivasi($hash=false)
	{
		$this->load->model('user_model');
		$aktivasi = $this->user_model->aktivasi($hash);
		if($aktivasi){
			$this->session->set_userdata('aktivasi_sukses',1);
			redirect('../');
		} else {
			exit('URL tidak valid atau sudah tidak berlaku.');
		}
	}

	public function do_resend_mail()
	{
		$this->load->model('user_model');
		$resend = $this->user_model->do_resend_mail();
		if(!$resend){
			$this->session->fail_resend = "Email yang anda masukan salah atau sudah diaktivasi sebelumnya.";
			redirect('resend_mail');
		} else {
			redirect('register');
		}
	}
	
	public function tracking()
	{
		if($this->input->post('captcha')&&$this->input->post('ID')){
			if($this->input->post('captcha')!=$this->session->mycaptcha){
				$this->session->fail_captcha = true;
				redirect('tracking');
				exit();
			} else {
				$this->load->model('Permohonan_model','pm');
				$data = $this->pm->tracking();
				if(!$data){
					$this->session->fail_ID = true;
					redirect('tracking');
					exit();
				} else {
					// print_r($data);
					return $this->load->view('tracking_detail', $data);
				}
			}
			exit();
		}
		$this->load->helper('captcha');
 		$fonts = array(
 			'captcha/RiotTon.ttf',
 			'captcha/mechanization/Mechanization.ttf',
 			'captcha/NMFDisplay-Regular.ttf',
 			'captcha/exostencil/Exostencil.otf',
 			'captcha/exostencil/ExostencilItalic.otf'
 			);
        $vals = array(
            'img_path'	 => 'captcha/',
            'img_url'	 => base_url().'captcha/',
            'img_width'	 => 150,
            'img_height' => 68,
            'border' => 0, 
            'font_size' => rand(22,25),
            'expiration' => 7200,
            'font_path'  => $fonts[rand(0,(count($fonts)-1))],
            // 'font_path'  => 'captcha/Rasanskool.ttf',
            'word' => rand(1111,9999),
            'colors' => array(
                'background' => array(56, 35, 114),
                'border' => array(255, 255, 255),
                'text' => array(255, 255, 0),
                'grid' => array(56, 35, 114)
        	)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $data['image'] = $cap['image'];

        // store the captcha word in a session
        $this->session->set_userdata('mycaptcha', $cap['word']);
        $this->load->view('tracking', $data);
        $this->session->fail_captcha = false;
        $this->session->fail_ID = false;
	}

	public function instruksi()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->manualinstruksi();
		return $this->load->view('instruksi', $data);
	}

	public function recaptcha()
	{
		$this->load->helper('captcha');
 		$fonts = array(
 			'captcha/RiotTon.ttf',
 			'captcha/mechanization/Mechanization.ttf',
 			'captcha/NMFDisplay-Regular.ttf',
 			'captcha/exostencil/Exostencil.otf',
 			'captcha/exostencil/ExostencilItalic.otf'
 			);
        $vals = array(
            'img_path'	 => 'captcha/',
            'img_url'	 => base_url().'captcha/',
            'img_width'	 => 150,
            'img_height' => 68,
            'border' => 0, 
            'font_size' => rand(22,25),
            'expiration' => 7200,
            'font_path'  => $fonts[rand(0,(count($fonts)-1))],
            // 'font_path'  => 'captcha/Rasanskool.ttf',
            'word' => rand(1111,9999),
            'colors' => array(
                'background' => array(56, 35, 114),
                'border' => array(255, 255, 255),
                'text' => array(255, 255, 0),
                'grid' => array(56, 35, 114)
        	)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $data['image'] = $cap['image'];

        // store the captcha word in a session
        $this->session->set_userdata('mycaptcha', $cap['word']);
        exit(json_encode($data));
    }

}
