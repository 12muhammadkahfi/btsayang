<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bts_report extends CI_Controller {

	public function export_excel_report_planning()
	{
		$filename="data-rekap_total_expand.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('report_model','lov3');
		$data = $this->lov3->entities_report_provinsi();
		$this->load->library('table');
		$datatable = array(array('#', 'Provinsi', 'Kabupaten', 'Kecamatan', 'Total Desa BTS Planning', 'Data Total','Data Realisasi'));
		foreach ($data as $key => $value) {
		   //$tls = $this->lov3->entities_report_total_kabupaten($value->lprov);
		   //foreach ($tls as $tlm => $yaxis) {
			$datatable[] = array(
				$key+1,
				'',
				$value->stat_prov,
				$value->dt,
				'Kabupaten',
				$value->dt,
				'Kecamatan',
				'0'
			);
			$total=0;
			$track = $this->lov3->entities_report_kabupaten($value->lprov);
			foreach ($track as $ykey => $yvalue) {
				$tracking = $this->lov3->entities_report_kecamatan($yvalue->lprov, $yvalue->lkab);
				foreach ($tracking as $skey => $svalue) {
					// $total = $skey;
					// if($value->lprov == $yvalue->lprov) { 
						// $total += $skey; 
					// } 
					$tracker = $this->lov3->entities_report_kelurahan($svalue->lprov, $svalue->lkab, $svalue->lkec);
					foreach ($tracker as $tkey => $tvalue) {
						$total_kec += $skey;
						$datatable[] = array(
						 '',
						 $ykey,
						 $yvalue->stat_kab,
						 $total_kec, 
					  	 $svalue->stat_kec,
						$tvalue->itung
						);
						$track_real = $this->lov3->entities_report_realisasi($svalue->lprov, $svalue->lkab, $svalue->lkec);
						foreach ($track_real as $ikey => $ivalue) {	
							$total_kec += $skey;
							$datatable[] = array(
						 	'',
						 	$ykey,
						 	$yvalue->stat_kab,
						 	$total_kec, 
					  	 	$svalue->stat_kec,
							$tvalue->itung,
							$ivalue->itung
							);
						}
					}				
				}
			}			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_report_realisasi()
	{
		$filename="data-rekap_total_expand_realisiasi.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('report_model','lov3');
		$data = $this->lov3->entities_report_provinsi_realisasi();
		$this->load->library('table');
		$datatable = array(array('#', 'Provinsi', 'Kabupaten', 'Kecamatan', 'Total Desa BTS Planning', 'Data Total','Data Realisasi'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				'',
				$value->stat_prov,
				'Kabupaten',
				'Kecamatan',
				'0'
			);
			$track = $this->lov3->entities_report_kabupaten_realisasi($value->kprov);
			foreach ($track as $ykey => $yvalue) {
				$tracking = $this->lov3->entities_report_kecamatan_realisasi($yvalue->kprov, $yvalue->kkab);
				foreach ($tracking as $skey => $svalue) {
					$tracker = $this->lov3->entities_report_kelurahan_realisasi($svalue->kprov, $svalue->kkab, $svalue->kkec);
					foreach ($tracker as $tkey => $tvalue) {
						$track_real = $this->lov3->entities_report_realisasi($svalue->kprov, $svalue->kkab, $svalue->kkec);
						foreach ($track_real as $ikey => $ivalue) {	
							$total_kec += $skey;
							$datatable[] = array(
						 	'',
						 	$ykey,
						 	$yvalue->stat_kab,
						 	$total_kec, 
					  	 	$svalue->stat_kec,
							$tvalue->itung,
							$ivalue->itung
							);
						}
					}				
				}
			}			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_report_planning_excel()
	{
		$filename="data-rekap_planning_excel_expand.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('report_model','lov3');
		$data = $this->lov3->entities_report_provinsi_eng();
		$this->load->library('table');
		$datatable = array(array('#', 'Provinsi', 'Kabupaten', 'Kecamatan', 'Total Desa BTS Planning', 'Data Total'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				'',
				$value->provinsi,
				'Kabupaten',
				'Kecamatan',
				'0'
			);
			$track = $this->lov3->entities_report_kabupaten_eng($value->provinsi);
			foreach ($track as $ykey => $yvalue) {
				$tracking = $this->lov3->entities_report_kecamatan_eng($yvalue->provinsi, $yvalue->kabupaten);
				foreach ($tracking as $skey => $svalue) {
					$tracker = $this->lov3->entities_report_kelurahan_eng($svalue->provinsi, $svalue->kabupaten, $svalue->kecamatan);
					foreach ($tracker as $tkey => $tvalue) {
						$total_kec += $skey;
						$datatable[] = array(
						'',
						$ykey,
						$yvalue->kabupaten,
						$total_kec, 
					  	$svalue->kecamatan,
						$tvalue->itung
						);
					}				
				}
			}			
		}
		//}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_report_realisasi_excel()
	{
		$filename="data-rekap_realisasi_excel_expand.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('report_model','lov3');
		$data = $this->lov3->entities_report_provinsi_idn();
		$this->load->library('table');
		$datatable = array(array('#', 'Provinsi', 'Kabupaten', 'Kecamatan', 'Total Desa BTS Planning', 'Data Total'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				'',
				$value->provinsi,
				'Kabupaten',
				'Kecamatan',
				'0'
			);
			$track = $this->lov3->entities_report_kabupaten_idn($value->provinsi);
			foreach ($track as $ykey => $yvalue) {
				$tracking = $this->lov3->entities_report_kecamatan_idn($yvalue->provinsi, $yvalue->kabupaten);
				foreach ($tracking as $skey => $svalue) {
					$tracker = $this->lov3->entities_report_kelurahan_idn($svalue->provinsi, $svalue->kabupaten, $svalue->kecamatan);
					foreach ($tracker as $tkey => $tvalue) {
						$total_kec += $skey;
						$datatable[] = array(
						'',
						$ykey,
						$yvalue->kabupaten,
						$total_kec, 
					  	$svalue->kecamatan,
						$tvalue->itung
						);
					}				
				}
			}			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_report_realisasi_planning()
	{
		$filename="data-rekap_realisasi_planning_excel_expand.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('report_model','lov3');
		$data = $this->lov3->entities_report_kabupaten_my_total();
		$this->load->library('table');
		$datatable = array(array('#', 'Provinsi', 'Kabupaten','', 'Kecamatan','Null','Total Desa BTS Planning','Total Desa BTS Realisasi'
		,'2015','2016','2017','2018','Data Total'));
		foreach ($data as $key => $value) {
		$track_t = $this->lov3->entities_report_kecamatan_my_total($value->provinsi);
			foreach ($track_t as $ykey_t => $yvalue_t) {
			$track_y = $this->lov3->entities_report_kabupaten_my($value->provinsi);
				foreach ($track_y as $ykey_y => $yvalue_y) {
				$datatable[] = array(
					'',
					$value->provinsi,
					'Kabupaten',
					$value->itung ,
					'Kecamatan',
					$yvalue_t->iteung,
					'0',
					$yvalue_y->itung 
				);
		$track = $this->lov3->entities_report_kptn_my($value->provinsi);
		foreach ($track as $ykey => $yvalue) {
			$tracking = $this->lov3->entities_report_kecamatan_my($yvalue->provinsi, $yvalue->kabupaten);
			foreach ($tracking as $skey => $svalue) {
				$tracker = $this->lov3->entities_report_kelurahan_my($svalue->provinsi, $svalue->kabupaten, $svalue->kecamatan);
				foreach ($tracker as $tkey => $tvalue) {
					$trackt = $this->lov3->entities_report_desa_my($svalue->provinsi, $svalue->kabupaten, $svalue->kecamatan);
					foreach ($trackt as $ikey => $ivalue) {
						$track15 = $this->lov3->entities_report_desa_my_five($svalue->provinsi, $svalue->kabupaten, $svalue->kecamatan);
						foreach ($track15 as $ikey15 => $value15) {
						  	$track17 = $this->lov3->entities_report_desa_my_seven($svalue->provinsi, $svalue->kabupaten, $svalue->kecamatan);
						  	foreach ($track17 as $ikey17 => $value17) {
								$track18 = $this->lov3->entities_report_desa_my_eight($svalue->provinsi, $svalue->kabupaten, $svalue->kecamatan);
								foreach ($track18 as $ikey18 => $value18) {
									$track19 = $this->lov3->entities_report_desa_my_nine($svalue->provinsi, $svalue->kabupaten, $svalue->kecamatan);
						  			foreach ($track19 as $ikey19 => $value19) {
									$datatable[] = array(
										'',
										'',
										$yvalue->kabupaten,
										'', 
										$svalue->kecamatan,
										'',  
										$tvalue->itung,
										$ivalue->itung,
										$value15->itung,
										$value17->itung,
										$value18->itung,
										$value19->itung,
										'0'
										);
									}
								}
							}
						}
					}				
				}
			
			}			
			}
		}
	}
	}
	//echo json_encode(array($datatable));
	//Untuk Menghidupkan File Excel Dibawah Ini
	 //echo array($this->table->generate($datatable));
	 //Samapai Baris DIaatas
	  print_r(array($this->table->generate($datatable)));
	}
	public function export_report_realisasi_planning_wrong_bts()
	{
		$this->load->model('report_model','lov3');
		$data = $this->lov3->entities_report_provinsi_my_wrong();
		$this->load->library('table');
		$datatable = array(array('#', 'Provinsi', 'Kabupaten', 'Kecamatan','Kelurahan','Initial','kode','provinsi bts','kabupaten bts','kecamatan bts','Kelurahan BTS'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				$key,
				$value->provinsi,
				$value->kabupaten ,
				$value->kecamatan,
				$value->kelurahan,
				$value->nd ,
				$value->nat,
				$value->prov,
				$value->kab,
				$value->kec,
				$value->kel
			);

		}
	print_r($this->table->generate($datatable));
	}
	public function export_report_realisasi_planning_wrong_number()
	{
		$this->load->model('report_model','lov3');
		$data = $this->lov3->entities_report_number_my_wrong();
		$this->load->library('table');
		$datatable = array(array('#','kode','provinsi bts','kabupaten bts','kecamatan bts','Kelurahan BTS'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				$key,
				$value->no,
				$value->prov,
				$value->kab
			);

		}
		echo json_encode($data);
	// print_r($this->table->generate($datatable));
	}
	//enlide//
}
