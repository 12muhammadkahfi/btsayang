<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class report_controller extends CI_Controller {

	public function uploadfiles()
	{
		foreach ($_FILES['file']['name'] as $key => $value) {
			$arrtype = explode('/', strtolower($_FILES['file']['type'][$key]));
			if($_FILES['file']['size'][$key]<=20000000&&($arrtype[0]==="image"||$arrtype[1]==="pdf")){
				move_uploaded_file($_FILES['file']['tmp_name'][$key], "tmps/{$this->session->ID}/{$value}");
				$_FILES['file']['type'][$key] = $arrtype;
			} else {
				unset($_FILES['file']['name'][$key]);
				unset($_FILES['file']['type'][$key]);
				unset($_FILES['file']['size'][$key]);
				unset($_FILES['file']['tmp_name'][$key]);
				unset($_FILES['file']['error'][$key]);
			}
		}
		exit(json_encode($_FILES));
		// file_put_contents('tmps/debug.txt', json_encode($_FILES['file']['name']));
	}

	public function clear_tmps()
	{
		if(!file_exists("tmps/{$this->session->ID}")){
			mkdir("tmps/{$this->session->ID}");
			chmod("tmps/{$this->session->ID}", 0777);
		}
		array_map('unlink', glob("tmps/{$this->session->ID}/*"));
	}

	
	public function uploaddokumen()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->uploaddokumen();
		if($data) exit(json_encode(array('status'=>true,'ID'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Permohonan anda tidak dapat kami terima.')));
	}
	public function ajukanpermohonan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->ajukanpermohonan();
		if($data) exit(json_encode(array('status'=>true,'ID'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Permohonan anda tidak dapat kami terima.')));
	}
	public function submitpermohonan()
	{
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->submitpermohonan();
		if($data) exit(json_encode(array('status'=>true,'ID'=>$data,'message'=>'Permohonan anda telah kami terima.')));
		else exit(json_encode(array('status'=>false,'message'=>'Permohonan anda tidak dapat kami terima.')));
	}
	public function history()
	{
		$this->load->model('Master_model','pm');
		$data = $this->pm->history();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function count_entitas_kabupaten()
	{
		$this->load->model('Master_model','pm');
		$data = $this->pm->count_entitas_kabupaten();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function count_entitas_kecamatan()
	{
		$this->load->model('Master_model','pm');
		$data = $this->pm->count_entitas_kecamatan();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function count_entitas_desa()
	{
		$this->load->model('Master_model','pm');
		$data = $this->pm->count_entitas_desa();
		exit(json_encode(array('status'=>true,'data'=>$data)));
	}
	public function export_excel()
	{
		$filename="data-provinsi.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->acehrep();
		$this->load->library('table');
		$datatable = array(array('#', 'KodeProv', 'Provinsi', 'Kabupaten','Kecamatan'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				$key+1,
				$value->kdDep,
				$value->Nama
//				$value->total
				);
			$tracking = $this->pm->tracking_aceh($value->kdDep);
			foreach ($tracking as $skey => $svalue) {
				$datatable[] = array(
				$key+1,
				$value->kdDep,
				$value->Nama,
				$svalue->Nama
				);
			$track = $this->pm->tracking_kec_aceh($svalue->kdDep);
			foreach ($track as $kkey => $tsvalue) {
				$datatable[] = array(
				$key+1,
				$value->kdDep,
				$value->Nama,
				$svalue->Nama,
				$tsvalue->Nama
				);	
			}
		}
	}
		echo $this->table->generate($datatable);
		
	}

	public function export_excel_single()
	{
		$filename="data-pengajuan-prov.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->reportaceh();
		$this->load->library('table');
		$datatable = array(array('#', 'KodeProv', 'Provinsi', 'KodeKab', 'Kabupaten', 'KodeKec', 'Kecamatan'));
		foreach ($data as $key => $value) {
			
			$datatable[] = array(
				$key+1,
				$value->kodeprov,
				$value->namaprov,
				$value->kodekab,
				$value->namakab,
				$value->kodekec,
				$value->namakec
				);
			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_rekap_total()
	{
		$filename="data-rekap_total.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->count_entitas_kabupaten();
		$this->load->library('table');
		$datatable = array(array('#', 'KodeProv', 'Provinsi', 'Total Kabupaten', 'Total Kecamatan', 'Total Desa', 'Data Total'));
		foreach ($data as $key => $value) {
				$tracking = $this->pm->count_entitas_kecamatan_parameter($value->kdDep);
				foreach ($tracking as $skey => $svalue) {
				$tracker = $this->pm->count_entitas_desa_parameter($value->kdDep);
				foreach ($tracker as $tkey => $tvalue) {
				$datatable[] = array(
					$tkey+1,
					$value->kdDep,
					$value->Nama,
					$value->ct_kab,
					$svalue->ct_kec,
					$tvalue->ct_desa,
					$value->ct_kab + $svalue->ct_kec + $tvalue->ct_desa
					);
			
				}	
						
				}
			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_rekap_expand()
	{
		$filename="data-rekap_total_expand.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->prov_data();
		$this->load->library('table');
		$datatable = array(array('#', 'KodeProv', 'Provinsi', 'Nama Kabupaten', 'Total Kecamatan', 'Total Desa', 'Data Total'));
		foreach ($data as $key => $value) {
				$track = $this->pm->count_entitas_kabupaten_parameter($value->Prov);
				foreach ($track as $ykey => $yvalue) {
				$tracking = $this->pm->count_entitas_kecamatan_expand($yvalue->Prov, $yvalue->Kab);
				foreach ($tracking as $skey => $svalue) {
				// $tracker = $this->pm->count_entitas_desa_parameter_expand($svalue->Prov, $svalue->Kab, $svalue->Kec);
				// foreach ($tracker as $tkey => $tvalue) {
				 $datatable[] = array(
					$tkey+1,
					$value->kdDep,
					$value->Nama,
					$yvalue->Nama,
					$svalue->Nama
					// $tvalue->Nama
					// $svalue->ct_kec,
					
					// $tvalue->ct_desa,
					// $tvalue->ct_desa
					);
			
				//}	
						
				}
			}
			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_bts()
	{
		$filename="data-rekap_wrap_bts.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->report_bts();
		$this->load->library('table');
		$datatable = array(array('#', 'KodeProv', 'Provinsi', 'Provinsi Lokasi', 'Nama Desa', 'Sumber Data'));
		foreach ($data as $key => $value) {

				$syntax_erno = substr($value->kdDep,0,2);
				$alfa = '.00';
				$track = $this->pm->report_sub_bts($syntax_erno, $alfa);
				foreach ($track as $ykey => $yvalue) {
				$datatable[] = array(
					$key+1,
					$value->kdDep,
					$value->PROV,
					$syntax_erno,
//					$value->dkey,
					//$yvalue->Nama,
					$value->DESA,
					$value->sumber_data
					);
				}
			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_kec($data)
	{
		$filename="data-pengajuan-kec.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->report_prov_kec($data);
		$this->load->library('table');
		$datatable = array(array('#', 'Kode Provinsi', 'Provinsi', 'Kode Kecamatan', 'Kecamatan'));
		foreach ($data as $key => $value) {
			
			$datatable[] = array(
				$key+1,
				$value->kdDep,
				$value->Nama
				);
			$tracking = $this->pm->report_kec_kec($value->kdDep);
			foreach ($tracking as $skey => $svalue) {
				$datatable[] = array(
				$skey+1,
				$value->kdDep,
				$value->Nama,
				$svalue->kdDep,
				$svalue->Nama
				);
			
			}
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_rekap_planning1()
	{
		$filename="data-rekap-planning.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->report_prov_group_plan();
		$this->load->library('table');
		$datatable = array(array('#', 'Jml', 'Provinsi', 'Jml','Kabupaten', 'Jml', 'Kecamatan', 'Kecamatan'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				'#',
				$key+1,
				$value->Nama,
				//$value->total_kab,
				'Kabupaten',
				//$value->total_kec,
				'Kecamatan'
				);
			$tracking = $this->pm->report_kab_group_plan($value->prov);
			foreach ($tracking as $skey => $svalue) {
			$track = $this->pm->report_kec_group_plan($svalue->prov, $svalue->kab);
			foreach ($track as $tkey => $tvalue) {
			$tracker = $this->pm->report_kel_group_plan($tvalue->prov, $tvalue->kab, $tvalue->kec);
			foreach ($tracker as $wkey => $wvalue) {
				$datatable[] = array(
				$skey+1,
				$svalue->Nama
				// $tkey+1,
				// $svalue->Nama,
//				$tvalue->kecamatan,
				// $tvalue->total_kel
				);
			}
			}
			}	
			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
		public function export_excel_rekap_planning()
	{
		$filename="data-rekap-planning.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->report_prov_group_plan();
		$this->load->library('table');
		$datatable = array(array('#', 'Jml', 'Provinsi', 'Jml','Kabupaten', 'Jml', 'Kecamatan', 'Jml'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				'#',
				$key+1,
				$value->Nama,
				'',
				'Kabupaten',
				'',
				'Kecamatan',
				$value->Prov
				);
			$tracking = $this->pm->report_kab_group_plan($value->Prov);
			foreach ($tracking as $skey => $svalue) {
			$track = $this->pm->report_kec_group_plan($svalue->Prov, $svalue->Kab);
			foreach ($track as $tkey => $tvalue) {
			 $tracker = $this->pm->report_kel_group_plan($tvalue->kdDep);
			 foreach ($tracker as $wkey => $wvalue) {
				$datatable[] = array(
				'',
				'',
				'',
				$skey+1,
				$svalue->Nama,
				$tkey+1,
				$tvalue->Nama,
				substr($tvalue->kdDep, 0,8),
				$wvalue->total_kel
				);
			 }	
			}
			}
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_div_kab($data)
	{
		$filename="data-pengajuan-kab.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$var_kab = $data;
		$data = $this->pm->report_prov_kec($data);
		$this->load->library('table');
		$datatable = array(array('#', 'Kode Provinsi', 'Provinsi', 'Kode Kabupaten', 'Kabupaten'));
		foreach ($data as $key => $value) {
			
			$datatable[] = array(
				$key+1,
				$value->kdDep,
				$value->Nama
				);
			$tracking = $this->pm->report_vic_kab_kec($value->kdDep);
			foreach ($tracking as $skey => $svalue) {
				$datatable[] = array(
				$skey+1,
				$value->kdDep,
				$value->Nama,
				$svalue->kdDep,
				$svalue->Nama
				);
			
			}
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_desa($data)
	{
		$filename="data-pengajuan-kec.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->report_prov_kec($data);
		$this->load->library('table');
		$datatable = array(array('#', 'Kode Provinsi', 'Provinsi', 'Kode Desa', 'Nama Desa'));
		foreach ($data as $key => $value) {
			
			$datatable[] = array(
				$key+1,
				$value->kdDep,
				$value->Nama
				);
			$tracking = $this->pm->report_desa_desa($value->kdDep);
			foreach ($tracking as $skey => $svalue) {
				$datatable[] = array(
				$skey+1,
				$value->kdDep,
				$value->Nama,
				$svalue->kdDep,
				$svalue->Nama
				);
			
			}
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_kab($data)
	{
		$filename="data-kecamatan.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$var_bas = $data;
		$data = $this->pm->report_prov_kec($data);
		$this->load->library('table');
		$datatable = array(array('#', 'Kode Provinsi', 'Provinsi','Kode Kabupaten', 'Kabupaten' ,'Kode Kecamatan', 'Kecamatan'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				$key+1,
				//substr($value->kdDep,0,6),
				$value->kdDep,
				$value->Nama,
				$var_bas
				);
			$tracking = $this->pm->report_kab_kab($var_bas);
			foreach ($tracking as $skey => $svalue) {
				$datatable[] = array(
				$skey+1,
				$value->kdDep,
				$value->Nama,
				$svalue->kdDep,
				$svalue->Nama
				);
				$track = $this->pm->report_div_list_kec($svalue->kdDep);
				foreach ($track as $tkey => $tvalue) {
					$datatable[] = array(
					$tkey+1,
					$value->kdDep,
					$value->Nama,
					$svalue->kdDep,
					$svalue->Nama,
					$tvalue->kdDep,
					$tvalue->Nama
				);
				}	
			}
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_sub_desa($data)
	{
		$filename="data-kecamatan.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$var_bas = $data;
		$data = $this->pm->report_prov_kec($data);
		$this->load->library('table');
		$datatable = array(array('#', 'Kode Provinsi', 'Provinsi','Kode Kabupaten', 'Kabupaten' ,'Kode Kecamatan', 'Kecamatan','Kode Desa','Nama Desa'));
		foreach ($data as $key => $value) {
			$datatable[] = array(
				$key+1,
				//substr($value->kdDep,0,6),
				$value->kdDep,
				$value->Nama,
				$var_bas
				);
			$tracking = $this->pm->report_kab_kab($var_bas);
			foreach ($tracking as $skey => $svalue) {
				$datatable[] = array(
				$skey+1,
				$value->kdDep,
				$value->Nama,
				$svalue->kdDep,
				$svalue->Nama
				);
				$track = $this->pm->report_list_kec($var_bas);
				foreach ($track as $tkey => $tvalue) {
					$datatable[] = array(
					$tkey+1,
					$value->kdDep,
					$value->Nama,
					$svalue->kdDep,
					$svalue->Nama,
					$tvalue->kdDep,
					$tvalue->Nama
				);
					$tracker = $this->pm->report_list_desa($tvalue->kdDep);
					foreach ($tracker as $ekey => $evalue) {
						$datatable[] = array(
						$ekey+1,
						$value->kdDep,
						$value->Nama,
						$svalue->kdDep,
						$svalue->Nama,
						$tvalue->kdDep,
						$tvalue->Nama,
						$evalue->kdDep,
						$evalue->Nama
					);
					}	
				}
				
			}
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_sub_kec($data)
	{
		$filename="data-kabupaten.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Master_model','pm');
		$data = $this->pm->report_kab_kec($data);
		$this->load->library('table');
		$datatable = array(array('#', 'Kode Provinsi', 'Provinsi', 'Kode Kabupaten', 'Kabupaten'));
		foreach ($data as $key => $value) {
			
			$datatable[] = array(
				$key+1,
				$value->kdDep,
				$value->Nama
				);
			$tracking = $this->pm->report_sub_kec($value->kdDep);
			foreach ($tracking as $skey => $svalue) {
				$datatable[] = array(
				$skey+1,
				$value->kdDep,
				$value->Nama,
				$svalue->kdDep,
				$svalue->Nama
				);
			
			}
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}
	public function export_excel_tipe($data)
	{
		$filename="data-pengajuan-tipe.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->data_perijinan_laporan_tipe($data);
		$this->load->library('table');
		$datatable = array(array('#', 'ID Pengajuan', 'Tanggal Pengajuan','Pembuat Pengajuan','Email','Perihal','Ruas Jalan','Provinsi','Contact Person','Telp','Status','Tahapan Proses','Satuan Unit Kerja','PPK','Jenis Permohonan'));
		foreach ($data as $key => $value) {
			switch ($value->status) {
				case 'PENDING':
					$status = "Menunggu Berkas";
					break;
				
				case 'PROCESSING':
					$status = "Dalam Proses";
					break;
				
				case 'ACCEPT':
					$status = "Rekomtek diterbitkan";
					break;
				
				case 'REJECT':
					$status = "Ditolak";
					break;
				
				default:
					$status = "";
					break;
			}
			$datatable[] = array(
				$key+1,
				$value->ID,
				date("d/m/Y",strtotime($value->tanggal_data)),
				$value->nama_lengkap,
				$value->email,
				$value->perihal,
				$value->nama_ruas_jalan,
				$value->nama_provinsi,
				$value->contact_person,
				"{$value->telp}",
				$status,
				$value->apv_status,
				$value->nama_unit,
				$value->nama_ppk,
				$value->nama_tipe
				);
			
		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}

	public function export_excel_double()
	{
		$filename="data-pengajuan.xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->model('Permohonan_model','pm');
		$data = $this->pm->data_tracking();
		$this->load->library('table');
		$datatable = array(array('#', 'ID Pengajuan', 'Tanggal Proses Tahapan','Pembuat Proses Tahapan','Status Tahapan','Keterangan'));
		foreach ($data as $key => $value) {
			
			$datatable[] = array(
				$key+1,
				$value->ID,
				date("d/m/Y",strtotime($value->tanggal)),
				$value->nama_lengkap,
				$value->wrstatus,
				$value->remark
				);

		}
		echo $this->table->generate($datatable);
		// print_r($data);
	}

}
