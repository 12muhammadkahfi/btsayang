<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_data_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}

	public function tipe_search()
	{
		$this->db->select('tipe.*, user.nama_lengkap as username');
		$this->db->join('user', 'tipe.creator = user.ID');
		$this->db->where('tipe.status', 1);
		$this->db->order_by('nama_tipe', 'asc');
		return $this->db->get('tipe')->result();
	}

	public function tipe()
	{
		$this->db->select('tipe.*');
	//	$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('tipe.status', 1);
		$this->db->order_by('nama_tipe', 'asc');
		return $this->db->get('tipe')->result();
	}

	public function instruksi()
	{
		$this->db->select('instruksi.*');
	//	$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->order_by('judul', 'asc');
		return $this->db->get('instruksi')->result();
	}

	public function dp_key()
	{
		$this->db->select('dp_key.*, user.nama_lengkap as username');
		$this->db->join('user', 'dp_key.creator = user.ID');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('dp_key')->result();
	}
	public function running()
	{
		$this->db->select('running.*, user.nama_lengkap as username');
		$this->db->join('user', 'running.creator = user.ID');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('running')->result();
	}

	public function dp_warna()
	{
		$this->db->select('warna.*');
		//$this->db->join('user', 'dp_key.creator = user.ID');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('warna')->result();
	}

	public function submit_tipe_x()
	{
		if(!isset($this->params->ID)){
			$this->params->id_tipe = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('tipe', $this->params);
		} else {
			return $this->db->where('ID', $this->params->ID)->update('tipe',$this->params);
		}
	}
	public function hapus_warna()
	{
		return $this->db->where('ID', $this->params->ID)->delete('warna',$this->params);		
	}
	public function submit_instruksi()
	{

			$this->params->tgl = date("Y-m-d");
			$this->params->tglwaktu = date("Y-m-d H:i:s");
			return $this->db->insert('instruksi', $this->params);
	}
	public function provinsi()
	{
		$this->db->select('provinsi.*, user.nama_lengkap as username');
		$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('provinsi.status', 1);
		$this->db->order_by('nama_provinsi', 'asc');
		return $this->db->get('provinsi')->result();
	}

	public function unitkerja()
	{
		$this->db->select('unitkerja.*, user.nama_lengkap as username, provinsi.nama_provinsi');
		$this->db->join('user', 'unitkerja.creator = user.ID');
		$this->db->join('provinsi', 'unitkerja.id_provinsi = provinsi.ID');
		$this->db->where('unitkerja.status', 1);
		$this->db->order_by('nama_unit', 'asc');
		return $this->db->get('unitkerja')->result();
	}

	public function ppk()
	{
		$this->db->select('ppk.*, user.nama_lengkap as username, unitkerja.nama_unit');
		$this->db->join('user', 'ppk.creator = user.ID');
		$this->db->join('unitkerja', 'ppk.id_unit = unitkerja.ID');
		$this->db->where('ppk.status', 1);
		$this->db->order_by('nama_ppk', 'asc');
		return $this->db->get('ppk')->result();
	}

	public function submit_ppk()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_date = date("Y-m-d");
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('ppk', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('ppk',$this->params);
		}
	}
	public function submit_bts()
	{
		$data->id_desa = $this->params->id_desa;
		$data->freq = $this->params->freq;
		$data->created_date = date("Y-m-d");
		$data->created_time = date("Y-m-d H:i:s");
		if(!isset($this->params->ID)){
			$alfa = $this->db->where('id_desa',$this->params->id_desa)->get('acehbts')->num_rows();
			if($alfa){
				return false;
			}
			return $this->db->insert('acehbts', $data);
		} else {
			return $this->db->where('ID', $this->params->ID)->update('acehbts', $data);
		}
	}
	public function submit_provinsi()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('provinsi', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('provinsi',$this->params);
		}
	}

	public function submit_unitkerja()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_date = date("Y-m-d");
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('unitkerja', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('unitkerja',$this->params);
		}
	}

	public function submit_key()
	{
		if(!isset($this->params->hash_id)){
			$this->db->truncate('dp_key');
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_date = date("Y-m-d");
			$this->params->created_time = date("Y-m-d H:i:s");

			return $this->db->insert('dp_key', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('dp_key',$this->params);
		}
	}

	public function submit_running()
	{
		if(!isset($this->params->hash_id)){
			$this->db->truncate('running');
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_date = date("Y-m-d");
			$this->params->created_time = date("Y-m-d H:i:s");

			return $this->db->insert('running', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('running',$this->params);
		}
	}

	public function submit_warna()
	{
		if(!isset($this->params->hash_id)){
			$this->db->where('tipe', $this->params->tipe)->delete('warna');
			$this->params->hash_id = uniqid();
			//$this->params->creator = $this->session->ID;
			//$this->params->created_date = date("Y-m-d");
			//$this->params->created_time = date("Y-m-d H:i:s");

			return $this->db->insert('warna', $this->params);
		} else {
			$this->db->where('tipe', $this->params->tipe)->delete('warna');
			$this->params->hash_id = uniqid();
			return $this->db->insert('warna', $this->params);
//			return $this->db->where('ID', $this->params->ID)->update('warna',$this->params);
		}
	}

	public function kota()
	{
		$this->db->select('kota.*, user.nama_lengkap as username, user.email, provinsi.nama_provinsi');
		$this->db->join('user', 'kota.creator = user.ID');
		$this->db->join('provinsi', 'kota.id_provinsi = provinsi.ID');
		if(isset($this->params->id_provinsi)&&$this->params->id_provinsi) $this->db->where('kota.id_provinsi', $this->params->id_provinsi);
		$this->db->where('kota.status', 1);
		$this->db->order_by('nama_kota', 'asc');
		return $this->db->get('kota')->result();
	}

	public function submit_kota()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('kota', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('kota',$this->params);
		}
	}

	 public function ruas_jalan_provinsi()
	 {
	 	$this->db->select('ruas_jalan.*, user.nama_lengkap as username, user.email, ppk.nama_ppk');
		$this->db->join('user', 'ruas_jalan.creator = user.ID');
		$this->db->join('ppk', 'ruas_jalan.id_ppk = ppk.ID');
		if(isset($this->params->id_ppk)&&$this->params->id_ppk) $this->db->where('ruas_jalan.id_ppk', $this->params->id_ppk);
		$this->db->where('ruas_jalan.status', 1);
		$this->db->order_by('nama_ruas_jalan', 'asc');
		return $this->db->get('ruas_jalan')->result();
	 }

	 public function ruas_jalan_provinsi_ppk()
	 {
	 	$this->db->select('ruas_jalan.*, user.nama_lengkap as username, user.email, ppk.nama_ppk');
		$this->db->join('user', 'ruas_jalan.creator = user.ID','left');
		$this->db->join('ppk', 'ruas_jalan.id_ppk = ppk.ID','left');
		if(isset($this->params->id_ppk)&&$this->params->id_ppk) $this->db->where('ruas_jalan.id_ppk', $this->params->id_ppk);
		$this->db->where('ruas_jalan.status', 1);
		$this->db->order_by('nama_ruas_jalan', 'asc');
		return $this->db->get('ruas_jalan')->result();
	 }

	public function unit_kerja_isi()
	 {
	 	$this->db->select('unitkerja.*, provinsi.nama_provinsi');
		//$this->db->join('user', 'unitkerja.creator = user.ID');
		$this->db->join('provinsi', 'unitkerja.id_provinsi = provinsi.ID');
		if(isset($this->params->id_provinsi)&&$this->params->id_provinsi) $this->db->where('unitkerja.id_provinsi', $this->params->id_provinsi);
		$this->db->where('unitkerja.status', 1);
		$this->db->order_by('nama_unit', 'asc');
		return $this->db->get('unitkerja')->result();
	}
	public function ppk_isi()
	 {
	 	$this->db->select('ppk.*, unitkerja.nama_unit');
		//$this->db->join('user', 'unitkerja.creator = user.ID');
		$this->db->join('unitkerja', 'ppk.id_unit = unitkerja.ID');
		if(isset($this->params->id_unit)&&$this->params->id_unit) $this->db->where('ppk.id_unit', $this->params->id_unit);
		$this->db->where('ppk.status', 1);
		$this->db->order_by('nama_ppk', 'asc');
		return $this->db->get('ppk')->result();
	}

	public function ruas_jalan()
	{
		$this->db->select('ruas_jalan.*');
		//$this->db->join('user', 'ruas_jalan.creator = user.ID');
		//$this->db->join('provinsi', 'ruas_jalan.id_provinsi = provinsi.ID','left');
		//$this->db->join('kota', 'ruas_jalan.id_kota = kota.ID','left');
		$this->db->where('ruas_jalan.status', 1);
		$this->db->order_by('nama_ruas_jalan', 'asc');
		return $this->db->get('ruas_jalan')->result();
	}

	public function submit_ruas_jalan()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			$this->params->remark = "None";
			return $this->db->insert('ruas_jalan', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('ruas_jalan',$this->params);
		}
	}

}
