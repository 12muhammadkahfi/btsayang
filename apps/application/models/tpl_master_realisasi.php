<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tpl_master_realisasi extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}
	public function realisasi_bts()
	{
		$this->db->select('realisasibts.*');
		$this->db->order_by('Provinsi','asc');
		return $this->db->get('realisasibts')->result();
	}
	public function realisasi_bts_kemendagri()
	{
		$this->db->select('realisasibts.Periode, realisasibts.Batch, realisasibts.Provinsi, realisasibts.Kabupaten, realisasibts.Kecamatan, kecamatan.kdDep, kecamatan.Nama, count(Desa) as Total, kabupaten.Nama as kab_dagri, provinsi.Nama as prov_dagri');
		$this->db->join('Kecamatan', 'kecamatan.Nama = realisasibts.Kecamatan','left');
		$this->db->join('Kabupaten', 'kabupaten.kab = kecamatan.kab and kabupaten.prov = kecamatan.prov','left');
		$this->db->join('provinsi', 'provinsi.prov = kabupaten.prov','left');
		$this->db->order_by('kecamatan.Nama','asc');
		$this->db->group_by('realisasibts.kecamatan');
		return $this->db->get('realisasibts')->result();
	}

}
