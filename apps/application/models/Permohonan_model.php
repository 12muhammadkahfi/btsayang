<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permohonan_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}

	public function maksimal()
	{
		$this->db->select('max(perijinan.ID), perijinan.id_perijinan');
		$this->db->where('perijinan.id_perijinan', 1);
		$this->db->order_by('nama_tipe', 'asc');
		return $this->db->get('tipe')->result();
	}
	public function groupperijinan()
	{
		$this->db->select('perijinan.*, user.nama_lengkap as username');
		$this->db->join('user', 'perijinan.creator = user.ID');
		$this->db->group_by('apv_status');
		if(intval($this->session->level)==5){
			$this->db->where('perijinan.creator', $this->session->ID);
		}
		return $this->db->get('perijinan')->result();
	}
	public function uploaddokumen()
	{
		$data = $this->params;
		$data->attachments = serialize($this->params->attachments);
		//$data->desc = serialize($data->desc);
		$data->ID = $this->generatekey();
		$data->tgl = date("Y-m-d");
		$data->tglwaktu = date("Y-m-d H:i:s");
		$insert = $this->db->insert('instruksi_upload',$data);
		if($insert){
			return $data->ID;
		} else {
			return false;
		}
	}
	public function ajukanpermohonan()
	{
		
		$data->ruas_jalan = $this->params->ruas_jalan;
		$data->perihal = $this->params->perihal;
		$data->keterangan = $this->params->keterangan;
		$data->nomor_surat = $this->params->nomor_surat;
		$data->jenis_permohonan = $this->params->jenis_permohonan;
		$data->contact_person = $this->params->contact_person;
		$data->telp = $this->params->telp;
		$data->ppk = $this->params->ppk;
		$data->unit_kerja = $this->params->unit_kerja;
		$data->attachments = serialize($this->params->attachments);
		$data->ID = $this->generateid();
		$data->creator = $this->session->ID;
		$data->created_time = date("Y-m-d H:i:s");
		$tgl = explode('/', $this->params->tanggal_surat);
		$data->tanggal_surat = "{$tgl[2]}-{$tgl[1]}-{$tgl[0]}";
		$data->apv_status = 'TERIMA';
		if(isset($this->params->tanggal_data)&&$this->params->tanggal_data) {
			$tgl = explode('/', $this->params->tanggal_data);
			$data->tanggal_data = "{$tgl[2]}-{$tgl[1]}-{$tgl[0]}";
		} else {
			$data->tanggal_data = date("Y-m-d");
		}
		$insert = $this->db->insert('perijinan',$data);
		if($insert){
			$this->db->insert('tracking',array(
				'remark'=>"Permohonan dibuat oleh {$this->session->nama_lengkap} ({$this->session->email})",
				'attachment'=>serialize($this->params->attachments),
				'creator'=>$this->session->ID,
				'created_time'=>$data->tanggal_data." ".date("H:i:s"),
				'tanggal'=>$data->tanggal_data,
				'id_perijinan'=>$data->ID
				));
			if(!file_exists("attachments/{$data->ID}")) mkdir("attachments/{$data->ID}");
			chmod("attachments/{$data->ID}", 0777);
			foreach (unserialize($data->attachments) as $key => $value) {
				$me = (object) $value;
				rename("tmps/{$this->session->ID}/{$me->name}","attachments/{$data->ID}/{$me->name}");
			}
			
			$perijinan = $this->db->where('ID',$data->ID)->get('perijinan')->row();
			$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Pengajuan permohonan perizinan perihal '".$this->params->perihal."'. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>"bbpjn6@gmail.com",
			'email_to_name'=>"ADMINISTRATOR BBPJN VI",
			'judul'=>'Informasi Pengajuan Permohonan Perizinan',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
			return $data->ID;
		} else {
			return false;
		}
	}

	public function submitpermohonan()
	{
		$data = $this->params;
		$data->attachments = serialize($data->attachments);
		$data->ID = $this->generateid();
		$data->creator = $this->session->ID;
		$data->created_time = date("Y-m-d H:i:s");
		$tgl = explode('/', $data->tanggal_surat);
		$data->tanggal_surat = "{$tgl[2]}-{$tgl[1]}-{$tgl[0]}";
		if(isset($data->tanggal_data)&&$data->tanggal_data) {
			$tgl = explode('/', $data->tanggal_data);
			$data->tanggal_data = "{$tgl[2]}-{$tgl[1]}-{$tgl[0]}";
		} else {
			$data->tanggal_data = date("Y-m-d");
		}
		$insert = $this->db->insert('perijinan',$data);
		if($insert){
			$this->db->insert('tracking',array(
				'remark'=>"Permohonan dibuat oleh {$this->session->nama_lengkap} ({$this->session->email})",
				'creator'=>$this->session->ID,
				'created_time'=>$data->tanggal_data." ".date("H:i:s"),
				'tanggal'=>$data->tanggal_data,
				'id_perijinan'=>$data->ID
				));
			if(!file_exists("attachments/{$data->ID}")) mkdir("attachments/{$data->ID}");
			chmod("attachments/{$data->ID}", 0777);
			foreach (unserialize($data->attachments) as $key => $value) {
				$me = (object) $value;
				rename("tmps/{$this->session->ID}/{$me->name}","attachments/{$data->ID}/{$me->name}");
			}
			
			$perijinan = $this->db->where('ID',$data->ID)->get('perijinan')->row();
			$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
			$edata["tracking"] = $this->db->select('tracking.*,user.nama_lengkap')->
								join('user','tracking.creator=user.ID')->
								where('id_perijinan',$data->ID)->
								order_by('tracking.tanggal','ASC')->
								order_by('tracking.ID','ASC')->
								get('tracking')->result();
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Informasi pernerimaan berkas permohonan perizinan. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Penerimaan Penerimaan Permohonan Perizinan',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));

			// $edata["nama"] = $user->nama_lengkap;
			// $edata["pembuka"] = "Pengajuan permohonan pemanfaat ruang milik jalan anda perihal <strong>{$perijinan->perihal}</strong> telah kami terima dengan nomor pengajuan <strong>{$perijinan->ID}</strong>. Tahap selanjutnya yang harus Anda lakukan adalah dengan menyerahkan berkas/dokumen yang dibutuhkan ke kantor BBPJN6.";
			// $edata["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			// $pesan = $this->load->view('email/status', $edata, true);
			// $this->aang->emailaang(array(
			// 	//'email_natuna'=>$user->email,
			// 	//'email_natuna_name'=>$user->nama_lengkap,
			// 	//'judul'=>'Update Status Terbaru | '.$perijinan->perihal,
			// 	//'pesan'=>$pesan
			// 	'email_natuna'=>'riyadafif21@yahoo.com',
			// 	'email_natuna_name'=>'riyadafif21@yahoo.com',
			// 	'judul'=>'Update Status Terbaru | '.$perijinan->perihal,
			// 	'pesan'=>$pesan
			// 	));
			return $data->ID;
		} else {
			return false;
		}
	}

	public function kirimdataemail($data)
	{
		$data["email_sender"] = "bbpjn6@gmail.com";
        $data["email_sender_name"] = "ADMINISTRATOR BBPJN6 (NO REPLY)";
		$this->load->library('email');
		$this->email->from($data["email_sender"], $data["email_sender_name"]);
		$this->email->to($data['email_to'],$data['email_to_name']);
		$this->email->subject($data['judul']);
		$this->email->message($data['pesan']);
		//$buffer, 'attachment', 'report.pdf', 'application/pdf'
		//$this->email->attach($data['file']);
		$this->email->attach($data['file']);
		$this->email->send();
		//if ($this->email->send())
		//	echo "Mail Sent!";
		//else
		//	echo "There is error in sending mail!";
	}

	private function generateid()
	{
		for($i=0;$i<=10;$i++){
			$random = rand(1000000000,9999999999);
			$check = $this->db->where('ID',$random)->get('perijinan')->num_rows();
			if(!$check) break;
			else continue;
		}
		return $random;
	}
	private function generatekey()
	{
		for($i=0;$i<=10;$i++){
			$random = rand(1000000000,9999999999);
			$check = $this->db->where('ID',$random)->get('instruksi_upload')->num_rows();
			if(!$check) break;
			else continue;
		}
		return $random;
	}

	public function detail()
	{
		if($this->session->level==='5'){
			$this->db->where('ID',$this->params->ID)->update('perijinan',array('rw_client'=>'1'));
		}
		if($this->session->level==='3'){
			$this->db->where('ID',$this->params->ID)->update('perijinan',array('rw_user'=>'1'));
		}
		$data['detail'] = $this->db->select('perijinan.*, ruas_jalan.nama_ruas_jalan, provinsi.nama_provinsi')->
						  join('ruas_jalan','perijinan.ruas_jalan=ruas_jalan.ID')->
						  join('unitkerja','perijinan.unit_kerja=unitkerja.ID')->
						  join('provinsi','unitkerja.id_provinsi=provinsi.ID')->
						  where('perijinan.ID',$this->params->ID)->get('perijinan')->row();
		if(!$data['detail']) return false;
		$data['detail']->attachments = (array) unserialize($data['detail']->attachments);
		$tracking_attachment = $this->db->where(array(
			'id_perijinan'=>$this->params->ID,
			'attachment !='=>''
			))->get('tracking')->result();
		if($tracking_attachment){
			foreach ($tracking_attachment as $key => $value) {
				$data['detail']->attachments[] = array(
					'name'=>"{$value->attachment}",
					'description'=>$value->remark,
					'ket' => $value->wrstatus
					);
			}
		}

		$data['tracking'] = $this->db->select('tracking.*, user.nama_lengkap, DATEDIFF(curdate(),tanggal) AS tgl')->
							join('user','tracking.creator=user.ID')->
							where('id_perijinan',$data['detail']->ID)->
							//order_by('tracking.tanggal','ASC')->
							order_by('tracking.ID','ASC')->
							get('tracking')->result();

		$data['dp_key'] = $this->db->select('log_view.*')->
								get('log_view')->result();


		return $data;
	}
	public function detaildokumen()
	{
		$data['detail'] = $this->db->select('instruksi_upload.*,')->
						  where('instruksi_upload.ID',$this->params->ID)->get('instruksi_upload')->row();
		if(!$data['detail']) return false;
		$data['detail']->attachments = (array) unserialize($data['detail']->attachments);
		
		return $data;
	}

	public function detail_print($ID)
	{
		
		$data = $this->db->select('user.*, ruas_jalan.nama_ruas_jalan, provinsi.nama_provinsi, ppk.nama_ppk, unitkerja.nama_unit, tipe.nama_tipe, perijinan.*')->
			  	join('ppk','perijinan.ppk=ppk.ID','left')->
			  	join('ruas_jalan','ppk.ID=ruas_jalan.id_ppk','left')->
			  	join('unitkerja','perijinan.unit_kerja=unitkerja.ID','left')->
			  	join('provinsi','unitkerja.id_provinsi=provinsi.ID','left')->
			  	join('tipe','perijinan.jenis_permohonan=tipe.ID','left')->
			  	join('user','perijinan.creator=user.ID','left')->
			  	where('perijinan.ID',$ID)->get('perijinan')->row();
		return $data;
	}

	public function tracking_only($id_perijinan)
	{
		return  $this->db->select('tracking.*,user.nama_lengkap,user.email')->
				join('user','tracking.creator=user.ID')->
				where('id_perijinan',$id_perijinan)->
				order_by('tracking.tanggal','ASC')->
				order_by('tracking.ID','ASC')->
				get('tracking')->result();
	}

	public function tracking()
	{
		$data['detail'] = $this->db->select('perijinan.*, ruas_jalan.nama_ruas_jalan, provinsi.nama_provinsi, user.nama_lengkap')->
						  join('ruas_jalan','perijinan.ruas_jalan=ruas_jalan.ID')->
						  join('provinsi','ruas_jalan.id_provinsi=provinsi.ID')->
						  join('user','perijinan.creator=user.ID')->
						  where('perijinan.ID',$this->input->post('ID',true))->get('perijinan')->row();
		if(!$data['detail']) return false;
		$data['detail']->attachments = (array) unserialize($data['detail']->attachments);
		$data['tracking'] = $this->db->select('tracking.*,user.nama_lengkap')->
							join('user','tracking.creator=user.ID')->
							where('id_perijinan',$data['detail']->ID)->
							order_by('tracking.tanggal','ASC')->
							order_by('tracking.ID','ASC')->
							get('tracking')->result();
		foreach ($data['tracking'] as $key => $value) {
			$data['tracking'][$key]->created_time = date("d/m/Y H:i",strtotime($value->created_time));
		}
		return $data;
	}

	public function manualinstruksi()
	{
		return $this->db->select('instruksi.*')->get('instruksi')->result();
		//return $data;
	}

	public function data_perijinan()
	{
		$this->db->select('user.*, ruas_jalan.nama_ruas_jalan, provinsi.nama_provinsi, ppk.nama_ppk, unitkerja.nama_unit, tipe.nama_tipe, perijinan.*');
		$this->db->join('ppk','perijinan.ppk=ppk.ID','left');
		$this->db->join('ruas_jalan','ppk.ID=ruas_jalan.id_ppk','left');
		$this->db->join('unitkerja','perijinan.unit_kerja=unitkerja.ID','left');
		$this->db->join('provinsi','unitkerja.id_provinsi=provinsi.ID','left');
		$this->db->join('tipe','perijinan.jenis_permohonan=tipe.ID','left');
		$this->db->join('user','perijinan.creator=user.ID','left');
		$this->db->order_by('perijinan.created_time', 'DESC');
		$this->db->group_by('perijinan.ID');
		if(intval($this->session->level)==5){
			$this->db->where('perijinan.creator', $this->session->ID);
		}
		return $this->db->get('perijinan')->result();
	}
	public function data_perijinan_group()
	{
		$this->db->select('user.*, ruas_jalan.nama_ruas_jalan, provinsi.nama_provinsi, ppk.nama_ppk, unitkerja.nama_unit, tipe.nama_tipe, perijinan.*');
		$this->db->join('ppk','perijinan.ppk=ppk.ID','left');
		$this->db->join('ruas_jalan','ppk.ID=ruas_jalan.id_ppk','left');
		$this->db->join('unitkerja','perijinan.unit_kerja=unitkerja.ID','left');
		$this->db->join('provinsi','unitkerja.id_provinsi=provinsi.ID','left');
		$this->db->join('tipe','perijinan.jenis_permohonan=tipe.ID','left');
		$this->db->join('user','perijinan.creator=user.ID','left');
		if($this->params->apv_status<>''){
			if(isset($this->params->apv_status)&&$this->params->apv_status) $this->db->where('perijinan.apv_status', $this->params->apv_status);			
		}
		if(intval($this->session->level)==5){
			$this->db->where('perijinan.creator', $this->session->ID);
		}
		$this->db->order_by('perijinan.created_time', 'DESC');
		$this->db->group_by('perijinan.ID');
		return $this->db->get('perijinan')->result();
	}
	public function data_perijinan_laporan_tipe($data)
	{
		$this->db->select('user.*, ruas_jalan.nama_ruas_jalan, provinsi.nama_provinsi, ppk.nama_ppk, unitkerja.nama_unit, tipe.nama_tipe, perijinan.*');
		$this->db->join('ppk','perijinan.ppk=ppk.ID','left');
		$this->db->join('ruas_jalan','ppk.ID=ruas_jalan.id_ppk','left');
		$this->db->join('unitkerja','perijinan.unit_kerja=unitkerja.ID','left');
		$this->db->join('provinsi','unitkerja.id_provinsi=provinsi.ID','left');
		$this->db->join('tipe','perijinan.jenis_permohonan=tipe.ID','left');
		$this->db->join('user','perijinan.creator=user.ID','left');
		if($data<>''){
			if(isset($data)&&$data) $this->db->where('perijinan.apv_status', $data);			
		}
		if(intval($this->session->level)==5){
			$this->db->where('perijinan.creator', $this->session->ID);
		}
		$this->db->order_by('perijinan.created_time', 'DESC');
		$this->db->group_by('perijinan.ID');
		return $this->db->get('perijinan')->result();
	}
	public function data_tracking()
	{
		$this->db->select('tracking.*,user.nama_lengkap,user.email');
		$this->db->join('user','tracking.creator=user.ID');
		$this->db->order_by('tracking.tanggal','ASC');
		$this->db->order_by('tracking.ID','ASC');
				
		if(intval($this->session->level)==5){
			$this->db->where('tracking.creator', $this->session->ID);
		}
		return $this->db->get('tracking')->result();
	}
	public function uploaddok()
	{
		$this->db->select('instruksi_upload.*');
		$this->db->order_by('instruksi_upload.ID', 'DESC');
		return $this->db->get('instruksi_upload')->result();
	}

	public function color_data($key)
	{
		$this->db->select('warna.tipe_warna');
		$this->db->where('warna.dok',$key);
		//$this->db->order_by('warna.ID', 'DESC');
		return $this->db->get('warna')->result();	
	}

	public function durasi_data($key)
	{
		$this->db->select('warna.durasi');
		$this->db->where('warna.dok',$key);
		//$this->db->order_by('warna.ID', 'DESC');
		return $this->db->get('warna')->result();	
	}

	public function submit_berkas_ekspose()
	{
		if(isset($this->params->filename)&&$this->params->filename){
			if(file_exists("tmps/{$this->session->ID}/{$this->params->filename}")) 
				rename("tmps/{$this->session->ID}/{$this->params->filename}","attachments/{$this->params->ID}/{$this->params->filename}");
			else
				unset($this->params->filename);
		}
		if($this->session->level!=='5'){
			$this->db->where('ID',$this->params->ID)->update('perijinan',array('rw_client'=>'0'));
		}
		$update = $this->db->where('ID',$this->params->ID)->update('perijinan',array(
			'status'=>'PROCESSING', 'apv_status'=>'EKSPOSE'
			));
		if($update){
			$warna = $this->db->where('tipe','EKSPOSE')->get('warna')->row();
			//$data['detail'] = $this->db->select('warna.*')->
			//				where('warna.tipe','EKSPOSE')->get('warna')->row();
			//if(!$warna) return false;
			$this->db->insert('tracking',array(
				'remark'=>"Berkas / dokumen diterima dari {$this->params->pengantar_berkas}",
				'creator'=>$this->session->ID,
				'created_time'=>date("Y-m-d H:i:s"),
				'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
				'id_perijinan'=>$this->params->ID,
				'attachment'=>isset($this->params->filename) ? $this->params->filename : '',
				'wrstatus'=>'EKSPOSE',
				'stp'=>'1',
				'dur'=> $warna->durasi,
				'warna'=> $warna->tipe_warna,
				));
			$perijinan = $this->db->where('ID',$this->params->ID)->get('perijinan')->row();
			$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Perubahan Status Pernerimaan Berkas Menjadi Ekspose berdasarkan nomor '".$this->params->ID."'. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Perubahan Status Perizinan Ke Ekspose',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
			return true;
		}
		else return false;
	}

	public function submit_penerimaan_berkas()
	{
		if(isset($this->params->filename)&&$this->params->filename){
			if(file_exists("tmps/{$this->session->ID}/{$this->params->filename}")) 
				rename("tmps/{$this->session->ID}/{$this->params->filename}","attachments/{$this->params->ID}/{$this->params->filename}");
			else
				unset($this->params->filename);
		}
		if($this->session->level!=='5'){
			$this->db->where('ID',$this->params->ID)->update('perijinan',array('rw_client'=>'0'));
		}
		$update = $this->db->where('ID',$this->params->ID)->update('perijinan',array(
			'status'=>'PROCESSING'
			));
		if($update){
			$this->db->insert('tracking',array(
				'remark'=>"Berkas / dokumen diterima dari {$this->params->pengantar_berkas}",
				'creator'=>$this->session->ID,
				'created_time'=>date("Y-m-d H:i:s"),
				'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
				'id_perijinan'=>$this->params->ID,
				'attachment'=>isset($this->params->filename) ? $this->params->filename : '',
				));
			$perijinan = $this->db->where('ID',$this->params->ID)->get('perijinan')->row();
			$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
			$data['tracking'] = $this->db->select('tracking.*,user.nama_lengkap')->
								join('user','tracking.creator=user.ID')->
								where('id_perijinan',$this->params->ID)->
								order_by('tracking.tanggal','ASC')->
								order_by('tracking.ID','ASC')->
								get('tracking')->result();
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Penerimaan berkas permohonana perizinan. Dengan nomor perizinan '".$this->params->ID."'. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Penerimaan Berkas Permohonan Perizinan',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
			return true;
		}
		else return false;
	}

	public function submit_status()
	{
		if(isset($this->params->filename)&&$this->params->filename){
			if(file_exists("tmps/{$this->session->ID}/{$this->params->filename}")) 
				rename("tmps/{$this->session->ID}/{$this->params->filename}","attachments/{$this->params->ID}/{$this->params->filename}");
			else
				unset($this->params->filename);
		}
		if($this->session->level!=='5'){
			$this->db->where('ID',$this->params->ID)->update('perijinan',array('rw_client'=>'0'));
		}
		$this->db->insert('tracking',array(
			'remark'=>$this->params->remark,
			'attachment'=>isset($this->params->filename) ? $this->params->filename : '',
			'creator'=>$this->session->ID,
			'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
			'step'=>'STATUS',
			'created_time'=>date("Y-m-d H:i:s"),
			'id_perijinan'=>$this->params->ID
			));
		$perijinan = $this->db->where('ID',$this->params->ID)->get('perijinan')->row();
		$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
		$data['tracking'] = $this->db->select('tracking.*,user.nama_lengkap')->
							join('user','tracking.creator=user.ID')->
							where('id_perijinan',$this->params->ID)->
							order_by('tracking.tanggal','ASC')->
							order_by('tracking.ID','ASC')->
							get('tracking')->result();
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Penerbitan surat rekomtek berdasarkan no perizinana '".$this->params->ID."'. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Perubahan Status Permohonan Perizinan',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
		return true;
	}
	public function submit_status_survei()
	{
		if(isset($this->params->filename)&&$this->params->filename){
			if(file_exists("tmps/{$this->session->ID}/{$this->params->filename}")) 
				rename("tmps/{$this->session->ID}/{$this->params->filename}","attachments/{$this->params->ID}/{$this->params->filename}");
			else
				unset($this->params->filename);
		}
		if($this->session->level!=='5'){
			$this->db->where('ID',$this->params->ID)->update('perijinan',array('rw_client'=>'0'));
		}
		$update = $this->db->where('ID',$this->params->ID)->update('perijinan',array(
			'status'=>'PROCESSING', 'apv_status'=>'SURVEI'
			));
		if($update){
			$rewrite = $this->db->where('id_perijinan',$this->params->ID)->update('tracking',array(
			'stp'=>'0'
			));
			$data['detail'] = $this->db->select('warna.*')->
							where('warna.tipe','SURVEI')->get('warna')->row();
			if(!$data['detail']) return false;
			
		$this->db->insert('tracking',array(
			'remark'=>$this->params->remark,
			'attachment'=>isset($this->params->filename) ? $this->params->filename : '',
			'creator'=>$this->session->ID,
			'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
			'step'=>'STATUS',
			'created_time'=>date("Y-m-d H:i:s"),
			'wrstatus'=>'SURVEI',
			'id_perijinan'=>$this->params->ID,
			'stp'=>'1',
			'dur'=>$data['detail']->durasi,
			'warna'=> $data['detail']->tipe_warna
			));
		$perijinan = $this->db->where('ID',$this->params->ID)->get('perijinan')->row();
		$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
		$data['tracking'] = $this->db->select('tracking.*,user.nama_lengkap')->
							join('user','tracking.creator=user.ID')->
							where('id_perijinan',$this->params->ID)->
							order_by('tracking.tanggal','ASC')->
							order_by('tracking.ID','ASC')->
							get('tracking')->result();
		$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Perubahan status permohonan dari ekspose menjadi survei berdasarkan nomor perizinan '".$this->params->ID."'. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Perubahan Status Permohonan Perizinan Ke Survei',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
		// $pesan = $this->load->view('email/status', $data, true);
		// $this->aang->emailaang(array(
		// 	'email_natuna'=>$user->email,
		// 	'email_natuna_name'=>$user->nama_lengkap,
		// 	'judul'=>'Update Status Terbaru | '.$perijinan->perihal,
		// 	'pesan'=>$pesan
		// 	));
		return true;
		}
		else return false;
	}
	public function submit_status_rapat()
	{
		if(isset($this->params->filename)&&$this->params->filename){
			if(file_exists("tmps/{$this->session->ID}/{$this->params->filename}")) 
				rename("tmps/{$this->session->ID}/{$this->params->filename}","attachments/{$this->params->ID}/{$this->params->filename}");
			else
				unset($this->params->filename);
		}
		if($this->session->level!=='5'){
			$this->db->where('ID',$this->params->ID)->update('perijinan',array('rw_client'=>'0'));
		}
		$update = $this->db->where('ID',$this->params->ID)->update('perijinan',array(
			'status'=>'PROCESSING', 'apv_status'=>'RAPAT'
			));
		if($update){
			$rewrite = $this->db->where('id_perijinan',$this->params->ID)->update('tracking',array(
			'stp'=>'0'
			));

			$data['detail'] = $this->db->select('warna.*')->
							where('warna.tipe','RAPAT')->get('warna')->row();
			if(!$data['detail']) return false;
			
		$this->db->insert('tracking',array(
			'remark'=>$this->params->remark,
			'attachment'=>isset($this->params->filename) ? $this->params->filename : '',
			'creator'=>$this->session->ID,
			'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
			'step'=>'STATUS',
			'created_time'=>date("Y-m-d H:i:s"),
			'wrstatus'=>'RAPAT',
			'id_perijinan'=>$this->params->ID,
			'stp'=>'1',
			'dur'=>$data['detail']->durasi,
			'warna'=> $data['detail']->tipe_warna
			));
		$perijinan = $this->db->where('ID',$this->params->ID)->get('perijinan')->row();
		$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
		$data['tracking'] = $this->db->select('tracking.*,user.nama_lengkap')->
							join('user','tracking.creator=user.ID')->
							where('id_perijinan',$this->params->ID)->
							order_by('tracking.tanggal','ASC')->
							order_by('tracking.ID','ASC')->
							get('tracking')->result();
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Informasi perubahaan status permohonan perizinan dari survei ke rapat '".$this->params->ID."'. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Perubahan Status Permohonan Perizinan Ke Rapat',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
		// $pesan = $this->load->view('email/status', $data, true);
		// $this->aang->emailaang(array(
		// 	'email_natuna'=>$user->email,
		// 	'email_natuna_name'=>$user->nama_lengkap,
		// 	'judul'=>'Update Status Terbaru | '.$perijinan->perihal,
		// 	'pesan'=>$pesan
		// 	));
		return true;
		}
		else return false;
	}
	public function submit_status_perijinan()
	{
		if(isset($this->params->filename)&&$this->params->filename){
			if(file_exists("tmps/{$this->session->ID}/{$this->params->filename}")) 
				rename("tmps/{$this->session->ID}/{$this->params->filename}","attachments/{$this->params->ID}/{$this->params->filename}");
			else
				unset($this->params->filename);
		}
		if($this->session->level!=='5'){
			$this->db->where('ID',$this->params->ID)->update('perijinan',array('rw_client'=>'0'));
		}
		$update = $this->db->where('ID',$this->params->ID)->update('perijinan',array(
			'status'=>'PROCESSING', 'apv_status'=>'PERSETUJUAN'
			));
		if($update){
			$rewrite = $this->db->where('id_perijinan',$this->params->ID)->update('tracking',array(
			'stp'=>'0'
			));
			$data['detail'] = $this->db->select('warna.*')->
							where('warna.tipe','PERSETUJUAN')->get('warna')->row();
			if(!$data['detail']) return false;
			
		$this->db->insert('tracking',array(
			'remark'=>$this->params->remark,
			'attachment'=>isset($this->params->filename) ? $this->params->filename : '',
			'creator'=>$this->session->ID,
			'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
			'step'=>'STATUS',
			'created_time'=>date("Y-m-d H:i:s"),
			'wrstatus'=>'PERSETUJUAN',
			'id_perijinan'=>$this->params->ID,
			'stp'=>'1',
			'dur'=>$data['detail']->durasi,
			'warna'=> $data['detail']->tipe_warna
			));
		$perijinan = $this->db->where('ID',$this->params->ID)->get('perijinan')->row();
		$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
		$data['tracking'] = $this->db->select('tracking.*,user.nama_lengkap')->
							join('user','tracking.creator=user.ID')->
							where('id_perijinan',$this->params->ID)->
							order_by('tracking.tanggal','ASC')->
							order_by('tracking.ID','ASC')->
							get('tracking')->result();
		$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Informasi perubahaan status permohonan perizinan dari rapat menjadi PERSETUJUAN berdasarkan nomor '".$this->params->ID."'. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Perubahan Status Perizinan',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
		// $pesan = $this->load->view('email/status', $data, true);
		// $this->aang->emailaang(array(
		// 	'email_natuna'=>$user->email,
		// 	'email_natuna_name'=>$user->nama_lengkap,
		// 	'judul'=>'Update Status Terbaru | '.$perijinan->perihal,
		// 	'pesan'=>$pesan
		// 	));
		return true;
		}
		else return false;
	}
	public function terima_permohonan()
	{
		if(isset($this->params->filename)&&$this->params->filename){
			if(file_exists("tmps/{$this->session->ID}/{$this->params->filename}")) 
				rename("tmps/{$this->session->ID}/{$this->params->filename}","attachments/{$this->params->ID}/{$this->params->filename}");
			else
				unset($this->params->filename);
		}

		$update = $this->db->where('ID',$this->params->ID)->update('perijinan',array(
			'status'=>'ACCEPT',
			'accept_by'=>$this->session->ID,
			'accepted_time'=>date("Y-m-d H:i:s"),
			'rekomtek'=>$this->params->rekomtek,
			'apv_status'=>'REKOMTEK'
			));
		if($update){
			$data['detail'] = $this->db->select('warna.*')->
							where('warna.tipe','REKOMTEK')->get('warna')->row();
			if(!$data['detail']) return false;
			$rewrite = $this->db->where('id_perijinan',$this->params->ID)->update('tracking',array(
			'stp'=>'0'
			));
			$this->db->insert('tracking',array(
				'remark'=>$this->params->remark,
				'creator'=>$this->session->ID,
				'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
				'step'=>'ACCEPT',
				'created_time'=>date("Y-m-d H:i:s"),
				'id_perijinan'=>$this->params->ID,
				'wrstatus'=>'REKOMTEK',
				'stp'=>'1',
				'dur'=>$data['detail']->durasi,
				'warna'=> $data['detail']->tipe_warna
				));
			$this->db->insert('tracking',array(
				'remark'=>"<span class='text-green'><i class='fa fa-check'></i> Permohonan diterima dengan nomor rekomtek : {$this->params->rekomtek}</span>",
				'creator'=>$this->session->ID,
				'created_time'=>date("Y-m-d H:i:s"),
				'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
				'id_perijinan'=>$this->params->ID,
				'attachment'=>isset($this->params->filename) ? $this->params->filename : '',
				));
			$perijinan = $this->db->where('ID',$this->params->ID)->get('perijinan')->row();
			$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
			$data['tracking'] = $this->db->select('tracking.*,user.nama_lengkap')->
								join('user','tracking.creator=user.ID')->
								where('id_perijinan',$this->params->ID)->
								order_by('tracking.tanggal','ASC')->
								order_by('tracking.ID','ASC')->
								get('tracking')->result();
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Informasi penerbitaan surat REKOMTEK berdasarkan nomor perizinan '".$this->params->remark."'. Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Penerimaan Dokumen Permohonan Perizinan',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
			return true;
		}
		else return false;
	}

	public function tolak_permohonan()
	{
		if(isset($this->params->filename)&&$this->params->filename){
			if(file_exists("tmps/{$this->session->ID}/{$this->params->filename}")) 
				rename("tmps/{$this->session->ID}/{$this->params->filename}","attachments/{$this->params->ID}/{$this->params->filename}");
			else
				unset($this->params->filename);
		}
		$update = $this->db->where('ID',$this->params->ID)->update('perijinan',array(
			'status'=>'REJECT',
			'reject_by'=>$this->session->ID,
			'rejected_time'=>date("Y-m-d H:i:s")
			));
		if($update){
			$this->db->insert('tracking',array(
				'remark'=>$this->params->remark,
				'creator'=>$this->session->ID,
				'created_time'=>date("Y-m-d H:i:s"),
				'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
				'step'=>'REJECT',
				'id_perijinan'=>$this->params->ID
				));
			$this->db->insert('tracking',array(
				'remark'=>"<span class='text-red'><i class='fa fa-ban'></i> Permohonan ditolak</span>",
				'creator'=>$this->session->ID,
				'created_time'=>date("Y-m-d H:i:s"),
				'tanggal'=>isset($this->params->tanggal) && $this->params->tanggal ? substr($this->params->tanggal, 6, 4).'-'.substr($this->params->tanggal, 3, 2).'-'.substr($this->params->tanggal, 0, 2) : date("Y-m-d"),
				'id_perijinan'=>$this->params->ID,
				'attachment'=>isset($this->params->filename) ? $this->params->filename : '',
				));
			$perijinan = $this->db->where('ID',$this->params->ID)->get('perijinan')->row();
			$user = $this->db->where('ID',$perijinan->creator)->get('user')->row();
			$data['tracking'] = $this->db->select('tracking.*,user.nama_lengkap')->
								join('user','tracking.creator=user.ID')->
								where('id_perijinan',$this->params->ID)->
								order_by('tracking.tanggal','ASC')->
								order_by('tracking.ID','ASC')->
								get('tracking')->result();
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Permohonan anda telah ditolak dengan alasan '".$this->params->remark."'. berdasarkan nomor perizinan '".$this->params->ID."' Untuk informasi lebih lanjut  silahkan hubungi tim perizinan balai besar pelaksaan jalan nasional VI.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/suratmasuk', $datae, true);
			$this->kirimdataemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Informasi Penolakan Permohonan Perizinan',
			'pesan'=>$pesan,
			'file'=>"C:/xampp/htdocs/rumija/apps/attachments/".$this->params->ID."/".$this->params->filename
			));
			// $data["nama"] = $user->nama_lengkap;
			// $data["pembuka"] = "Setelah melakukan prosedur yang ada, kami menyatakan bahwa pengajuan permohonan Anda perihal <strong>{$perijinan->perihal}</strong> dengan nomor pengajuan <strong>{$perijinan->ID}</strong> <strong style='color:red;'>DITOLAK</strong>.";
			// $data["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			// $pesan = $this->load->view('email/status', $data, true);
			// $this->aang->emailaang(array(
			// 	'email_natuna'=>$user->email,
			// 	'email_natuna_name'=>$user->nama_lengkap,
			// 	'judul'=>'Permohonan Ditolak | '.$perijinan->perihal,
			// 	'pesan'=>$pesan
			// 	));
			return true;
		}
		else return false;
	}

	public function hapus_ijin()
	{
		$hapus = $this->db->where('ID',$this->params->ID)->delete('perijinan');
		if($hapus){
			$this->deleteDir("attachments/{$this->params->ID}");
			return $this->db->where('id_perijinan',$this->params->ID)->delete('tracking');
		} else {
			return false;
		}
	}

	public static function deleteDir($dirPath) {
	    if (!is_dir($dirPath)) {
	        return false;
	    }
	    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
	        $dirPath .= '/';
	    }
	    $files = glob($dirPath . '*', GLOB_MARK);
	    foreach ($files as $file) {
	        if (is_dir($file)) {
	            self::deleteDir($file);
	        } else {
	            unlink($file);
	        }
	    }
	    rmdir($dirPath);
	}

}
