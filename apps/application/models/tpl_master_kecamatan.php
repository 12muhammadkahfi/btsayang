<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tpl_master_kecamatan extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}
	public function kecamatan()
	{
		$this->db->select('kecamatan.*');
		$this->db->order_by('Nama','asc');
		return $this->db->get('kecamatan')->result();
	}
	public function cari_kecamatan()
	{
		$this->db->select('kecamatan.*');
		$this->db->where('kec', $this->params->ID);
		$this->db->where('prov', $this->params->PRID);
		$this->db->where('kab', $this->params->KABID);
		$this->db->order_by('Nama','asc');
		return $this->db->get('kecamatan')->row();
	}
	public function sub_kecamatan()
	{
		$this->db->select('kecamatan.*');
		$this->db->where('prov', $this->params->PRID);
		$this->db->where('kab', $this->params->KABID);
		$this->db->order_by('Nama','asc');
		return $this->db->get('kecamatan')->row();
	}
}
