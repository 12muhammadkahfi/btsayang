<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tpl_provinsi extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}
	public function provinsi()
	{
		$this->db->select('provinsi.*');
		$this->db->order_by('Nama','asc');
		return $this->db->get('provinsi')->result();
	}
	public function cari_provinsi()
	{
		$this->db->select('provinsi.*');
		$this->db->where('prov', $this->params->ID);
		$this->db->order_by('Nama','asc');
		return $this->db->get('provinsi')->row();
	}
}
