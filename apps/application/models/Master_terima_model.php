<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_terima_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}

	public function tipe_search()
	{
		$this->db->select('tipe.*, user.nama_lengkap as username');
		$this->db->join('user', 'tipe.creator = user.ID');
		$this->db->where('tipe.status', 1);
		$this->db->order_by('nama_tipe', 'asc');
		return $this->db->get('tipe')->result();
	}

	public function terima()
	{
		$this->db->select('perijinan.*');
	//	$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('perijinan.status', 'PENDING');
		$this->db->where('perijinan.apv_status', 'TERIMA');
		$this->db->where('DATEDIFF(tanggal_data,CURDATE()) <=',2);
		$this->db->order_by('ID', 'asc');
		return $this->db->get('perijinan')->result();
	}

	public function terimaClient()
	{
		$this->db->select('perijinan.*');
	//	$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('perijinan.rw_client', '0');
		$this->db->where('perijinan.apv_status <>', 'TERIMA');
		$this->db->where('perijinan.creator',$this->session->ID);
		$this->db->order_by('ID', 'asc');
		return $this->db->get('perijinan')->result();
	}

	public function terimaUser()
	{
		$this->db->select('perijinan.*');
	//	$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('perijinan.rw_user', '0');
		$this->db->where('perijinan.apv_status', 'TERIMA');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('perijinan')->result();
	}

	public function survei()
	{
		$this->db->select('perijinan.*, warna.tipe, warna.tipe_warna, warna.durasi');
		$this->db->join('warna', 'perijinan.apv_status = warna.tipe');
		$this->db->where('perijinan.status','PROCESSING');
		$this->db->where('perijinan.apv_status','SURVEI');
		$this->db->where('DATEDIFF(tanggal_data,CURDATE()) <=','warna.durasi');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('perijinan')->result();
	}
	public function ekspose()
	{
		$this->db->select('perijinan.*, warna.tipe, warna.tipe_warna, warna.durasi');
		$this->db->join('warna', 'perijinan.apv_status = warna.tipe');
		$this->db->where('perijinan.status','PROCESSING');
		$this->db->where('perijinan.apv_status','EKSPOSE');
		$this->db->where('DATEDIFF(tanggal_data,CURDATE()) <=','warna.durasi');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('perijinan')->result();
	}

	public function hasil_rapat()
	{
		$this->db->select('perijinan.*, warna.tipe, warna.tipe_warna, warna.durasi');
		$this->db->join('warna', 'perijinan.apv_status = warna.tipe');
		$this->db->where('perijinan.status','PROCESSING');
		$this->db->where('perijinan.apv_status','HASIL RAPAT');
		$this->db->where('DATEDIFF(tanggal_data,CURDATE()) <=','warna.durasi');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('perijinan')->result();
	}
	public function persetujuan()
	{
		$this->db->select('perijinan.*, warna.tipe, warna.tipe_warna, warna.durasi');
		$this->db->join('warna', 'perijinan.apv_status = warna.tipe');
		$this->db->where('perijinan.status','PROCESSING');
		$this->db->where('perijinan.apv_status','PERSETUJUAN');
		$this->db->where('DATEDIFF(tanggal_data,CURDATE()) <=','warna.durasi');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('perijinan')->result();
	}
	public function rekomtek()
	{
		$this->db->select('perijinan.*');
	//	$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('perijinan.status', 'ACCEPT');
		$this->db->where('perijinan.apv_status','REKOMTEK');
//		$this->db->where('DATEDIFF(tanggal_data,CURDATE()) <=',2);
		$this->db->order_by('ID', 'asc');
		return $this->db->get('perijinan')->result();
	}
	public function submit_tipe()
	{
		if(!isset($this->params->id_tipe)){
			$this->params->id_tipe = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('tipe', $this->params);
		} else {
			return $this->db->where('id_tipe', $this->params->id_tipe)->update('tipe',$this->params);
		}
	}
	public function submit_tipe_pembangunan()
	{
		if(!isset($this->params->ID)){
			$this->params->id_tipe = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('tipe', $this->params);
		} else {
			return $this->db->where('ID', $this->params->ID)->update('tipe',$this->params);
		}
	}
	public function provinsi()
	{
		$this->db->select('provinsi.*, user.nama_lengkap as username');
		$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('provinsi.status', 1);
		$this->db->order_by('nama_provinsi', 'asc');
		return $this->db->get('provinsi')->result();
	}

	public function submit_provinsi()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('provinsi', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('provinsi',$this->params);
		}
	}

	public function kota()
	{
		$this->db->select('kota.*, user.nama_lengkap as username, user.email, provinsi.nama_provinsi');
		$this->db->join('user', 'kota.creator = user.ID');
		$this->db->join('provinsi', 'kota.id_provinsi = provinsi.ID');
		if(isset($this->params->id_provinsi)&&$this->params->id_provinsi) $this->db->where('kota.id_provinsi', $this->params->id_provinsi);
		$this->db->where('kota.status', 1);
		$this->db->order_by('nama_kota', 'asc');
		return $this->db->get('kota')->result();
	}

	

	// public function ruas_jalan()
	// {
	// 	$this->db->select('ruas_jalan.*, user.nama_lengkap, provinsi.nama_provinsi, kota.nama_kota');
	// 	$this->db->join('user', 'ruas_jalan.creator = user.ID');
	// 	$this->db->join('provinsi', 'ruas_jalan.id_provinsi = provinsi.ID','left');
	// 	$this->db->join('kota', 'ruas_jalan.id_kota = kota.ID','left');
	// 	$this->db->where('ruas_jalan.status', 1);
	// 	$this->db->order_by('nama_ruas_jalan', 'asc');
	// 	return $this->db->get('ruas_jalan')->result();
	// }



	public function submit_ruas_jalan()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('ruas_jalan', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('ruas_jalan',$this->params);
		}
	}

}
