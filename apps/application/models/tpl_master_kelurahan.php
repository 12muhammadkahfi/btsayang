<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tpl_master_kelurahan extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}
	public function kelurahan()
	{
		$this->db->select('kelurahan.*');
		$this->db->order_by('Nama','asc');
		return $this->db->get('kelurahan')->result();
	}
	public function cari_kelurahan()
	{
		$this->db->select('kelurahan.*');
		$this->db->where('kel', $this->params->ID);
		$this->db->where('kec', $this->params->KECID);
		$this->db->where('prov', $this->params->PRID);
		$this->db->where('kab', $this->params->KABID);
		$this->db->order_by('Nama','asc');
		return $this->db->get('kelurahan')->row();
	}
	public function sub_kelurahan()
	{
		$this->db->select('kelurahan.*');
		$this->db->where('prov', $this->params->PRID);
		$this->db->where('kab', $this->params->KABID);
		$this->db->where('kec', $this->params->KECID);
		$this->db->order_by('Nama','asc');
		return $this->db->get('kelurahan')->row();
	}
}
