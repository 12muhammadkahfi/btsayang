<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class report_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}
	public function entities_report_provinsi()
	{
		$this->db->select('planningbts.kode,planningbts.lprov, planningbts.stat_prov');
		$this->db->group_by('planningbts.lprov');
		$this->db->order_by('planningbts.stat_prov', 'asc');
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_total_kabaputen($lanoy)
	{
		$this->db->select('datakab.*, SUM(dt) as dts');
		$this->db->where('datakab.lprov', $lanoy);
		return $this->db->get('datakab')->result();
	}
	public function entities_report_kabupaten($tengku)
	{
		$this->db->select('stat_kab,lprov,lkab,COUNT(KODE) AS itn');
		$this->db->where('lprov', $tengku);
		$this->db->group_by('lkab');
		$this->db->order_by('stat_kab', 'asc');
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_kecamatan($tengku, $safrina)
	{
		$this->db->select('lkec,stat_kec,lprov,lkab');
		$this->db->where('lprov', $tengku);
		$this->db->where('lkab', $safrina);
		$this->db->group_by('lkec');
		$this->db->order_by('stat_kec', 'asc');
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_kelurahan($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('lprov', $tengku);
		$this->db->where('lkab', $lani);
		$this->db->where('lkec', $safrina);
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_realisasi($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('kprov', $tengku);
		$this->db->where('kkab', $lani);
		$this->db->where('kkec', $safrina);
		return $this->db->get('realisasibts')->result();
	}

	public function entities_report_provinsi_realisasi()
	{
		$this->db->select('realisasibts.kode,realisasibts.kprov, realisasibts.stat_prov');
		$this->db->group_by('realisasibts.kprov');
		$this->db->order_by('realisasibts.stat_prov', 'asc');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_kabupaten_realisasi($tengku)
	{
		$this->db->select('stat_kab,kprov,kkab');
		$this->db->where('kprov', $tengku);
		$this->db->group_by('kkab');
		$this->db->order_by('stat_kab', 'asc');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_kecamatan_realisasi($tengku, $safrina)
	{
		$this->db->select('kkec,stat_kec,kprov,kkab');
		$this->db->where('kprov', $tengku);
		$this->db->where('kkab', $safrina);
		$this->db->group_by('kkec');
		$this->db->order_by('stat_kec', 'asc');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_kelurahan_realisasi($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('kprov', $tengku);
		$this->db->where('kkab', $lani);
		$this->db->where('kkec', $safrina);
		return $this->db->get('realisasibts')->result();
	}

	public function entities_report_provinsi_eng()
	{
		$this->db->select('provinsi');
		$this->db->group_by('provinsi');
		$this->db->order_by('provinsi', 'asc');
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_kabupaten_eng($tengku)
	{
		$this->db->select('provinsi, kabupaten, kecamatan');
		$this->db->where('provinsi', $tengku);
		$this->db->group_by('kabupaten');
		$this->db->order_by('kabupaten', 'asc');
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_kecamatan_eng($tengku, $safrina)
	{
		$this->db->select('kecamatan, provinsi, kabupaten');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $safrina);
		$this->db->group_by('kecamatan');
		$this->db->order_by('kecamatan', 'asc');
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_kelurahan_eng($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_provinsi_idn()
	{
		$this->db->select('provinsi');
		$this->db->group_by('provinsi');
		$this->db->order_by('provinsi', 'asc');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_kabupaten_idn($tengku)
	{
		$this->db->select('provinsi, kabupaten, kecamatan');
		$this->db->where('provinsi', $tengku);
		$this->db->group_by('kabupaten');
		$this->db->order_by('kabupaten', 'asc');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_kecamatan_idn($tengku, $safrina)
	{
		$this->db->select('kecamatan, provinsi, kabupaten');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $safrina);
		$this->db->group_by('kecamatan');
		$this->db->order_by('kecamatan', 'asc');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_kelurahan_idn($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_provinsi_my()
	{
		$this->db->select('provinsi');
		$this->db->group_by('provinsi');
		$this->db->order_by('provinsi', 'asc');
		return $this->db->get('test')->result();
	}
	public function entities_report_provinsi_my_wrong()
	{
		$this->db->select('test.provinsi,test.kabupaten,test.kecamatan,test.kelurahan,test.nd,test.nat,planningbts.provinsi as prv,planningbts.kabupaten as kab,planningbts.kecamatan as kec,planningbts.desa as kel');
		$this->db->join('planningbts','planningbts.no = test.nat');
		$this->db->where('test.nd','P');
		$this->db->where('test.provinsi','');
		$this->db->or_where('test.kabupaten','');
		$this->db->or_where('test.kecamatan','');
		$this->db->order_by('test.nat', 'asc');
		return $this->db->get('test')->result();
	}
	public function entities_report_number_my_wrong()
	{
		$this->db->select('planningbts.no,planningbts.stat_prov as prov,planningbts.stat_kab as kab');
		$this->db->join('kabupaten','kabupaten.prov = planningbts.lprov');
		// $this->db->where('planningbts.lprov','kabupaten.prov');
		// $this->db->where_not_in('planningbts.lkab','kabupaten.kab');
		// $this->db->group_by('planningbts.lprov');
		// $this->db->group_by('planningbts.lkab');
		$this->db->order_by('planningbts.no', 'asc');
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_kabupaten_my_wrong()
	{
		$this->db->select('test.provinsi,test.kabupaten,test.kecamatan,test.kelurahan,test.nd,test.nat,planningbts.provinsi as prv,planningbts.kabupaten as kab,planningbts.kecamatan as kec ');
		$this->db->join('planningbts','planningbts.no = test.nat');
		$this->db->where('test.nd','P');
		$this->db->where('test.kabupaten','');
		$this->db->order_by('test.nat', 'asc');
		return $this->db->get('test')->result();
	}
	public function entities_report_kecamatan_my_wrong()
	{
		$this->db->select('test.provinsi,test.kabupaten,test.kecamatan,test.kelurahan,test.nd,test.nat,planningbts.provinsi as prv,planningbts.kabupaten as kab,planningbts.kecamatan as kec ');
		$this->db->join('planningbts','planningbts.no = test.nat');
		$this->db->where('test.nd','P');
		$this->db->where('test.kecamatan','');
		$this->db->order_by('test.nat', 'asc');
		return $this->db->get('test')->result();
	}
	public function entities_report_kabupaten_my($tengku)
	{
		$this->db->select('provinsi, count(kelurahan) as itung');
		$this->db->where('provinsi', $tengku);
		//$this->db->group_by('kabupaten');
		return $this->db->get('test')->result();
	}
	public function entities_report_kptn_my($tengku)
	{
		$this->db->select('provinsi, kabupaten');
		$this->db->where('provinsi', $tengku);
		$this->db->group_by('kabupaten');
		return $this->db->get('test')->result();
	}
	public function entities_report_kabupaten_my_total()
	{
		$this->db->select('provinsi,count(kabupaten) as itung');
		$this->db->group_by('provinsi');
		return $this->db->get('test2')->result();
//		return $this->db->num_rows();
	}
	public function entities_report_kabupaten_my_total_salah()
	{
		$this->db->select('provinsi,count(kabupaten) as itung');
		$this->db->group_by('provinsi');
		return $this->db->get('test2')->result();
//		return $this->db->num_rows();
	}
	public function entities_report_kecamatan_my_total($tengku)
	{
		$this->db->select('count(kecamatan) as iteung');
		$this->db->where('provinsi', $tengku);
		return $this->db->get('test3')->result();
//		return $this->db->num_rows();
	}
	public function entities_report_kabupaten_my_real($tengku, $lani)
	{
		$this->db->select('stat_prov, stat_kab');
		$this->db->where('stat_prov', $tengku);
		$this->db->where('stat_kab <>', $lani);
		$this->db->order_by('stat_kab', 'asc');
		
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_kabupaten_my_real_backup($tengku, $lani)
	{
		$this->db->select('provinsi, kabupaten');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten <>', $lani);
		$this->db->order_by('kabupaten', 'asc');
		
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_kecamatan_my($tengku, $safrina)
	{
		$this->db->select('kecamatan, provinsi, kabupaten');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $safrina);
		$this->db->group_by('kecamatan');
		$this->db->order_by('kecamatan', 'asc');
		return $this->db->get('test')->result();
	}
	public function entities_report_kelurahan_my($tengku, $lani, $safrina)
	{   
		$this->db->select('count(nat) as itung');
		$this->db->where('nd','P');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);		
		return $this->db->get('test')->result();
	}
	public function entities_report_kelurahan_my_backup($tengku, $lani, $safrina)
	{   
		$this->db->select('count(kode) as itung');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);
		return $this->db->get('planningbts')->result();
	}
	public function entities_report_desa_my($tengku, $lani, $safrina)
	{
		$this->db->select('count(nat) as itung');
		$this->db->where('nd','R');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);		
		return $this->db->get('test')->result();
	}
	public function entities_report_desa_my_five($tengku, $lani, $safrina)
	{
		$this->db->select('count(nat) as itung');
		$this->db->where('nd','R');
		$this->db->where('reff', '2015');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);		
		return $this->db->get('test')->result();
	}
	public function entities_report_desa_my_seven($tengku, $lani, $safrina)
	{
		$this->db->select('count(nat) as itung');
		$this->db->where('nd','R');
		$this->db->where('reff', '2016');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);		
		return $this->db->get('test')->result();
	}
	public function entities_report_desa_my_eight($tengku, $lani, $safrina)
	{
		$this->db->select('count(nat) as itung');
		$this->db->where('nd','R');
		$this->db->where('reff', '2017');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);		
		return $this->db->get('test')->result();
	}
	public function entities_report_desa_my_nine($tengku, $lani, $safrina)
	{
		$this->db->select('count(nat) as itung');
		$this->db->where('nd','R');
		$this->db->where('reff', '2018');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);		
		return $this->db->get('test')->result();
	}
	public function entities_report_desa_my_backup($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_desa_my_five_backup($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);
		$this->db->where('periode', '2015');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_desa_my_seven_backup($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);
		$this->db->where('periode', '2016');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_desa_my_eight_backup($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);
		$this->db->where('periode', '2017');
		return $this->db->get('realisasibts')->result();
	}
	public function entities_report_desa_my_nine_backup($tengku, $lani, $safrina)
	{
		$this->db->select('count(kode) as itung');
		$this->db->where('provinsi', $tengku);
		$this->db->where('kabupaten', $lani);
		$this->db->where('kecamatan', $safrina);
		$this->db->where('periode', '2018');
		return $this->db->get('realisasibts')->result();
	}
}
