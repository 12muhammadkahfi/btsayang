<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}

	public function dologin()
	{		
		$user = $this->db->where(array(
			'email'=>strtolower($this->input->post('email',true)),
			'password'=>$this->aang->superhash(strtoupper($this->input->post('password'))))
		)->get('user')->row();
		return $user;
	}

	public function pulihkan_akun($enc)
	{
		$user = $this->db->where('forgot_password',$enc)->get('user')->row();
		if(!$user) return false;
		else {
			$password_baru = rand(11111111,99999999);
			$this->db->where('forgot_password',$enc)->update('user',array('forgot_password'=>'','password'=>$this->aang->superhash($password_baru)));
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Password baru Anda saat ini adalah <strong>{$password_baru}</strong>. Kami menyarankan agar Anda segera mengubah password Anda setelah Anda login.";
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
			$pesan = $this->load->view('email/status', $datae, true);
			$this->kirimemail(array(
				'email_to'=>$user->email,
				'email_to_name'=>$user->nama_lengkap,
				'judul'=>'Password Baru Akun Anda',
				'pesan'=>$pesan
				));
			return true;
		}
	}

	public function forgot_password()
	{
		$user = $this->db->where('email',$this->input->post('email',true))->get('user')->row();
		if(!$user){
			return array('status'=>'error','message'=>'Tidak ditemukan pengguna dengan alamat email yang anda masukan : '.$_POST['email']);
		} else {
			$enc = md5(uniqid());
			$this->db->where('email',$this->input->post('email',true))->update('user',array('forgot_password'=>$enc));
			$datae["nama"] = $user->nama_lengkap;
			$datae["pembuka"] = "Seseorang telah meminta email pemulihan akun Anda karena lupa password. Jika anda yang melakukannya klik/kunjungi link di bawah ini, namun jika Anda tidak merasa melakukannya harap abaikan email ini.<br/><br/>";
			$datae["pembuka"] .= "Link pemulihan akun : ".site_url('pulihkan_akun/'.$enc);
			$datae["penutup"] = "Terima kasih telah menggunakan layanan Application MKTREE.<br/><br/>Regard's,<br/>MK TREE.";
			$pesan = $this->load->view('email/status', $datae, true);
			$this->kirimemail(array(
				'email_to'=>$user->email,
				'email_to_name'=>$user->nama_lengkap,
				'judul'=>'Email Pemulihan Akun',
				'pesan'=>$pesan
				));
			return array('status'=>'success','message'=>'Kami telah mengirimkan email pemulihan akun ke alamat email yang anda masukan : '.$_POST['email']);
		}
	}

	public function doregister()
	{
		$this->session->set_userdata('postdata',serialize($this->input->post(null,true)));

		if(strlen(trim($_POST['password']))<6){
			$this->session->fail_registration = "Panjang password minimal 6 karakter!";
			return false;
		}

		if(strtoupper(trim($_POST['password']))===strtoupper("j8kl9m")){
			$this->session->fail_registration = "Anda menggunakan password sample, password sample (j8kl9m) tidak boleh digunakan.";
			return false;
		}

		if(strtoupper(trim($_POST['password']))!==strtoupper(trim($_POST['repassword']))){
			$this->session->fail_registration = "Password yang anda masukan tidak sesuai dengan konfirmasi password yang anda masukan.";
			return false;
		}

		$check = $this->db->where('LOWER(`email`)',"'".strtolower(trim($this->input->post('email',true)))."'",false)->get('user')->num_rows();
		if($check){
			$this->session->fail_registration = "Alamat email {$_POST['email']} sudah terdaftar dalam database kami sebelumnya.";
			return false;
		}

//		$data = (object) $this->input->post(null,true);
		$data->password = $this->aang->superhash(strtoupper($_POST['password']));
		$data->email = strtolower(trim($_POST['email']));
//		$data->email = strtolower(trim($data->email));
		$data->created_time = date("Y-m-d H:i:s");
		$data->nama_lengkap = $_POST['nama_lengkap'];
		$data->pin = rand(10000000,99999999);
		$data->level = 1;
		unset($data->repassword);
		$user = $this->db->insert('user',$data);
		$this->session->set_userdata('success_registration',$data->pin);
		$datae["nama"] = $data->nama_lengkap;
		$datae["pembuka"] = "Anda telah melakukan pendaftaran sebagai user aplikasi MKTREE. Untuk melakukan aktivasi akun anda silahkan kunjungi link berikut ini : <br/><br/><a href='".site_url('aktivasi/'.md5($data->pin))."'>".site_url('aktivasi/'.md5($data->pin))."</a>";
		$datae["penutup"] = "Terima kasih telah menggunakan layanan MKTREE.<br/><br/>Salam,<br/>Regard's<br/><b>MK TREE.</b>";
		$pesan = $this->load->view('email/status', $datae, true);
		$this->kirimemail(array(
			'email_to'=>$data->email,
			'email_to_name'=>$data->nama_lengkap,
			'judul'=>'Konfirmasi Pendaftaran',
			'pesan'=>$pesan
			));
		return $user;
	}
	
	public function kirimemail($data)
	{
		$data["email_sender"] = "mail.mksgroup@gmail.com";
        $data["email_sender_name"] = "ADMINISTRATOR MKTREE - (NO REPLY)";
		$this->load->library('email');
		$this->email->from($data["email_sender"], $data["email_sender_name"]);
		$this->email->to($data['email_to'],$data['email_to_name']);
		$this->email->subject($data['judul']);
		$this->email->message($data['pesan']);
		$this->email->send();
		// if ($this->email->send())
		// 	echo "Mail Sent!";
		// else
		// 	echo "There is error in sending mail!";
	}

	public function aktivasi($hash)
	{
		$this->db->where('MD5(`pin`)', "'{$hash}'", false);
		$check = $this->db->get('user')->num_rows();
		if(!$check) return false;
		return $this->db->where('MD5(`pin`)', "'{$hash}'", false)->update('user',array(
			'pin'=>0
			));
	}

	public function do_resend_mail()
	{
		$this->db->where('email',$this->input->post('email',true));
		$this->db->where('pin >', 0);
		$user = $this->db->get('user')->row();
		if(!$user) return false;
		$datae["nama"] = $user->nama_lengkap;
		$datae["pembuka"] = "Anda telah meminta pengiriman ulang email aktivasi akun Anda. Untuk melakukan aktivasi akun anda silahkan kunjungi link berikut ini : <br/><br/><a href='".site_url('aktivasi/'.md5($user->pin))."'>".site_url('aktivasi/'.md5($user->pin))."</a>";
		$datae["penutup"] = "Terima kasih telah menggunakan layanan Sistem Informasi Ruang Milik Jalan oleh BBPJN6.<br/><br/>Salam,<br/>IT Department BBPJN6.";
		$pesan = $this->load->view('email/status', $datae, true);
		$this->kirimemail(array(
			'email_to'=>$user->email,
			'email_to_name'=>$user->nama_lengkap,
			'judul'=>'Konfirmasi Pendaftaran',
			'pesan'=>$pesan
			));
		return true;
	}

	public function data_pengguna()
	{
		$this->db->where('level', 5);
		$this->db->where('blacklist', 0);
		return $this->db->get('user')->result();
	}

	public function data_administrator()
	{
		$this->db->where('level <', 5);
		$this->db->where('blacklist', 0);
		return $this->db->get('user')->result();
	}

	public function detail()
	{
		$this->db->where('level', 5);
		$this->db->where('blacklist', 0);
		$this->db->where('ID', $this->params->ID);
		$data['detail'] = $this->db->get('user')->row();
		$this->db->select('user.*, ruas_jalan.nama_ruas_jalan, provinsi.nama_provinsi, perijinan.*');
		$this->db->join('ruas_jalan','perijinan.ruas_jalan=ruas_jalan.ID');
		$this->db->join('provinsi','ruas_jalan.id_provinsi=provinsi.ID');
		$this->db->join('user','perijinan.creator=user.ID');
		$this->db->order_by('perijinan.created_time', 'DESC');
		$this->db->where('perijinan.creator', $this->params->ID);
		$data['perijinan'] = $this->db->get('perijinan')->result();
		return $data;
	}

	public function buat_admin()
	{
		$check = $this->db->where('LOWER(`email`)',"'".strtolower(trim($this->params->email))."'",false)->get('user')->num_rows();
		if(intval($check)==0){
			$this->params->email = strtolower($this->params->email);
			$password = rand(11111111,99999999);
			$this->params->password = $this->aang->superhash($password);
			$this->params->level = 3;
			$this->params->created_time = date("Y-m-d H:i:s");
			$this->params->pin = 0;
			$insert = $this->db->insert('user',$this->params);
			if($insert){
				$datae["nama"] = $this->params->nama_lengkap;
				$datae["pembuka"] = "Anda telah didaftarkan sebagai administrator Sistem Informasi RUMIJA BBPJN6 oleh {$this->session->nama_lengkap}. Password akun anda saat ini adalah <strong>{$password}</strong>.<br/><br/>Anda disarankan untuk mengubah password yang dibuat otomatis oleh sistem pada saat Anda login ke akun anda.<br/><br/><a href='".base_url()."'>Klik disini untuk login</a>";
				$datae["penutup"] = "<br/><br/>Salam,<br/>IT Department BBPJN6.";
				$pesan = $this->load->view('email/status', $datae, true);
				$this->kirimemail(array(
					'email_to'=>$this->params->email,
					'email_to_name'=>$this->params->nama_lengkap,
					'judul'=>'Pendaftaran Administrator',
					'pesan'=>$pesan
					));
				return array(
					'status'=>true,
					'message'=>'Berhasil menambahkan '.$this->params->email.' sebagai administrator'
					);
			} else {
				return array(
					'status'=>false,
					'message'=>'Gagal menambahkan '.$this->params->email.' sebagai administrator'
					);
			}
		} else {
			return array(
				'status'=>false,
				'message'=>'Alamat email '.$this->params->email.' sudah didaftarkan sebelumnya'
				);
		}
	}

	public function ganti_password()
	{
		//'password'=>$this->aang->superhash(strtoupper($this->input->post('password'))))
		 $check = $this->db->where('ID',$this->session->ID)->
		 where('password',$this->aang->superhash(strtoupper($this->params->old_password)))->
		 get('user')->num_rows();
		 if(!$check){
		 	return array(
		 		'status'=>false,
		 		'message'=>'Password lama yang anda masukan salah'.$this->session->ID
		 		);
		 } else {
			$cto = $this->db->where('ID',$this->session->ID)->update('user',array(
				'password'=>$this->aang->superhash(strtoupper($this->params->new_password))
				));
			if($cto){
				return array(
					'status'=>true,
					'message'=>'Berhasil mengubah password'
				);
			}else{
				return array(
					'status'=>false,
					'message'=>'Gagal mengubah password'
				);
			}
		 }
	}
	public function dashboard()
	{
		$this->db->where('mid(kdDep,4,2)','00');
		$data['summary']['provinsi'] = $this->db->get('aceh')->num_rows();
		$this->db->where('mid(kdDep,4,2)<>','00');
		$this->db->where('mid(kdDep,7,2)','00');
		$data['summary']['kabupaten'] = $this->db->get('aceh')->num_rows();
		$this->db->where('mid(kdDep,4,2)<>','00');
		$this->db->where('mid(kdDep,7,2)<>','00');
		$data['summary']['kecamatan'] = $this->db->get('aceh')->num_rows();
		$this->db->where('right(kdDep,4)<>','0000');
		$data['summary']['desa'] = $this->db->get('aceh')->num_rows();
		// $this->db->where('status','PENDING');
		// if(intval($this->session->level)==5) $this->db->where('creator',$this->session->ID);
		// $data['summary']['PENDING'] = $this->db->get('perijinan')->num_rows();
		// $this->db->where('status','PROCESSING');
		// if(intval($this->session->level)==5) $this->db->where('creator',$this->session->ID);
		// $data['summary']['PROCESSING'] = $this->db->get('perijinan')->num_rows();
		// $this->db->where('status','ACCEPT');
		// if(intval($this->session->level)==5) $this->db->where('creator',$this->session->ID);
		// $data['summary']['ACCEPT'] = $this->db->get('perijinan')->num_rows();
		// $this->db->where('status','REJECT');
		// if(intval($this->session->level)==5) $this->db->where('creator',$this->session->ID);
		// $data['summary']['REJECT'] = $this->db->get('perijinan')->num_rows();
		// $data['chart']['data'] = array(array(),array());
		// for ($i = 11; $i >= 0; $i--) {
		// 	$month = date("Y-m", strtotime( date( 'Y-m' )." -$i months") );
		//     $months[] = date("M Y", strtotime( date( 'Y-m' )." -$i months") );
		//     $pengajuan = $this->db->where('SUBSTRING(tanggal_data,1,7)',"'{$month}'",false)->get('perijinan')->num_rows();
		//     $data['chart']['data'][0][] = intval($pengajuan);
		//     $rekomtek = $this->db->where('SUBSTRING(tanggal,1,7)',"'{$month}'",false)->where('step','ACCEPT')->group_by('id_perijinan')->get('tracking')->num_rows();
		//     $data['chart']['data'][1][] = intval($rekomtek);
		// }
		// $provinsi = $this->db->select('COUNT(*) AS total, provinsi.nama_provinsi',false)->
		// 			join('ruas_jalan','perijinan.ruas_jalan=ruas_jalan.ID')->
		// 			join('provinsi','ruas_jalan.id_provinsi=provinsi.ID')->
		// 			group_by('id_provinsi')->
		// 			order_by('nama_provinsi','asc')->
		// 			get('perijinan')->result();
		// $provinsi2 = $this->db->select('COUNT(*) AS total, provinsi.nama_provinsi',false)->
		// 			join('ruas_jalan','perijinan.ruas_jalan=ruas_jalan.ID')->
		// 			join('provinsi','ruas_jalan.id_provinsi=provinsi.ID')->
		// 			where('perijinan.status','ACCEPT')->
		// 			group_by('id_provinsi')->
		// 			order_by('nama_provinsi','asc')->
		// 			get('perijinan')->result();
		// // print_r($provinsi);
		// $data['provinsi'] = array();
		// $data['provinsi']['series'] = array('Pengajuan Diterima','Rekomtek Diterbitkan');
		// foreach ($provinsi as $key => $value) {
		// 	$data['provinsi']['labels'][] = $value->nama_provinsi;
		// 	$data['provinsi']['data'][0][] = intval($value->total);
		// 	$data['provinsi']['data'][1][] = intval($provinsi2[$key]->total);
		// }
		// $data['chart']['months'] = $months;
		//$data[]='';
		return $data;
	}

	public function hapus()
	{
		if(intval($this->session->level)!=1) exit();
		$hapus = $this->db->where('ID',$this->params->ID)->update('user',array(
			'blacklist'=>1
			));
		if($hapus) return array(
			'status'=>true,
			'message'=>'Pengguna berhasil dihapus'
			);
		else return array(
			'status'=>false,
			'message'=>'Pengguna gagal dihapus'
			);
	}

}
