<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tpl_master_kabupaten extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}
	public function kabupaten()
	{
		$this->db->select('kabupaten.*');
		$this->db->order_by('Nama','asc');
		return $this->db->get('kabupaten')->result();
	}
	public function cari_kabupaten()
	{
		$this->db->select('kabupaten.*');
		$this->db->where('kab', $this->params->ID);
		$this->db->where('prov', $this->params->PRID);
		$this->db->order_by('Nama','asc');
		return $this->db->get('kabupaten')->row();
	}
	public function sub_kabupaten()
	{
		$this->db->select('kabupaten.*');
		$this->db->where('prov', $this->params->ID);
		$this->db->order_by('Nama','asc');
		return $this->db->get('kabupaten')->row();	
	}
}
