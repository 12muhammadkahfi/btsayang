<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tpl_master_planning extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}
	public function planning_bts()
	{
		$this->db->select('planningbts.*');
		$this->db->order_by('Provinsi','asc');
		return $this->db->get('planningbts')->result();
	}
	public function planning_bts_kemendagri()
	{
		$this->db->select('planningbts.No ,planningbts.Provinsi, planningbts.Kabupaten, planningbts.Kecamatan, planningbts.Desa, planningbts.Sumber_data, kelurahan.Nama, Kelurahan.kdDep, kabupaten.Nama as kab_dagri, provinsi.Nama as prov_dagri, kecamatan.Nama as kec_dagri');
		$this->db->join('kelurahan', 'kelurahan.Nama = planningbts.Desa','left');
		$this->db->join('kecamatan', 'kecamatan.Nama = planningbts.Kecamatan','left');
		$this->db->join('kabupaten', 'kabupaten.Nama = planningbts.Kabupaten','left');
		$this->db->join('provinsi', 'provinsi.Nama = planningbts.Provinsi','left');
		// $this->db->join('Kecamatan', 'kecamatan.Kec = kelurahan.Kec and kecamatan.kab = kelurahan.kab and kecamatan.prov = kelurahan.prov','left');
		// $this->db->join('Kabupaten', 'kabupaten.kab = kecamatan.kab and kabupaten.prov = kecamatan.prov','left');
		// $this->db->join('provinsi', 'provinsi.prov = kabupaten.prov','left');
		$this->db->group_by('planningbts.No');
		$this->db->order_by('kelurahan.Nama','asc');
		$this->db->limit(5000);
		return $this->db->get('planningbts')->result();
	}

}
