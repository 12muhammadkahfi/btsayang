<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->params = (object) json_decode(file_get_contents("php://input"), true);
	}
	public function autokabupaten()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Generate Autonumber";
		//$this->params->ID = $this->session->ID;
		$this->db->insert('history', $this->params);
		$key = substr($this->params->ID,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',(int)$this->params->kd_prov);
		$this->db->order_by('kdDep','asc');
		$this->db->limit(1);
		return $this->db->get('aceh')->result();
	}
	public function aceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Entitas";
		//$this->params->ID = $this->session->username;
		$this->db->insert('history', $this->params);
		$this->db->select('aceh.*');
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	// public function bts()
	// {
	// 	$this->params->tanggal = date("Y-m-d");
	// 	$this->params->waktu = date("H:i:s");
	// 	$this->params->tglwkt = date("Y-m-d H:i:s");
	// 	$this->params->log = "Melihat Daftar BTS Wrap";
	// 	$this->db->insert('history', $this->params);
	// 	$this->db->select('b.DESA, b.PROV, b.sumber_data, ab.Nama as namadata, ab.kdDep as kodedata, if(mid(a.kdDep,4,2)=00 and left(ab.kdDep,2) = left(a.kdDep,2),a.Nama,"") as prs');
	// 	$this->db->join('aceh ab','ab.Nama = b.DESA');
	// 	$this->db->join('aceh a','left(ab.kdDep,2) = left(a.kdDep,2)');
	// 	return $this->db->get('bts b')->result();		
	// }
	public function report_bts()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Data Report BTS";
		$this->db->insert('history', $this->params);
		$this->db->select('bts.*, aceh.kdDep, aceh.Nama');
		$this->db->join('aceh','bts.DESA = aceh.Nama');
		return $this->db->get('bts')->result();		
	}
	public function report_sub_bts($data,$alfa)
	{
		//$keys = substr($data, 0,2);
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Data Report BTS";
		$this->db->insert('history', $this->params);
		
		$this->db->select('aceh.Nama');
		$this->db->where('left(kdDep,2)',$data);
		$this->db->where('mid(kdDep,3,3)',$alfa);
		//$this->db->group_by('kdDep');
		//$this->db->where('mid(aceh.kdDep,7,2)',00);
		//$this->db->where('right(aceh.kdDep,4)',0000);
		//$this->db->limit(1000);
		return $this->db->get('aceh')->result();		
	}
	public function bts_backup()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar BTS Wrap";
		$this->db->insert('history', $this->params);
		$data['bts'] = $this->db->select('bts.*, aceh.Nama as nama_data, aceh.kdDep as kode_data')->
							join('aceh','bts.DESA = aceh.Nama')->
							order_by('aceh.Nama','ASC')->
							get('bts')->result();
							$this->db->select('aceh.*')->
							where('aceh.kdDep',result()->kode_data)->
							get('aceh')->result();
		$data['bts'] = $this->db->select('b.PROV, b.KAB, b.KEC, b.DESA, ab.Nama as nama_data, ab.kdDep as kode_data, a.Nama, b.sumber_data')->
							join('aceh a','b.DESA = ab.Nama')->
							join('aceh ab','left(a.kdDep,2) = left(ab.kdDep,2) and mid(a.kdDep,3,4) != mid(ab.kdDep,3,4)')->
							order_by('b.DESA','ASC')->
							get('bts b')->result();
		return $data;
		// $this->db->select('b.*, ab.Nama as namadata, ab.kdDep as kodedata, a.Nama as prs');
		// $this->db->join('aceh ab','b.DESA = ab.Nama');
		// $this->db->join('aceh a','left(a.kdDep,2) = left(ab.kdDep,2)','left');
		// return $this->db->get('bts b')->result();
		// $this->db->select('wrapbts.*');
		// $this->db->order_by('wrapbts.kdDep','asc');
		// $this->db->limit(500);
		//return $this->db->get('wrapbts')->result();
		// $data['bts'] = $this->db->select('wrapbts.*')->
		// 				order_by('wrapbts.kdDep','ASC')->
		// 				limit(0,500)->
		// 				get('wrapbts')->result();
							
		// $data['aceh'] = $this->db->select('aceh.*')->
		// 						where('aceh.kdDep',$data['bts']->kdDep)->
		// 						get('aceh')->result();
		//return $data;		
	}
	public function count_entitas_kabupaten()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Laporan Jumlah Entitas Kabupaten";
		$this->db->insert('history', $this->params);

		$this->db->select('a.*, sum(if(mid(b.kdDep,4,2)<>00 AND mid(b.kdDep,7,2)=00 AND right(b.kdDep,4)=0000,1,0)) as ct_kab');
		$this->db->join('aceh b', 'left(a.kdDep,2) = left(b.kdDep,2)');
		$this->db->where('mid(a.kdDep,3,4)',00);
		$this->db->group_by('a.kdDep');
		$this->db->order_by('a.kdDep', 'asc');
		return $this->db->get('aceh a')->result();
	}
	public function count_entitas_kabupaten_parameter($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Laporan Jumlah Entitas Kabupaten";
		$this->db->insert('history', $this->params);
		$keys = substr($data,0,2);
		$this->db->select('kabupaten.*');
		$this->db->where('kabupaten.Prov',$data);
		// $this->db->group_by('kdDep');
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('kabupaten')->result();
	}
	public function count_entitas_kecamatan()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Laporan Jumlah Entitas Kecamatan";
		$this->db->insert('history', $this->params);

		$this->db->select('a.*, sum(if(mid(b.kdDep,4,2)<>00 AND mid(b.kdDep,7,2) <> 00 AND right(b.kdDep,4)=0000,1,0)) as ct_kec');
		$this->db->join('aceh b', 'left(a.kdDep,2) = left(b.kdDep,2)');
		$this->db->where('mid(a.kdDep,3,4)',00);
		$this->db->group_by('a.kdDep');
		$this->db->order_by('a.kdDep', 'asc');
		return $this->db->get('aceh a')->result();
	}
	public function count_entitas_kecamatan_parameter($key)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Jumlah Entitas Kecamatan";
		$this->db->insert('history', $this->params);
		$keys = substr($key,0,2);
		$this->db->select('a.*, sum(if(mid(b.kdDep,4,2)<>00 AND mid(b.kdDep,7,2) <> 00 AND right(b.kdDep,4)=0000,1,0)) as ct_kec');
		$this->db->join('aceh b', 'left(a.kdDep,2) = left(b.kdDep,2)');
		$this->db->where('left(a.kdDep,2)',$keys);
		$this->db->where('mid(a.kdDep,3,4)',00);
		$this->db->group_by('a.kdDep');
		$this->db->order_by('a.kdDep', 'asc');
		return $this->db->get('aceh a')->result();
	}
	public function count_entitas_desa()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Laporan Jumlah Entitas Desa";
		$this->db->insert('history', $this->params);

		$this->db->select('a.*, sum(if(mid(b.kdDep,4,2)<>00 AND mid(b.kdDep,7,2) <> 00 AND right(b.kdDep,4)<>0000,1,0)) as ct_desa');
		$this->db->join('aceh b', 'left(a.kdDep,2) = left(b.kdDep,2)');
		$this->db->where('mid(a.kdDep,3,4)',00);
		$this->db->group_by('a.kdDep');
		$this->db->order_by('a.kdDep', 'asc');
		return $this->db->get('aceh a')->result();
	}
	public function count_entitas_desa_parameter($key)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Jumlah Entitas Desa";
		$this->db->insert('history', $this->params);
		$id_keys = substr($key,0,2);
		$this->db->select('a.*, sum(if(mid(b.kdDep,4,2)<>00 AND mid(b.kdDep,7,2) <> 00 AND right(b.kdDep,4)<>0000,1,0)) as ct_desa');
		$this->db->join('aceh b', 'left(a.kdDep,2) = left(b.kdDep,2)');
		$this->db->where('left(a.kdDep,2)',$id_keys);
		$this->db->where('mid(a.kdDep,3,4)','00');
		$this->db->group_by('a.kdDep');
		$this->db->order_by('a.kdDep', 'asc');
		return $this->db->get('aceh a')->result();
	}
	public function count_entitas_kecamatan_expand($key, $keyas)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Jumlah Entitas Kecamatan";
		$this->db->insert('history', $this->params);
		
		$this->db->select('kecamatan.*');
		$this->db->where('kecamatan.Prov',$key);
		$this->db->where('kecamatan.Kab', $keyas);
		// $this->db->group_by('kdDep');
		// $this->db->order_by('kdDep', 'asc');
		return $this->db->get('kecamatan')->result();
	}
	public function count_entitas_desa_parameter_expand($key, $keyas, $keytos)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Jumlah Entitas Desa";
		$this->db->insert('history', $this->params);
		//$keys = substr($key,0,8);
		$this->db->select('kelurahan.*');
		$this->db->where('kelurahan.Prov',$key);
		$this->db->where('kelurahan.Kab',$keyas);
		$this->db->where('kelurahan.Kec',$keytos);
		// $this->db->group_by('kdDep');
		// $this->db->order_by('kdDep', 'asc');
		return $this->db->get('kelurahan')->num_rows();
	}
	public function history()
	{
		$data->tanggal = date('Y-m-d');
		$data->waktu = date('H:i:s');
		$data->tglwkt = date('Y-m-d H:i:s');
		$data->log = 'Melihat Daftar History';
		$this->db->insert('history', $data);
		//}
		$this->db->select('history.*');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('history')->result();
	}
	public function prov_data()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Provinsi Asli";
		$this->db->insert('history', $this->params);
		$this->db->select('provinsi.*');
		$this->db->group_by('provinsi.kdDep');
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('provinsi')->result();
	}
	public function kab_data()
	{
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)', substr($this->params->ID, 0,2));
		$this->db->where('mid(kdDep,4,2)<>',00);
		$this->db->where('mid(kdDep,7,2)',00);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function desa_data()
	{
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,8)', substr($this->params->ID, 0,8));
		$this->db->where('right(kdDep,4)<>',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function cek_data_bts()
	{
		$this->db->select('acehbts.*');
		$this->db->where('id_desa', $this->params->ID);
		return $this->db->get('acehbts')->row();
	}
	public function list_data_bts_a()
	{
		$this->db->select('acehbts.*');
		//$this->db->where('id_desa', $this->params->ID);
		$this->db->order_by('acehbts.ID','asc');
		return $this->db->get('acehbts')->result();
	}
	public function list_data_bts()
	{
		$data['bts'] = $this->db->select('acehbts.*, aceh.Nama, aceh.kdDep')->
							join('aceh','acehbts.id_desa = aceh.kdDep')->
							order_by('acehbts.ID','ASC')->
							get('acehbts')->result();
		if(!$data['bts']) return false;
		
		$data['pros'] = $this->db->select('aceh.*')->
							where('aceh.kdDep', $data['bts']->kdDep)->
							get('aceh')->result();
		return $data;
		// $this->db->select('acehbts.*, aceh.Nama, aceh.kdDep');
		// //$this->db->where('id_desa', $this->params->ID);
		// $this->db->join('aceh','acehbts.id_desa = aceh.kdDep');
		// $this->db->order_by('acehbts.ID','asc');
		// return $this->db->get('acehbts')->result();
	}
	public function get_fails()
	{
		$this->db->select('aceh.*');
		$this->db->where('aceh.kdDep', $this->params->ID);
		$this->db->get('aceh')->row();
	}
	public function max_data_bts()
	{
		$this->db->select('MAX(ID)');
		$this->order_by('ID','desc');
		return $this->db->get('acehbts')->row();
	}
	public function kec_data()
	{
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,5)', substr($this->params->ID, 0,5));
		$this->db->where('mid(kdDep,7,2)<>',00);
		$this->db->where('right(kdDep,4)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function acehrep()
	{

		$this->db->select('aceh.*');
		$this->db->where('mid(kdDep,3,4)',00);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function sub_var_kec_aceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Kecamatan";
		$this->db->insert('history', $this->params);
		$key = substr($this->params->ID,0,6);
		$key2 = substr($this->params->ID,3,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,6)',$key);
		//$this->db->where('mid(kdDep,3,4)',$key2);
		$this->db->where('mid(kdDep,6,7)<>',00);
		$this->db->where('right(kdDEp,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function tracking_aceh($id_key)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Provinsi";
		$this->db->insert('history', $this->params);
		$id_keys = substr($id_key,0,2);
		return  $this->db->select('aceh.*')->
				where('mid(kdDep,3,4)<>',00)->
				where('left(kdDep,2)',$id_keys)->
				where('mid(kdDep,6,7)',00)->
				order_by('kdDep','asc')->
				get('aceh')->result();
	}
	public function tracking_kec_aceh($id_key)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Kecamatan";
		$this->db->insert('history', $this->params);
		$id_keys = substr($id_key,0,5);
		return  $this->db->select('aceh.*')->
				where('left(kdDep,5)',$id_keys)->
				where('mid(kdDep,6,7)<>',00)->
				where('right(kdDep,3)',0000)->
				order_by('kdDep','asc')->
				get('aceh')->result();
	}
	public function var_kab_aceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Kabupaten";
		$this->db->insert('history', $this->params);
		$key = substr($this->params->ID,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',$key);
		$this->db->where('mid(kdDep,3,4)<>',00);
		$this->db->where('mid(kdDep,6,7)',00);
		$this->db->where('right(kdDEp,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function var_kec_aceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Kecamatan";
		$this->db->insert('history', $this->params);
		$key = substr($this->params->ID,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',$key);
		$this->db->where('mid(kdDep,3,4)<>',00);
		$this->db->where('mid(kdDep,6,7)<>',00);
		$this->db->where('right(kdDEp,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function report_prov_group_plan()
	{
		$this->db->select('provinsi.*');
		$this->db->order_by('Nama', 'asc');
		return $this->db->get('provinsi')->result();
	}
	public function report_kab_group_plan($data)
	{
		$this->db->select('kabupaten.*');
		$this->db->where('kabupaten.Prov',$data);
		$this->db->order_by('Nama', 'asc');
		return $this->db->get('kabupaten')->result();
	}
	public function report_kec_group_plan($data,$key)
	{
		$this->db->select('kecamatan.*');
		$this->db->where('kecamatan.Prov',$data);
		$this->db->where('kecamatan.Kab',$key);
		return $this->db->get('kecamatan')->result();
	}
	public function report_kel_group_plan($data)
	{
		$key = substr($data,0,8);
		$this->db->select('count(no) as total_kel');
		$this->db->where('left(kelurahan.kdDep,8)',$key);
		return $this->db->get('kelurahan')->result();
	}
	public function report_prov_kec($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Provinsi";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',$key);
		$this->db->where('mid(kdDep,3,4)',00);
		$this->db->where('mid(kdDep,6,7)',00);
		$this->db->where('right(kdDep,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function report_kab_kec($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Kabupaten";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',$key);
		$this->db->where('mid(kdDep,3,4)',00);
		$this->db->where('mid(kdDep,6,7)',00);
		$this->db->where('right(kdDep,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function report_vic_kab_kec($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Kabupaten";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',$key);
		$this->db->where('mid(kdDep,3,4)<>',00);
		$this->db->where('mid(kdDep,6,7)',00);
		$this->db->where('right(kdDep,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function report_kec_kec($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Kecamatan";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',$key);
		$this->db->where('mid(kdDep,3,4)<>',00);
		$this->db->where('mid(kdDep,6,7)<>',00);
		$this->db->where('right(kdDep,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function report_desa_desa($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Desa";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',$key);
		$this->db->where('mid(kdDep,3,4)<>',00);
		$this->db->where('mid(kdDep,6,7)<>',00);
		$this->db->where('right(kdDep,3)<>',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function report_kab_kab($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Kabupaten";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,6);
		//$keyto = substr($data,4,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,6)',$key);
		$this->db->where('mid(kdDep,6,7)',00);
		$this->db->where('right(kdDep,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	//all data kecamatan
// 	public function report_kab_kab($data)
// 	{
// 		$key = substr($data,0,4);
// 		//$keyto = substr($data,4,2);
// 		$this->db->select('aceh.*');
// 		$this->db->where('left(kdDep,4)',$key);
// //		$this->db->where('mid(kdDep,3,4)',11);
// 		$this->db->where('mid(kdDep,6,7)',00);
// 		$this->db->where('right(kdDep,3)',0000);
// 		$this->db->order_by('kdDep', 'asc');
// 		return $this->db->get('aceh')->result();
// 	}
	public function report_div_list_kec($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Kecamatan";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,6);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,6)',$key);
		$this->db->where('right(kdDep,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function report_list_kec($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Kecamatan";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,9);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,9)',$key);
		$this->db->where('right(kdDep,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function report_list_desa($data)
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Desa";
		$this->db->insert('history', $this->params);
		$key = substr($data,0,9);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,9)',$key);
		$this->db->where('right(kdDep,3)<>',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function sub_var_desa_aceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Menggabungkan Laporan Daftar Desa";
		$this->db->insert('history', $this->params);
		$key = substr($this->params->ID,0,9);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,9)',$key);
		$this->db->where('right(kdDEp,3)<>',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function var_desa_aceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Desa";
		$this->db->insert('history', $this->params);
		$key = substr($this->params->ID,0,2);
		$this->db->select('aceh.*');
		$this->db->where('left(kdDep,2)',$key);
		$this->db->where('mid(kdDep,3,4)<>',00);
		$this->db->where('mid(kdDep,6,7)<>',00);
		$this->db->where('right(kdDEp,3)<>',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function prov_aceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Provinsi";
		$this->db->insert('history', $this->params);
		$this->db->select('aceh.*');
		$this->db->where('mid(kdDep,3,4)',00);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function kab_aceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Melihat Daftar Kabupaten";
		$this->db->insert('history', $this->params);
		$this->db->select('aceh.*');
		$this->db->where('mid(kdDep,6,7)<>',00);
		$this->db->where('right(kdDep,3)',0000);
		$this->db->order_by('kdDep', 'asc');
		return $this->db->get('aceh')->result();
	}
	public function reportaceh()
	{
		$this->params->tanggal = date("Y-m-d");
		$this->params->waktu = date("H:i:s");
		$this->params->tglwkt = date("Y-m-d H:i:s");
		$this->params->log = "Penarikan Report Keseluruhan";
		$this->db->insert('history', $this->params);
		$this->db->select('nva_acehdata.*');
		return $this->db->get('nva_acehdata')->result();
	}
	public function tipe_search()
	{
		$this->db->select('tipe.*, user.nama_lengkap as username');
		$this->db->join('user', 'tipe.creator = user.ID');
		$this->db->where('tipe.status', 1);
		$this->db->order_by('nama_tipe', 'asc');
		return $this->db->get('tipe')->result();
	}

	public function tipe()
	{
		$this->db->select('tipe.*');
	//	$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('tipe.status', 1);
		$this->db->order_by('nama_tipe', 'asc');
		return $this->db->get('tipe')->result();
	}

	public function instruksi()
	{
		$this->db->select('instruksi.*');
	//	$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->order_by('judul', 'asc');
		return $this->db->get('instruksi')->result();
	}

	public function dp_key()
	{
		$this->db->select('dp_key.*, user.nama_lengkap as username');
		$this->db->join('user', 'dp_key.creator = user.ID');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('dp_key')->result();
	}
	public function running()
	{
		$this->db->select('running.*, user.nama_lengkap as username');
		$this->db->join('user', 'running.creator = user.ID');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('running')->result();
	}

	public function dp_warna()
	{
		$this->db->select('warna.*');
		//$this->db->join('user', 'dp_key.creator = user.ID');
		$this->db->order_by('ID', 'asc');
		return $this->db->get('warna')->result();
	}
	public function submit_aceh()
	{
		if(!isset($this->params->kdDep)){
			return $this->db->insert('aceh', $this->params);
		} else {
			return $this->db->where('kdDep', $this->params->kdDep)->update('aceh',$this->params);
		}
	}
	public function submit_tipe_x()
	{
		if(!isset($this->params->ID)){
			$this->params->id_tipe = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('tipe', $this->params);
		} else {
			return $this->db->where('ID', $this->params->ID)->update('tipe',$this->params);
		}
	}
	public function hapus_warna()
	{
		return $this->db->where('ID', $this->params->ID)->delete('warna',$this->params);		
	}
	public function submit_instruksi()
	{

			$this->params->tgl = date("Y-m-d");
			$this->params->tglwaktu = date("Y-m-d H:i:s");
			return $this->db->insert('instruksi', $this->params);
	}
	public function provinsi()
	{
		$this->db->select('provinsi.*, user.nama_lengkap as username');
		$this->db->join('user', 'provinsi.creator = user.ID');
		$this->db->where('provinsi.status', 1);
		$this->db->order_by('nama_provinsi', 'asc');
		return $this->db->get('provinsi')->result();
	}

	public function unitkerja()
	{
		$this->db->select('unitkerja.*, user.nama_lengkap as username, provinsi.nama_provinsi');
		$this->db->join('user', 'unitkerja.creator = user.ID');
		$this->db->join('provinsi', 'unitkerja.id_provinsi = provinsi.ID');
		$this->db->where('unitkerja.status', 1);
		$this->db->order_by('nama_unit', 'asc');
		return $this->db->get('unitkerja')->result();
	}

	public function ppk()
	{
		$this->db->select('ppk.*, user.nama_lengkap as username, unitkerja.nama_unit');
		$this->db->join('user', 'ppk.creator = user.ID');
		$this->db->join('unitkerja', 'ppk.id_unit = unitkerja.ID');
		$this->db->where('ppk.status', 1);
		$this->db->order_by('nama_ppk', 'asc');
		return $this->db->get('ppk')->result();
	}

	public function submit_ppk()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_date = date("Y-m-d");
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('ppk', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('ppk',$this->params);
		}
	}

	public function submit_provinsi()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('provinsi', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('provinsi',$this->params);
		}
	}

	public function submit_unitkerja()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_date = date("Y-m-d");
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('unitkerja', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('unitkerja',$this->params);
		}
	}

	public function submit_key()
	{
		if(!isset($this->params->hash_id)){
			$this->db->truncate('dp_key');
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_date = date("Y-m-d");
			$this->params->created_time = date("Y-m-d H:i:s");

			return $this->db->insert('dp_key', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('dp_key',$this->params);
		}
	}

	public function submit_running()
	{
		if(!isset($this->params->hash_id)){
			$this->db->truncate('running');
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_date = date("Y-m-d");
			$this->params->created_time = date("Y-m-d H:i:s");

			return $this->db->insert('running', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('running',$this->params);
		}
	}

	public function submit_warna()
	{
		if(!isset($this->params->hash_id)){
			$this->db->where('tipe', $this->params->tipe)->delete('warna');
			$this->params->hash_id = uniqid();
			//$this->params->creator = $this->session->ID;
			//$this->params->created_date = date("Y-m-d");
			//$this->params->created_time = date("Y-m-d H:i:s");

			return $this->db->insert('warna', $this->params);
		} else {
			$this->db->where('tipe', $this->params->tipe)->delete('warna');
			$this->params->hash_id = uniqid();
			return $this->db->insert('warna', $this->params);
//			return $this->db->where('ID', $this->params->ID)->update('warna',$this->params);
		}
	}

	public function kota()
	{
		$this->db->select('kota.*, user.nama_lengkap as username, user.email, provinsi.nama_provinsi');
		$this->db->join('user', 'kota.creator = user.ID');
		$this->db->join('provinsi', 'kota.id_provinsi = provinsi.ID');
		if(isset($this->params->id_provinsi)&&$this->params->id_provinsi) $this->db->where('kota.id_provinsi', $this->params->id_provinsi);
		$this->db->where('kota.status', 1);
		$this->db->order_by('nama_kota', 'asc');
		return $this->db->get('kota')->result();
	}

	public function submit_kota()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			return $this->db->insert('kota', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('kota',$this->params);
		}
	}

	 public function ruas_jalan_provinsi()
	 {
	 	$this->db->select('ruas_jalan.*, user.nama_lengkap as username, user.email, ppk.nama_ppk');
		$this->db->join('user', 'ruas_jalan.creator = user.ID');
		$this->db->join('ppk', 'ruas_jalan.id_ppk = ppk.ID');
		if(isset($this->params->id_ppk)&&$this->params->id_ppk) $this->db->where('ruas_jalan.id_ppk', $this->params->id_ppk);
		$this->db->where('ruas_jalan.status', 1);
		$this->db->order_by('nama_ruas_jalan', 'asc');
		return $this->db->get('ruas_jalan')->result();
	 }

	 public function ruas_jalan_provinsi_ppk()
	 {
	 	$this->db->select('ruas_jalan.*, user.nama_lengkap as username, user.email, ppk.nama_ppk');
		$this->db->join('user', 'ruas_jalan.creator = user.ID','left');
		$this->db->join('ppk', 'ruas_jalan.id_ppk = ppk.ID','left');
		if(isset($this->params->id_ppk)&&$this->params->id_ppk) $this->db->where('ruas_jalan.id_ppk', $this->params->id_ppk);
		$this->db->where('ruas_jalan.status', 1);
		$this->db->order_by('nama_ruas_jalan', 'asc');
		return $this->db->get('ruas_jalan')->result();
	 }

	public function unit_kerja_isi()
	 {
	 	$this->db->select('unitkerja.*, provinsi.nama_provinsi');
		//$this->db->join('user', 'unitkerja.creator = user.ID');
		$this->db->join('provinsi', 'unitkerja.id_provinsi = provinsi.ID');
		if(isset($this->params->id_provinsi)&&$this->params->id_provinsi) $this->db->where('unitkerja.id_provinsi', $this->params->id_provinsi);
		$this->db->where('unitkerja.status', 1);
		$this->db->order_by('nama_unit', 'asc');
		return $this->db->get('unitkerja')->result();
	}
	public function ppk_isi()
	 {
	 	$this->db->select('ppk.*, unitkerja.nama_unit');
		//$this->db->join('user', 'unitkerja.creator = user.ID');
		$this->db->join('unitkerja', 'ppk.id_unit = unitkerja.ID');
		if(isset($this->params->id_unit)&&$this->params->id_unit) $this->db->where('ppk.id_unit', $this->params->id_unit);
		$this->db->where('ppk.status', 1);
		$this->db->order_by('nama_ppk', 'asc');
		return $this->db->get('ppk')->result();
	}

	public function ruas_jalan()
	{
		$this->db->select('ruas_jalan.*');
		//$this->db->join('user', 'ruas_jalan.creator = user.ID');
		//$this->db->join('provinsi', 'ruas_jalan.id_provinsi = provinsi.ID','left');
		//$this->db->join('kota', 'ruas_jalan.id_kota = kota.ID','left');
		$this->db->where('ruas_jalan.status', 1);
		$this->db->order_by('nama_ruas_jalan', 'asc');
		return $this->db->get('ruas_jalan')->result();
	}

	public function submit_ruas_jalan()
	{
		if(!isset($this->params->hash_id)){
			$this->params->hash_id = uniqid();
			$this->params->creator = $this->session->ID;
			$this->params->created_time = date("Y-m-d H:i:s");
			$this->params->remark = "None";
			return $this->db->insert('ruas_jalan', $this->params);
		} else {
			return $this->db->where('hash_id', $this->params->hash_id)->update('ruas_jalan',$this->params);
		}
	}

}
