<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MKS {

	public $ci;

	public function subInstruksi(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('ket')->get('instruksi')->result();
		return $sub;
	}

	public function subKategori(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('status','PENDING')->get('perijinan')->result();
		return $sub;
	}
	public function subTerima(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('status','PROCESSING')->where('apv_status','TERIMA')->where('DATEDIFF(tanggal_data,CURDATE()) <=',2)->get('perijinan')->result();
		return $sub;
	}
	public function subTerimaUser(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('rw_user','0')->where('status <>','PENDING')->get('perijinan')->result();
		return $sub;
	}
	public function subTerimaClient($keyword){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('rw_client','0')->where('apv_status <>','TERIMA')->where('creator',$keyword)->get('perijinan')->result();
		return $sub;
	}
	public function subEkspos(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('status','PROCESSING')->where('apv_status','EKSPOSE')->where('DATEDIFF(tanggal_data,CURDATE()) <=',2)->get('perijinan')->result();
		return $sub;
	}
	public function subSurvei(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('status','PROCESSING')->where('apv_status','SURVEI')->where('DATEDIFF(tanggal_data,CURDATE()) <=',2)->get('perijinan')->result();
		return $sub;
	}
	public function subHasilRapat(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('status','PROCESSING')->where('apv_status','HASIL RAPAT')->where('DATEDIFF(tanggal_data,CURDATE()) <=',2)->get('perijinan')->result();
		return $sub;
	}
	public function subPersetujuan(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('status','PROCESSING')->where('apv_status','PERSETUJUAN')->where('DATEDIFF(tanggal_data,CURDATE()) <=',2)->get('perijinan')->result();
		return $sub;
	}
	public function subRekomtek(){
		$this->ci =& get_instance();
		$sub = $this->ci->db->select('COUNT(*) AS ID')->where('status','ACCEPT')->where('apv_status','REKOMTEK')->where('DATEDIFF(tanggal_data,CURDATE()) <=',2)->get('perijinan')->result();
		return $sub;
	}

}
?>