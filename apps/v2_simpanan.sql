-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 12, 2016 at 07:07 
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koppasv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `v2_simpanan`
--

CREATE TABLE `v2_simpanan` (
  `ID` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `simpanan_pokok` int(11) NOT NULL,
  `simpanan_wajib` int(11) NOT NULL,
  `simpanan_wajib_khusus` int(11) NOT NULL,
  `simpanan_sukarela` int(11) NOT NULL,
  `remark` text NOT NULL,
  `creator` int(11) NOT NULL,
  `created_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v2_simpanan`
--

INSERT INTO `v2_simpanan` (`ID`, `client`, `simpanan_pokok`, `simpanan_wajib`, `simpanan_wajib_khusus`, `simpanan_sukarela`, `remark`, `creator`, `created_time`) VALUES
(1, 3, 1000000, 125000, 500000, 25000, 'Simpanan pertama', 1, '2016-09-12 09:33:12'),
(2, 3, 2000000, 300000, 100000, 50000, 'Simpanan kedua', 1, '2016-09-12 09:34:00'),
(3, 2, 5000000, 250000, 150000, 80000, 'Simpanan pertama Susi Susanti', 1, '2016-09-12 10:38:54'),
(6, 2, 0, 0, 100000, 0, 'SWK <strong><a class="text-orange" href="#/pinjaman_anggota/detail/6cac88b97e2e14a56e0b2b07adfb0a4c">PA0000001</a></strong>', 1, '2016-09-12 19:03:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `v2_simpanan`
--
ALTER TABLE `v2_simpanan`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `v2_simpanan`
--
ALTER TABLE `v2_simpanan`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
