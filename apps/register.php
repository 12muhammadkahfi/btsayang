<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Pendaftaran Perijinan RUMIJA BBPJN6</title>
  <base href="<?php echo $_SERVER['SERVER_NAME'] === "localhost" ? "http://localhost/perijinan/" : "" ?>">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/admin/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css">
  <style type="text/css">
    .login-box, .register-box {width: auto;margin: 2% auto !important;}
  </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition register-page">
<form action="index.php/welcome/doregister" method="POST" role="form">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px;">
    <div class="register-logo">
      <a href="../../index2.html"><b>PENDAFTARAN SIRUMIJA</b> BBPJN6</a>
    </div>
  </div>
</div>
<div class="row" style="background: #FFF;">
<div class="col-md-6 col-sm-12">
<div class="register-box">
  <div class="register-box-body">
    <h4>Info Pendaftaran</h4>

      <div class="form-group has-feedback">
        <select name="tipe_organisasi" id="inputTipe_organisasi" class="form-control" required="required">
          <option value="">Pilih tipe organisasi</option>
          <option>PERORANGAN</option>
          <option>KELOMPOK MASYARAKAT</option>
          <option>ORGANISASI</option>
          <option>BADAN USAHA</option>
          <option>BADAN HUKUM</option>
          <option>INSTANSI PEMERINTAH</option>
        </select>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="nama_organisasi" type="text" class="organisasi form-control" placeholder="Nama organisasi">
        <span class="glyphicon glyphicon-home form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="pimpinan_organisasi" type="text" class="organisasi form-control" placeholder="Nama pimpinan organisasi">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="alamat_organisasi" type="text" class="organisasi form-control" placeholder="Alamat organisasi">
        <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="telp_organisasi" type="text" class="organisasi form-control" placeholder="No. Telp organisasi">
        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="fax_organisasi" type="text" class="organisasi form-control" placeholder="No. Fax organisasi">
        <span class="glyphicon glyphicon-print form-control-feedback"></span>
      </div>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
</div>
<div class="col-md-6 col-sm-12">
<div class="register-box">
  <div class="register-box-body">
    <h4>Info Personal dan Akun Login</h4>

      <div class="form-group has-feedback">
        <input required="required" name="nama_lengkap" type="text" class="form-control" placeholder="Nama lengkap anda">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="jabatan" type="text" class="organisasi form-control" placeholder="Jabatan dalam organisasi">
        <span class="glyphicon glyphicon-credit-card form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="email" type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input required="required" name="repassword" type="password" class="form-control" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Sudah punya akun? <a href="#">Login Disini</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Daftar</button>
        </div>
        <!-- /.col -->
      </div>

  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
</div>
<!-- jQuery 2.2.3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    $("select[name=tipe_organisasi]").change(function(){
      var tipe_organisasi = $(this).val();
      if(tipe_organisasi==="PERORANGAN"){
        $(".organisasi").removeAttr("required").attr("disabled","disabled").css("color","transparent");
      } else {
        $(".organisasi").removeAttr("disabled").attr("required","required").removeAttr("style");
      }
    });
  });
</script>
</div>
</div>
</div>
</div>
</body>
</form>
</html>