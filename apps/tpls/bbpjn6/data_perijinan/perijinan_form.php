<style type="text/css">
	.wrpTable{
		max-height: 700px;
		overflow-x: auto;
		width: auto;
	}
</style>
<section class="content">
	<form ng-submit="submitpermohonan()">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header">
					<h3 class="box-title" ng-bind="form_title"></h3>
				</div>
				<div class="box-body">
					<div class="row">
						<!-- {{mysession}} -->

						<div class="col-md-5 col-lg-5">
							<h4 class="text-orange"><i class="fa fa-pencil"></i> Detail Permohonan</h4>
							<div style="margin-bottom: 45px;" ng-show="mysession.level!=='5'" class="form-group">
								<label for="tanggal_data">Tanggal Pengajuan Diterima</label>
								<!-- {{FormData.tanggal_surat}} -->
								<datepicker
								date-format="dd/MM/yyyy">
								<input
									type="text" 
									id="tanggal_data"
									class="form-control" 
									ng-required="mysession.level!=='5'" 
									onkeyup="return false"
									onkeypress="return false"
									onkeydown="return false"
									ng-disabled="mysession.level==='5'" 
									ng-model="FormData.tanggal_data" 
									placeholder="" />
								</datepicker>
							</div>
							<div class="form-group">
								<label for="nomor_surat">Nomor Surat</label>
								<input
									type="text" 
									id="nomor_surat"
									class="form-control" 
									ng-required="true" 
									ng-readonly="false" 
									ng-model="FormData.nomor_surat" 
									placeholder="" />
							</div>
							<div class="form-group">
								<label for="tanggal_surat">Tanggal Surat</label>
								<!-- {{FormData.tanggal_surat}} -->
								<datepicker
								date-format="dd/MM/yyyy">
								<input
									type="text" 
									id="tanggal_surat"
									class="form-control" 
									ng-required="true" 
									onkeyup="return false"
									onkeypress="return false"
									onkeydown="return false"
									ng-readonly="false" 
									ng-model="FormData.tanggal_surat" 
									placeholder="" />
								</datepicker>
							</div>
							<div class="form-group" style="margin-top: 50px;">
								<label for="contact_person">Contact Person</label>
								<input
									type="text" 
									id="contact_person"
									class="form-control" 
									ng-required="true" 
									ng-readonly="false" 
									ng-model="FormData.contact_person" 
									placeholder="" />
							</div>
							<div class="form-group">
								<label for="telp">No. Telp / Handphone</label>
								<input
									type="text" 
									id="telp"
									class="form-control" 
									ng-required="true" 
									ng-readonly="false" 
									ng-model="FormData.telp" 
									placeholder="" />
							</div>
							<div class="form-group">
								<label for="jenis_permohonan">Jenis Permohonan</label>
								<select
									class="form-control" 
									id="jenis_permohonan"
									ng-required="true" 
									ng-readonly="false"
									
									ng-model="FormData.jenis_permohonan">
								<option value="">Pilih Jenis Permohonan</option>
								<option 
									ng-repeat="(key, value) in tipe_perijinan" 
									value="{{value.ID}}" 
									ng-bind="value.nama_tipe"></option>
								</select>
							</div>
							<div class="form-group">
								<label for="provinsi">Provinsi</label>
								<select
									class="form-control" 
									id="provinsi"
									ng-required="true" 
									ng-readonly="false"
									ng-change="get_unit_kerja()" 
									ng-model="FormData.provinsi">
								<option value="">Pilih Jenis Provinsi</option>
								<option 
									ng-repeat="(key, value) in provinsi" 
									value="{{value.ID}}" 
									ng-bind="value.nama_provinsi"></option>
								</select>
							</div>
							 <div class="form-group">
								<label for="unit_kerja">Unit Kerja</label>
								<select
									class="form-control" 
									id="unit_kerja"
									ng-required="true" 
									ng-readonly="false"
									ng-change="get_ppk()"
									ng-model="FormData.unit_kerja">
								<option value="">Pilih Unit Kerja</option>
								<option 
									ng-repeat="(key, value) in unit_kerja" 
									value="{{value.ID}}" 
									ng-bind="value.nama_unit"></option>
								</select>
							</div>
							<div class="form-group">
								<label for="ppk">PPK</label>
								<select
									class="form-control" 
									id="ppk"
									ng-required="true" 
									ng-readonly="false"
									ng-change="get_ruas()"
									ng-model="FormData.ppk">
								<option value="">Pilih PPK</option>
								<option 
									ng-repeat="(key, value) in ppk" 
									value="{{value.ID}}" 
									ng-bind="value.nama_ppk"></option>
								</select>
							</div>
							<div class="form-group">
								<label for="ruas_jalan">Ruas Jalan</label>
								<select
									class="form-control" 
									id="ruas_jalan"
									ng-required="true" 
									ng-readonly="false"
									style="word-wrap:break-word;width:100%;"" 
									ng-model="FormData.ruas_jalan">
								<option value="">Pilih Nama Ruas Jalan</option>
								<option
									ng-repeat="(key, value) in ruas_jalan" 
									value="{{value.ID}}" 
									alt="value.nama_ruas_jalan"
									style="word-wrap: break-word;"
									ng-bind="value.nama_ruas_jalan"></option>
								</select>
							</div>
							
							<div class="form-group">
								<label for="perihal">Perihal</label>
								<input
									type="text" 
									id="perihal"
									class="form-control" 
									ng-required="true" 
									ng-readonly="false" 
									ng-model="FormData.perihal" 
									placeholder="" />
							</div>
							<div class="form-group">
								<label for="konten">Keterangan</label>
								<textarea
									id="konten"
									class="form-control" 
									ng-required="true" 
									ng-readonly="false" 
									ng-model="FormData.keterangan" 
									rows="5"
									placeholder=""></textarea>
							</div>
							<div class="form-group" ng-show="files_success.name.length>=6">
								<hr/>
								<button type="submit" class="btn btn-primary">Submit</button>
								<a href="#/data_perijinan" class="btn btn-danger">Batal</a>
							</div>
						</div>
						<div class="wrpTable">
						<div >
							<h4 class="text-orange"><i class="fa fa-paperclip"></i> Lampiran Permohonan</h4>
							<div class="form-group">
								<label for="model">Upload Lampiran (File gambar / PDF)</label>
								<progress ng-show="onUpload" value="{{progressPercentage}}" max="100">
							</div>
							
							<div ng-show="files_success.name.length == 0" 
								 ngf-drop="uploadFiles($files)" 
								 class="drop-box"
								 ngf-select="uploadFiles($files)"
							  	 ngf-drag-over-class="'dragover'" ngf-multiple="true" 
							  	 ngf-pattern="'image/*,application/pdf'" style="margin-top: -15px;background: #ececec;display: block;padding: 30px 30px;text-align: center;border: 5px dashed #ccc;">
							  	 Klik atau drop file disini.<br/>Upload satu atau beberapa file sekaligus.
							</div>
							<div ngf-no-file-drop>File Drag/Drop is not supported for this browser</div>
							
							<div ng-show="files_success.name.length == 0" class="callout callout-info" style="margin-top: 10px;">
				                <h4>Info</h4>

				                <p>
				                	Anda hanya diijinkan mengupload file gambar atau pdf saja dengan ukuran maksimum setiap file 20 MB.
				                </p>
				            </div>
							<ul class="list-group" style="margin: -15px 0px 5px 0px;">
								<li style="{{key%2!=0 ? 'background: #f6f6f6;' : ''}}" ng-show="value&&value.length>0&&files_success.size[key]>0" class="list-group-item" ng-repeat="(key, value) in files_success.name">
								<table width="100%">
									<tr>
										<td width="84">
											<img ng-show="files_success.type[key][0]==='image'" width="84" class="pull-left" ng-src="tmps/{{mysession.ID}}/{{value}}" />
											<img ng-if="files_success.type[key][1]==='pdf'" width="84" class="pull-left" ng-src="assets/pdf-flat.png" />
										</td>
										<td style="padding-left: 20px;" valign="top">
											<!-- <span class="badge" ng-bind="(files_success.size[key]/1000 | number:0) + ' KB'"></span> -->
											<button ng-click="$parent.delattach(key)" style="margin: -2px 5px -2px -5px;" type="button" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i> Hapus</button>
											<span ng-bind="value"></span>
											<div class="form-group" style="padding: 0px;margin: 5px 0px 0px 0px;">
												<label for="model">Keterangan File</label>
												<input
													type="text" 
													id="model"
													class="form-control" 
													ng-required="true" 
													ng-readonly="false" 
													ng-model="files_success.remark[key]" 
													placeholder="Contoh : Surat Permohonan" />
											</div>
										</td>
									</tr>
								</table>
								</li>
							</ul>
							<a ngf-select="uploadFiles($files)" ngf-multiple="true" ng-show="files_success.name.length > 0" href="javascript:void(0)" class="btn btn-default"><i class="fa fa-upload"></i> Upload File Lainnya</a>
						</div>
						</div>
					</div>
				</div>
				<div class="box-footer" ng-show="files_success.name.length<6">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="#/data_perijinan" class="btn btn-danger">Batal</a>
				</div>
			</div>
		</div>
	</div>
	</form>
</section>