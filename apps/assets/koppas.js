String.prototype.ucFirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

Date.prototype.addMonths = function(monthOffset){
    var dt = new Date(this);
    dt.setMonth(dt.getMonth()+ monthOffset);
    if (dt.getDate() < this.getDate()) { dt.setDate(0); }
    return dt;
};

angular.module('koppas', ['ngRoute','angucomplete-alt','fcsa-number','ngFileUpload','720kb.datepicker','chart.js','datatables','ngResource']).config(function($routeProvider,$httpProvider,$provide,ChartJsProvider) {
    // ChartJsProvider.setOptions({ colors : ['#FDB45C', '#949FB1', '#4D5360'] });    
    $routeProvider.when("/", {
        templateUrl : base_url("tpls/dashboard.html"),
        controller : "db"
    });
    //Inline Data Aceh
    $routeProvider.when("/master_data/kawil_aceh", {
        templateUrl : base_url("tpls/master/aceh.html"),
        controller : "kwlaceh"
    });
    $routeProvider.when("/master_data/listprovinsi", {
        templateUrl : base_url("tpls/tplMaster/provinsi.html"),
        controller : "tplprovinsi"
    });
    $routeProvider.when("/master_data/listkabupaten", {
        templateUrl : base_url("tpls/tplMaster/kabupaten.html"),
        controller : "tpl_kabupaten"
    });
    $routeProvider.when("/master_data/listkecamatan", {
        templateUrl : base_url("tpls/tplMaster/kecamatan.html"),
        controller : "tpl_kecamatan"
    });
    $routeProvider.when("/master_data/listkelurahan", {
        templateUrl : base_url("tpls/tplMaster/kelurahan.html"),
        controller : "tpl_kelurahan"
    });
    $routeProvider.when("/transaction/real_bts", {
        templateUrl : base_url("tpls/tplMaster/realisasi_bts.html"),
        controller : "tpl_realisasibts"
    });
    $routeProvider.when("/transaction/real_bts/edit", {
        templateUrl : base_url("tpls/tplMaster/realisasi_bts_form.html"),
        controller : "tpl_realisasibts"
    });
    $routeProvider.when("/transaction/real_btswrap", {
        templateUrl : base_url("tpls/tplMaster/realisasi_bts_dagri.html"),
        controller : "tpl_realisasibts_dagri"
    })
    $routeProvider.when("/transaction/plan_bts", {
        templateUrl : base_url("tpls/tplMaster/planning_bts.html"),
        controller : "tpl_planningbts"
    })
    $routeProvider.when("/transaction/plan_btswrap", {
        templateUrl : base_url("tpls/tplMaster/planning_bts_dagri.html"),
        controller : "tpl_planningbts_dagri"
    })
    $routeProvider.when("/master_data/kawil_aceh", {
        templateUrl : base_url("tpls/master/aceh.html"),
        controller : "kwlaceh"
    });
    $routeProvider.when("/master_data/kawil_aceh/edit", {
        templateUrl : base_url("tpls/master/aceh_form.html"),
        controller : "kwlaceh"
    });
    $routeProvider.when("/data_perijinan/detail/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_detail.php"),
        controller : "dp_detail"
    });
    $routeProvider.when("/master_data/kwl_kab_aceh/:id", {
        templateUrl : base_url("tpls/master/kabaceh.html"),
        controller : "kwl_kab_aceh"
    });
    $routeProvider.when("/master_data/kwl_kec_aceh/:id", {
        templateUrl : base_url("tpls/master/kecaceh.html"),
        controller : "kwl_kec_aceh"
    });
    $routeProvider.when("/master_data/sub_kwl_kec_aceh/:id", {
        templateUrl : base_url("tpls/master/subkecaceh.html"),
        controller : "sub_kwl_kec_aceh"
    });
    $routeProvider.when("/master_data/kwl_desa_aceh/:id", {
        templateUrl : base_url("tpls/master/desaaceh.html"),
        controller : "kwl_desa_aceh"
    });
    $routeProvider.when("/master_data/sub_kwl_desa_aceh/:id", {
        templateUrl : base_url("tpls/master/subdesaaceh.html"),
        controller : "sub_kwl_desa_aceh"
    });
    $routeProvider.when("/master_data/kawil_aceh/tambah", {
        templateUrl : base_url("tpls/master/aceh_form.html"),
        controller : "kwlaceh"
    });
    $routeProvider.when("/master_data/kawil_kabupaten/tambah", {
        templateUrl : base_url("tpls/master/kabupaten_form.html"),
        controller : "kwlkabupaten"
    });
    $routeProvider.when("/master_data/kawil_aceh/edit", {
        templateUrl : base_url("tpls/master/aceh_form.html"),
        controller : "kwlaceh"
    });
    $routeProvider.when("/master_data/bts_wrap", {
        templateUrl : base_url("tpls/master/wrap_bts.html"),
        controller : "bts_wrap"
    });
    $routeProvider.when("/master_data/bts_single", {
        templateUrl : base_url("tpls/master/bts_form.html"),
        controller : "bts_wrap_single"
    });
     $routeProvider.when("/master_data/bts_single_list", {
        templateUrl : base_url("tpls/master/bts_aceh.html"),
        controller : "bts_wrap_list"
    });
    $routeProvider.when("/report_data/history", {
        templateUrl : base_url("tpls/report/history.html"),
        controller : "report_user"
    });
    $routeProvider.when("/report_data/count_entitas", {
        templateUrl : base_url("tpls/report/count_entitas.html"),
        controller : "report_count_entitas"
    });
    //End Line Aceh
    $routeProvider.when("/master_data/provinsi", {
        templateUrl : base_url("tpls/bbpjn6/master_data/provinsi.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/provinsi/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/provinsi_form.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/unitkerja", {
        templateUrl : base_url("tpls/bbpjn6/master_data/unitkerja.html"),
        controller : "muk"
    });

    $routeProvider.when("/master_data/unitkerja/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/unitkerja_form.html"),
        controller : "muk"
    });

    $routeProvider.when("/master_data/unitkerja/edit", {
        templateUrl : base_url("tpls/bbpjn6/master_data/unitkerja_form.html"),
        controller : "muk"
    });

    $routeProvider.when("/master_data/ppk", {
        templateUrl : base_url("tpls/bbpjn6/master_data/ppk.html"),
        controller : "muk"
    });

    $routeProvider.when("/master_data/key", {
        templateUrl : base_url("tpls/bbpjn6/master_data/key.html"),
        controller : "dp_key"
    });
    $routeProvider.when("/master_data/key/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/key_form.html"),
        controller : "dp_key"
    });
     $routeProvider.when("/master_data/warna", {
        templateUrl : base_url("tpls/bbpjn6/master_data/warna.html"),
        controller : "dp_warna"
    });

    $routeProvider.when("/master_data/warna/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/warna_form.html"),
        controller : "dp_warna"
    });
    $routeProvider.when("/master_data/warna/edit", {
        templateUrl : base_url("tpls/bbpjn6/master_data/warna_form.html"),
        controller : "dp_warna"
    });

    $routeProvider.when("/master_data/ppk/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/ppk_form.html"),
        controller : "muk"
    });

    $routeProvider.when("/master_data/ppk/edit", {
        templateUrl : base_url("tpls/bbpjn6/master_data/ppk_form.html"),
        controller : "muk"
    });

    $routeProvider.when("/master_data/provinsi/edit", {
        templateUrl : base_url("tpls/bbpjn6/master_data/provinsi_form.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/tipe_pembangunan", {
        templateUrl : base_url("tpls/bbpjn6/master_data/tipe_pembangunan.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/tipe_pembangunan/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/tipe_pembangunan_form.html"),
        controller : "md"
    });

//    $routeProvider.when("/master_data/instruksi", {
  //      templateUrl : base_url("configuration/index"),
    //    controller : "mst"
    //});

//     $routeProvider.when("/master_data/instruksi", {
//         site_url("http://bbpjn6.net/bbpjndev/rumija/apps/configuration/")
// //        templateUrl : base_url("tpls/bbpjn6/master_data/instruksi_form.html"),
//  //       controller : "mst"
//     });

     $routeProvider.when("/master_data/upload", {
        templateUrl : base_url("tpls/bbpjn6/master_data/upload_instruksi.html"),
        controller : "mod"
    });

    $routeProvider.when("/master_data/upload/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/upload_instruksi_form.html"),
        controller : "mod"
    });

    $routeProvider.when("/master_data/tipe_pembangunan/edit", {
        templateUrl : base_url("tpls/bbpjn6/master_data/tipe_pembangunan_form.html"),
        controller : "md"
    });

    $routeProvider.when("/terimaClient", {
        templateUrl : base_url("tpls/bbpjn6/data_notifikasi/surat_terima_klien.html"),
        controller : "mtClient"
    });

    $routeProvider.when("/terimaUser", {
        templateUrl : base_url("tpls/bbpjn6/data_notifikasi/surat_terima_user.html"),
        controller : "mtUser"
    });

    $routeProvider.when("/terima", {
        templateUrl : base_url("tpls/bbpjn6/data_notifikasi/surat_terima.html"),
        controller : "mdp"
    });

    $routeProvider.when("/ekspose", {
        templateUrl : base_url("tpls/bbpjn6/data_notifikasi/surat_ekspos.html"),
        controller : "meks"
    });

    $routeProvider.when("/survei", {
        templateUrl : base_url("tpls/bbpjn6/data_notifikasi/surat_survei.html"),
        controller : "msurv"
    });

    $routeProvider.when("/hasil_rapat", {
        templateUrl : base_url("tpls/bbpjn6/data_notifikasi/surat_hasil_rapat.html"),
        controller : "mrap"
    });

    $routeProvider.when("/persetujuan", {
        templateUrl : base_url("tpls/bbpjn6/data_notifikasi/surat_persetujuan.html"),
        controller : "mpers"
    });

    $routeProvider.when("/rekomtek", {
        templateUrl : base_url("tpls/bbpjn6/data_notifikasi/surat_rekomtek.html"),
        controller : "mdp"
    });

    $routeProvider.when("/master_data/kota", {
        templateUrl : base_url("tpls/bbpjn6/master_data/kota.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/kota/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/kota_form.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/kota/edit", {
        templateUrl : base_url("tpls/bbpjn6/master_data/kota_form.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/running", {
        templateUrl : base_url("tpls/bbpjn6/master_data/running.html"),
        controller : "mdy"
    });

    $routeProvider.when("/master_data/running/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/running_form.html"),
        controller : "mdy"
    });

    $routeProvider.when("/master_data/running/edit", {
        templateUrl : base_url("tpls/bbpjn6/master_data/running_form.html"),
        controller : "mdy"
    });

    $routeProvider.when("/master_data/ruas_jalan", {
        templateUrl : base_url("tpls/bbpjn6/master_data/ruas_jalan.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/ruas_jalan/tambah", {
        templateUrl : base_url("tpls/bbpjn6/master_data/ruas_jalan_form.html"),
        controller : "md"
    });

    $routeProvider.when("/master_data/ruas_jalan/edit", {
        templateUrl : base_url("tpls/bbpjn6/master_data/ruas_jalan_form.html"),
        controller : "md"
    });

    $routeProvider.when("/penjualan_dan_jasa/beli_barang/tambah", {
        templateUrl : base_url("tpls/penjualan_dan_jasa/beli_barang_form.html"),
        controller : "penjualan_dan_jasa"
    });

    $routeProvider.when("/pengguna", {
        templateUrl : base_url("tpls/bbpjn6/pengguna.html"),
        controller : "pengguna"
    });

    $routeProvider.when("/administrator", {
        templateUrl : base_url("tpls/bbpjn6/administrator.html"),
        controller : "administrator"
    });

    $routeProvider.when("/profil", {
        templateUrl : base_url("tpls/bbpjn6/profil.html"),
        controller : "profil"
    });

    $routeProvider.when("/administrator/tambah", {
        templateUrl : base_url("tpls/bbpjn6/administrator_form.html"),
    
        controller : "administrator_tambah"
    });

    $routeProvider.when("/pengguna/detail", {
        templateUrl : base_url("tpls/bbpjn6/pengguna_detail.html"),
        controller : "pengguna_detail"
    });

    $routeProvider.when("/data_perijinan", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan.html"),
        controller : "dpx"
    });

    $routeProvider.when("/data_import", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/import.html"),
        controller : "dpimport"
    });

    $routeProvider.when("/data_perijinan/tambah", {
        //templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_form.html"),
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_form.php"),
        controller : "dpx"
    });

    $routeProvider.when("/data_perijinan/sukses/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_detail.html"),
        controller : "dp_detail"
    });

    $routeProvider.when("/data_perijinan/terima_berkas/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_terima_berkas.html"),
        controller : "dp_terima_berkas"
    });

    $routeProvider.when("/data_perijinan/update_status/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_update_status.html"),
        controller : "dp_update_status"
    });

    $routeProvider.when("/data_perijinan/update_status_ekspose/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_update_status_ekspose.html"),
        controller : "dp_update_status"
    });

    $routeProvider.when("/data_perijinan/update_status_survei/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_update_status_survei.html"),
        controller : "dp_update_status"
    });

    $routeProvider.when("/data_perijinan/update_status_hasil_rapat/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_update_status_hasil_rapat.html"),
        controller : "dp_update_status"
    });

    $routeProvider.when("/data_perijinan/update_status_ekspose_rekomtek/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_update_status_rekomtek.html"),
        controller : "dp_update_status"
    });

    $routeProvider.when("/data_perijinan/terima_permohonan/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_terima_permohonan.html"),
        controller : "dp_terima_permohonan"
    });

    $routeProvider.when("/data_perijinan/tolak_permohonan/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_tolak_permohonan.html"),
        controller : "dp_tolak_permohonan"
    });

    $routeProvider.when("/data_perijinan/detail/:id", {
        templateUrl : base_url("tpls/bbpjn6/data_perijinan/perijinan_detail.php"),
        controller : "dp_detail"
    });

    $provide.factory('httpInterceptor', function ($q, $rootScope, $location) {
        return {
        'request': function (config) {
            var paths = $location.path().split("/");
            if(paths[1]!=="home") $rootScope.NowLoading = true;
            console.log('Loading On');
            $rootScope.$broadcast('httpRequest', config);
            return config || $q.when(config);
        },
        'response': function (response) {
            $rootScope.NowLoading = false;
            $rootScope.$broadcast('httpResponse', response);
            if(typeof response.data === 'object') console.log('Ajax Request Success',response);
            console.log('Loading Off Success');
            return response || $q.when(response);
        },
        'requestError': function (rejection) {
            $rootScope.NowLoading = false;
            $rootScope.$broadcast('httpRequestError', rejection);
            console.log('Ajax Request Error',rejection.data);
            console.log('Loading Off Request Error');
            return $q.reject(rejection);
        },
        'responseError': function (rejection) {
            $rootScope.NowLoading = false;
            $rootScope.$broadcast('httpResponseError', rejection);
            console.log('Ajax Response Error',rejection.data);
            console.log('Loading Off Response Error');
            return $q.reject(rejection);
        }
        };
    });

    $httpProvider.interceptors.push('httpInterceptor');
}).run(function($rootScope,$sce,$location,$timeout,$interval){
    $rootScope.toHtml = function( html ){
        return $sce.trustAsHtml( html );
    }
    $rootScope.page_location = 0;
    $rootScope.userdetail = function(ID){
        $rootScope.user_id_context = ID;
        $timeout(function(){
            $location.path("/pengguna/detail");
        },500);
    }
	$rootScope.page_title = "MKTREE SOURCE";
    $rootScope.mysession = mysession;
    $rootScope.tipe_perijinan = ['','Utilitas Kabel Fiber Optik','Utilitas Pipa Gas / Air','Akses Jalan Keluar - Masuk','Utilitas Kabel PLN','Pembukaan Median Jalan','Pemotongan Pohon','Media Iklan Jalan','Test Masukin Data'];
    $rootScope.tahun_berjalan = "ALFA";
    $rootScope.intval = function(stringint){
        if(!stringint) stringint = "0";
		return parseInt(stringint);
	}
    $rootScope.mydate = function(dt){
        return dt.substring(8,10) + '/' + dt.substring(5,7) + '/' + dt.substring(0,4);
    }
    $rootScope.mydate2 = function(dt){
        var month = {
            "01" : "Jan",
            "02" : "Feb",
            "03" : "Mar",
            "04" : "Apr",
            "05" : "Mei",
            "06" : "Jun",
            "07" : "Jul",
            "08" : "Agu",
            "09" : "Sep",
            "10" : "Okt",
            "11" : "Nov",
            "12" : "Des"
        };
        return dt.substring(8,10) + ' ' + month[dt.substring(5,7)] + ' ' + dt.substring(0,4);
    }
    $rootScope.mytime = function(dt){
        return dt.substring(11,16);
    }
    
    $rootScope.mydatetime = function(dt){
        return dt.substring(8,10) + '/' + dt.substring(5,7) + '/' + dt.substring(0,4) + ' '+dt.substring(11,13)+':'+dt.substring(14,16);
    }
});