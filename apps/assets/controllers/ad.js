angular.module('koppas').controller('administrator',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	$rootScope.page_title = "Administrator MKTREE SOURCE";
	$http.post(site_url('pengguna/data_administrator'),{}).success(function(data){
		if(data.status){
		$scope.pengguna = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	$scope.hapususer = function(ID,email){
		var confirm1 = $window.confirm("Anda akan menghapus administrator dengan alamat email "+email);
	    if(confirm1){
	        var confirm2 = $window.confirm("Tindakan ini tidak dapat dibatalkan, apakah anda yakin?");
	        if(confirm2){
	            $http.post(site_url('pengguna/hapus'),{ID:ID}).success(function(data){
	                if(data.status){
	                successToastAuto(data.message);
	                $route.reload();
	            } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	        }    
	    }
	}
});

angular.module('koppas').controller('administrator_tambah',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	$rootScope.page_title = "Buat Akun Administrator Baru";
	$scope.buatadmin = function(){
		$http.post(site_url('pengguna/buat_admin'),$scope.FormData).success(function(data){
			console.log(data);
			if(data.status){
			successToastAuto(data.message);
			$location.path("/administrator");
			} else {errorToastAuto(data.message)}
		}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
});

angular.module('koppas').controller('profil',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	$rootScope.page_title = "Profil Saya";
	$scope.gantipassword = function(){
		if($scope.FormData.old_password.trim().length<6){
			errorToastAuto('Panjang password lama minimal 6 karakter');
			return;
		}
		if($scope.FormData.new_password.trim().length<6){
			errorToastAuto('Panjang password minimal 6 karakter');
			return;
		}
		if($scope.FormData.new_password!==$scope.FormData.new_password_1){
			errorToastAuto('Password tidak sama, silahkan ketik ulang password.');
			return;
		}
		
		$http.post(site_url('pengguna/ganti_password'),$scope.FormData).success(function(data){
			if(data.status){
			successToastAuto(data.message);
			$scope.sidebox = false;
			$scope.FormData = {};
		} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
});