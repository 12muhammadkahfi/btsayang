angular.module('koppas').controller('pengguna',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	$rootScope.page_title = "Pengguna (Public User)";
	$http.post(site_url('pengguna/data_pengguna'),{}).success(function(data){
		if(data.status){
		$scope.pengguna = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
});

angular.module('koppas').controller('pengguna_detail',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	$rootScope.page_title = "Detail Pengguna";
	if(typeof $rootScope.user_id_context === "undefined") $location.path("/pengguna");
	$http.post(site_url('pengguna/detail'),{
		ID : $rootScope.user_id_context
		// ID : 7
	}).success(function(data){
		console.log(data);
		if(data.status){
			$scope.data = data.data;
		} else {}
	}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
});

angular.module('koppas').controller('pengguna',function($scope,$rootScope,$location,$http,$filter,$window,$route,$resource){
	$rootScope.page_title = "Pengguna (Public User)";
	$http.post(site_url('pengguna/data_pengguna'),{}).success(function(data){
		if(data.status){
		// $scope.pengguna = data.data;
		$scope.pengguna = data.data;
        $resource(data.data).query().$promise.then(function(persons) {
            vm.pengguna = persons;
        });
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	$scope.hapususer = function(ID,email){
		var confirm1 = $window.confirm("Anda akan menghapus pengguna dengan alamat email "+email);
        if(confirm1){
            var confirm2 = $window.confirm("Tindakan ini tidak dapat dibatalkan, apakah anda yakin?");
            if(confirm2){
                $http.post(site_url('pengguna/hapus'),{ID:ID}).success(function(data){
                    if(data.status){
                    successToastAuto(data.message);
                    $route.reload();
                } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
            }    
        }
	}
});