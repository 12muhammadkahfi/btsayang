angular.module('koppas').controller('report_user',function($scope,$rootScope,$location,$http,$filter,$window,$route,$sce){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.user = [];
	$scope.FormData = {};
	$scope.history = [];
	$http.post(site_url('perijinan/clear_tmps'));
	$http.post(site_url('report_controller/history'),{}).success(function(data){
	if(data.status){
		$rootScope.page_title = "MKTREE SOURCE";
		$scope.history = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
});
angular.module('koppas').controller('report_count_entitas',function($scope,$rootScope,$location,$http,$filter,$window,$route,$sce){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.user = [];
	$scope.FormData = {};
	$scope.count_entity_kabupaten = [];
	$scope.count_entity_kecamatan = [];
	$scope.count_entity_desa = [];
	$http.post(site_url('perijinan/clear_tmps'));
	$http.post(site_url('report_controller/count_entitas_kabupaten'),{}).success(function(data){
	if(data.status){
	//	$rootScope.page_title = "MKTREE SOURCE - KABUPATEN";
		$scope.count_entity_kabupaten = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	$http.post(site_url('report_controller/count_entitas_kecamatan'),{}).success(function(data){
	if(data.status){
	//	$rootScope.page_title = "MKTREE SOURCE - KECAMATAN";
		$scope.count_entity_kecamatan = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	$http.post(site_url('report_controller/count_entitas_desa'),{}).success(function(data){
	if(data.status){
	//	$rootScope.page_title = "MKTREE SOURCE - DESA";
		$scope.count_entity_desa = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
});
