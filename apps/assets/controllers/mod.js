angular.module('koppas').controller('mod',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,$resource,Upload){
    $rootScope.page_title = "Data Perijinan";
    $rootScope.form_title = "Formulir Pengajuan Ijin Pemanfaatan Ruang Milik Jalan | BBPJN6";
    $scope.files = [];
    $scope.files_success = {name:[],size:[],type:[],remark:[]};
    $scope.onUpload = false;
    $scope.progressPercentage = 0;
    $scope.provinsi = [];
    $scope.jenis_provinsi = [];
    $scope.upddk = [];
    $scope.ruas_jalan = [];
//    $scope.modif = ['A1','A2','']
    $http.post(site_url('perijinan/clear_tmps'));
    $http.post(site_url('perijinan/uploaddok'),{}).success(function(data){
        if(data.status){
        $scope.upddk = data.data;
    } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    
    $scope.delattach = function(index){
        var yes = $window.confirm("Anda yakin akan menghapus lampiran :\n\n"+$scope.files_success.name[index]);
        if(yes){
            delete $scope.files_success.size.splice(index,1);
            delete $scope.files_success.name.splice(index,1);
            delete $scope.files_success.type.splice(index,1);
            delete $scope.files_success.remark.splice(index,1);
        }
    }
    $scope.uploadFiles = function (files) {
      if (files && files.length) {
        $scope.onUpload = true;
		$timeout(function(){$rootScope.NowLoading = false;},10);
		$timeout(function(){$rootScope.NowLoading = false;},50);
		$timeout(function(){$rootScope.NowLoading = false;},350);
		$timeout(function(){$rootScope.NowLoading = false;},1000);
		Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success.name.length==0){
                $scope.files_success.name = resp.data.file.name;
                $scope.files_success.size = resp.data.file.size;
                $scope.files_success.type = resp.data.file.type;
                angular.forEach($scope.files_success.name, function(value, index){
                    $scope.files_success.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success.name.indexOf(value)<0){
                        $scope.files_success.name.push(value);
                        $scope.files_success.size.push(resp.data.file.size[index]);
                        $scope.files_success.type.push(resp.data.file.type[index]);
                        $scope.files_success.remark.push('');
                    }
                });
            }
            console.log($scope.files_success);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage)==100){
				$timeout(function(){
					$scope.onUpload = false;
				},350);
			}
		});;
      }
    }

    $scope.submitdokumen = function(){
        if($scope.files_success.name.length<1){
            errorToastAuto('Anda harus mengupload setidaknya 1 lampiran terlebih dahulu.');
            return false;
        }
        $scope.FormData.attachments = [];
        angular.forEach($scope.files_success.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                desc : $scope.files_success.remark[index]
            });
        });
      
        $http.post(site_url('perijinan/uploaddokumen'),$scope.FormData).success(function(data){
        if(data.status){
            successToastAuto(data.message);
            $location.path("data_perijinan/detail/"+data.ID);
        } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.hapusijin = function(ID){
        var confirm1 = $window.confirm("Anda akan menghapus pengajuan dengan nomor "+ID);
        if(confirm1){
            var confirm2 = $window.confirm("Tindakan ini tidak dapat dibatalkan, apakah anda yakin?");
            if(confirm2){
                $http.post(site_url('perijinan/hapus_ijin'),{ID:ID}).success(function(data){
                    if(data.status){
                    successToastAuto(data.message);
                    $route.reload();
                } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
            }    
        }
    }
    delete $rootScope.terimaberkas_context;
    delete $rootScope.updatestatus_context;
    delete $rootScope.terimapermohonan_context;
    delete $rootScope.tolakpermohonan_context;
});




});
