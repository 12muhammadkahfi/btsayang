angular.module('koppas').controller('dpimport',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,$resource,Upload){
    $rootScope.page_title = "Data Import";
    $rootScope.form_title = "Formulir Pengajuan Ijin Pemanfaatan Ruang Milik Jalan | BBPJN6";
    $scope.files = [];
    $scope.files_success = {name:[],size:[],type:[],remark:[]};
    $scope.onUpload = false;
    $scope.progressPercentage = 0;
    $scope.provinsi = [];
    $scope.jenis_provinsi = [];
    $scope.tipe_perijinan = [];
    $scope.ruas_jalan = [];
    $scope.unit_kerja = [];
    $scope.ppk = [];
    $scope.inout = [];
    $http.post(site_url('perijinan/clear_tmps'));
    $http.post(site_url('master_data/tipe'),{}).success(function(data){
        if(data.status){
        $scope.tipe_perijinan = data.data;
    } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Tipe');});
    
    
    $scope.delattach = function(index){
        var yes = $window.confirm("Anda yakin akan menghapus lampiran :\n\n"+$scope.files_success.name[index]);
        if(yes){
            delete $scope.files_success.size.splice(index,1);
            delete $scope.files_success.name.splice(index,1);
            delete $scope.files_success.type.splice(index,1);
            delete $scope.files_success.remark.splice(index,1);
        }
    }
    
    $scope.alert = function(arg){
        alert(arg);
    }
    $scope.submitimport = function(){
                var yes = $window.confirm("Anda yakin akan Mendownload "+$scope.FormData.apv_status+"?");
                var apv_status = $scope.FormData.apv_status;
                if(yes){
                   $window.location('index.php/perijinan/export_excel_tipe/'+$scope.FormData.apv_status);
                }
                    jQuery('#modal-default').modal('hide');
                    alert(apv_status);
                    $route.reload();
    }
    $http.post(site_url('master_data/ruas_jalan'),{}).success(function(data){
    if(data.status){
        $scope.ruas_jalan = data.data;
        $http.post(site_url('perijinan/data_perijinan')).success(function(data){
            if(data.status){
                $scope.data_perijinan = data.data;
                $resource(data.data).query().$promise.then(function(persons) {
                    vm.data_perijinan = persons;
                });
            } else {}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Perijinan');});
    } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Ruas Izij');});
    $scope.uploadFiles = function (files) {
      if (files && files.length) {
        $scope.onUpload = true;
		$timeout(function(){$rootScope.NowLoading = false;},10);
		$timeout(function(){$rootScope.NowLoading = false;},50);
		$timeout(function(){$rootScope.NowLoading = false;},350);
		$timeout(function(){$rootScope.NowLoading = false;},1000);
		Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success.name.length==0){
                $scope.files_success.name = resp.data.file.name;
                $scope.files_success.size = resp.data.file.size;
                $scope.files_success.type = resp.data.file.type;
                angular.forEach($scope.files_success.name, function(value, index){
                    $scope.files_success.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success.name.indexOf(value)<0){
                        $scope.files_success.name.push(value);
                        $scope.files_success.size.push(resp.data.file.size[index]);
                        $scope.files_success.type.push(resp.data.file.type[index]);
                        $scope.files_success.remark.push('');
                    }
                });
            }
            console.log($scope.files_success);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage)==100){
				$timeout(function(){
					$scope.onUpload = false;
				},350);
			}
		});;
      }
    }
     
    $scope.submitimport = function(){
        if($scope.files_success.name.length<1){
            errorToastAuto('Anda harus mengupload setidaknya 1 lampiran terlebih dahulu.');
            return false;
        }
        $scope.FormData.attachments = [];
        angular.forEach($scope.files_success.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                description : $scope.files_success.remark[index]
            });
        });
        
        $http.post(site_url('perijinan/ajukanpermohonan'),$scope.FormData).success(function(data){
        if(data.status){
            successToastAuto(data.message);
            $location.path("data_perijinan/detail/"+data.ID);
        } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.hapusimport = function(ID){
        var confirm1 = $window.confirm("Anda akan menghapus pengajuan dengan nomor "+ID);
        if(confirm1){
            var confirm2 = $window.confirm("Tindakan ini tidak dapat dibatalkan, apakah anda yakin?");
            if(confirm2){
                $http.post(site_url('perijinan/hapus_ijin'),{ID:ID}).success(function(data){
                    if(data.status){
                    successToastAuto(data.message);
                    $route.reload();
                } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
            }    
        }
    }
    delete $rootScope.terimaberkas_context;
    delete $rootScope.updatestatus_context;
    delete $rootScope.terimapermohonan_context;
    delete $rootScope.tolakpermohonan_context;
});


angular.module('koppas').controller('dp_tolak_permohonan',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
    $rootScope.page_title = "Tolak Permohonan";
    var route = $location.path().substring(1).split('/');
    $scope.route = route;
    if(typeof $rootScope.tolakpermohonan_context === "undefined"){
        $location.path("/data_perijinan/detail/"+route[2]);
        return;
    }
    $http.post(site_url('perijinan/clear_tmps'));
    if($rootScope.tolakpermohonan_context.status !== "PROCESSING"&&$rootScope.tolakpermohonan_context.status !== "PENDING"){
        $location.path("/data_perijinan/detail/"+route[2]);
        errorToastAuto('Sudah tidak dapat menolak perijinan '+route[2]);
        return;
    }
    $timeout(function(){
        jQuery('#remark').focus();
    },1000);
    $scope.tolakpermohonan = function() {
        $scope.FormData.ID = route[2];
        $http.post(site_url('perijinan/tolak_permohonan'),$scope.FormData).success(function(data){
            if(data.status){
                successToastAuto(data.message);
                $location.path("/data_perijinan/detail/"+route[2]);
            } else {errorToastAuto(data.message)}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.uploadFiles = function (files) {
        // return console.log(files[0]);
        if (files && files.length) {
        $scope.onUpload = true;
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if(resp.data.file.name.length==1){
                $scope.filename = "tmps/"+$rootScope.mysession.ID+"/"+resp.data.file.name[0];
                if($scope.filename.indexOf('pdf')>=0){
                    $scope.filename = "assets/pdf-flat.png";
                }
                $scope.real_name = resp.data.file.name[0];
                $scope.FormData.filename = resp.data.file.name[0];
            }
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $timeout(function(){
                $scope.onUpload = false;
            },350);
        });;
      }
    }
});