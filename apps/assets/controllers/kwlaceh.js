//inline aceh
angular.module('koppas').controller('kwlaceh',function($scope,$rootScope,$location,$http,$filter,$window,$route,$sce){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.aceh = [];
	$scope.prov_aceh = [];
	$scope.provinsi = [];
	$scope.kota = [];
	$scope.ruas_jalan = [];
	$scope.tipe_pembangunan = [];
	$scope.ppk = [];
	$scope.unit_kerja = [];
	$scope.FormData = {};
	$scope.kab_aceh = [];
	$rootScope.page_title = "MKTREE SOURCE";
	$http.post(site_url('perijinan/clear_tmps'));
	// $http.post(site_url('masterController/aceh'),{}).success(function(data){
 //    console.log(data.data);
 //        if(data.status){
 //            $scope.aceh = data.data;
 //            $timeout(function(){
 //                jQuery(function(){
 //                    var m = new Masonry(jQuery('.grid').get()[0], {
 //                        itemSelector: ".thumbs"
 //                    });
 //                });
 //            },100);
 //        } else {
 //            errorToastAuto(data.message);
 //            $location.path("/master_data/kawil_aceh");
 //        }
 //    }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	$http.post(site_url('masterController/aceh'),{}).success(function(data){
	if(data.status){
		$scope.aceh = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	//inline aceh appv
	$http.post(site_url('masterController/prov_aceh'),{}).success(function(data){
	if(data.status){
		$scope.prov_aceh = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	//endline aceh provinsi
	$http.post(site_url('masterController/kab_aceh'),{}).success(function(data){
	if(data.status){
		$scope.kab_aceh = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	//endline aceh kabupaten
	if($rootScope.route[1]==="kawil_aceh"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Provinsi Aceh";
			$scope.editaceh = function(data){
				$rootScope.context_aceh = data;
				$location.path('master_data/kawil_aceh/edit');
			}
			$scope.kabaceh = function(_data){
				$rootScope.context_aceh = data;
				$http.post(site_url('masterController/var_kab_aceh'),{
						kdDep : _data.kdDep,
						Nama : _data.Nama
						// status : 0
					}).success(function(data){
						if(data.status){
						$scope.kab_aceh = data.data;
						//successToastAuto(data.message);
						$location.path('master_data/kawil_aceh/kabaceh');
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});				
			}
			$scope.hapusprovinsi = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus provinsi aceh lokasi "+_data.Nama+"?");
				if(yes){
					$http.post(site_url('masterController/hapus_aceh'),{
						kdDep : _data.kdDep,
						Nama : _data.Nama
						// status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
	$scope.submitaceh = function(){
			$http.post(site_url('masterController/submit_aceh'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/kawil_aceh")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Entitas";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Provinsi Aceh";
			if(typeof $rootScope.context_aceh === "undefined") $location.path("master_data/aceh");
			$scope.FormData.kdDep = $rootScope.context_aceh.kdDep;
			$scope.FormData.Nama = $rootScope.context_aceh.Nama;

		}
	}
	//endline aceh appv
	//-------//
	//line kabupaten aceh//
	//endline kabupaten aceh//
	if($rootScope.route[1]==="kota"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Kota";
			$scope.editkota = function(data){
				$rootScope.context_kota = data;
				//$location.path('master_data/kota/edit','_blank');
				//$location.path('master_data/kota/edit').replace();
				//$window.open(openNewTab,'_blank');
				 $location.path('master_data/kota/edit');
//				$location.path(b);
				
			}
			$scope.hapuskota = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus kota "+_data.nama_kota+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_kota'),{
						hash_id : _data.hash_id,
						nama_kota : _data.nama_kota,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			$http.post(site_url('master_data/kota'),{}).success(function(data){
				if(data.status){
				$scope.kota = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			return;
		}
		$scope.submitkota = function(){
			$http.post(site_url('master_data/submit_kota'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/kota")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Kota";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Kota";
			if(typeof $rootScope.context_kota === "undefined") $location.path("master_data/kota");
			$scope.FormData.hash_id = $rootScope.context_kota.hash_id;
			$scope.FormData.nama_kota = $rootScope.context_kota.nama_kota;
			$scope.FormData.id_provinsi = $rootScope.context_kota.id_provinsi;
			$scope.FormData.remark = $rootScope.context_kota.remark;
		}
	}
	if($rootScope.route[1]==="ruas_jalan"){
		//$http.post(site_url('master_data/ppk'),{}).success(function(data){
        //	if(data.status){
        //		$scope.ppk = data.data;
    	//	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		$http.post(site_url('master_data/unitkerja'),{}).success(function(data){
        	if(data.status){
        		$scope.unit_kerja = data.data;
    		} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Ruas Jalan";
			
			$scope.editruasjalan = function(data){
				$rootScope.context_ruas_jalan = data;
				$location.path('master_data/ruas_jalan/edit');
			}
			$http.post(site_url('master_data/ruas_jalan'),{}).success(function(data){
				if(data.status){
				$scope.ruas_jalan = data.data;
				$resource(data.data).query().$promise.then(function(persons) {
                    vm.ruas_jalan = persons;
                });
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.hapusruasjalan = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus ruas jalan "+_data.nama_ruas_jalan+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_ruas_jalan'),{
						hash_id : _data.hash_id,
						nama_ruas_jalan : _data.nama_ruas_jalan,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
		$scope.get_ppk = function(){
			$http.post(site_url('master_data/ppk_isi'),{
				id_unit : $scope.FormData.id_unit
			}).success(function(data){
				if(data.status){
				$scope.ppk = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		$scope.get_kota = function(){
			$http.post(site_url('master_data/kota'),{
				id_provinsi : $scope.FormData.id_provinsi
			}).success(function(data){
				if(data.status){
				$scope.kota = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		$scope.submitruasjalan = function(){
			$http.post(site_url('master_data/submit_ruas_jalan'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/ruas_jalan")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Ruas Jalan";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Ruas Jalan";
			if(typeof $rootScope.context_ruas_jalan === "undefined") $location.path("master_data/ruas_jalan");
			$scope.FormData.hash_id = $rootScope.context_ruas_jalan.hash_id;
			$scope.FormData.nama_ruas_jalan = $rootScope.context_ruas_jalan.nama_ruas_jalan;
			$scope.FormData.id_unit = $rootScope.context_ruas_jalan.id_unit;
			$scope.FormData.id_ppk = $rootScope.context_ruas_jalan.id_ppk;
			$scope.get_ppk();
		}
	}
});
angular.module('koppas').controller('kwl_kab_aceh',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
  //  var route = $location.path().substring(1).split('/');
    $rootScope.page_title = "Kabupaten Aceh";
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.kab_aceh = [];
	$scope.data_kab = [];
	$rootScope.page_title = "MKTREE SOURCE";
	$http.post(site_url('perijinan/clear_tmps'));
    $scope.warna = ["#bdf947","#b4f82e","#acf715","#9feb08","#8ed207","#7eba06","#6da105"];
    var route = $location.path().substring(1).split('/');
    $http.post(site_url('perijinan/clear_tmps'));
    $scope.route = route;
	 
    	$scope.editaceh = function(data){
			$rootScope.context_aceh = data;
			$location.path('master_data/kawil_aceh/edit');
		}
	$http.post(site_url('masterController/var_kab_aceh'),{ID:route[2]}).success(function(data){
        console.log(data.data);
        if(data.status){
            $scope.kab_aceh = data.data;
            $scope.data_kab = route[2];
            $timeout(function(){
                jQuery(function(){
                    var m = new Masonry(jQuery('.grid').get()[0], {
                        itemSelector: ".thumbs"
                    });
                });
            },100);
        } else {
            errorToastAuto(data.message);
            $location.path("/master_data/kawil_aceh");
        }
    }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    $scope.submitaceh = function(){
			$http.post(site_url('masterController/submit_aceh'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/kawil_aceh")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
});
angular.module('koppas').controller('kwl_kec_aceh',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
  //  var route = $location.path().substring(1).split('/');
    $rootScope.page_title = "Kecamatan Aceh";
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.kec_aceh = [];
	$scope.hed_aceh = [];
	$rootScope.page_title = "MKTREE SOURCE";
	$http.post(site_url('perijinan/clear_tmps'));
    $scope.warna = ["#bdf947","#b4f82e","#acf715","#9feb08","#8ed207","#7eba06","#6da105"];
    var route = $location.path().substring(1).split('/');
    $http.post(site_url('perijinan/clear_tmps'));
    $scope.route = route;
    $scope.editaceh = function(data){
		$rootScope.context_aceh = data;
		$location.path('master_data/kawil_aceh/edit');
	}
	$scope.hapuskota = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus kota "+_data.nama_kota+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_kota'),{
						hash_id : _data.hash_id,
						nama_kota : _data.nama_kota,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
	}
	$scope.expkecaceh = function(){
                var yes = $window.confirm("Anda yakin akan Mendownload "+route[2]+"?");
                var apv_status = route[2];
                if(yes){
                   $window.location('index.php/report_controller/export_excel_kec/'+route[2]);
                }
//                    jQuery('#modal-default').modal('hide');
                    alert(apv_status);
                    $route.reload();
    }
	$http.post(site_url('masterController/var_kec_aceh'),{ID:route[2]}).success(function(data){
        console.log(data.data);
        if(data.status){
            $scope.kec_aceh = data.data;
            $scope.hed_aceh = route[2];
            $timeout(function(){
                jQuery(function(){
                    var m = new Masonry(jQuery('.grid').get()[0], {
                        itemSelector: ".thumbs"
                    });
                });
            },100);
        } else {
            errorToastAuto(data.message);
            $location.path("/master_data/kawil_aceh");
        }
    }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    $scope.submitaceh = function(){
		$http.post(site_url('masterController/submit_aceh'),$scope.FormData).success(function(data){
			if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/kawil_aceh")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
	
});
angular.module('koppas').controller('kwl_desa_aceh',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
  //  var route = $location.path().substring(1).split('/');
    $rootScope.page_title = "Desa Aceh";
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.desa_aceh = [];
	$rootScope.page_title = "MKTREE SOURCE";
	$http.post(site_url('perijinan/clear_tmps'));
    $scope.warna = ["#bdf947","#b4f82e","#acf715","#9feb08","#8ed207","#7eba06","#6da105"];
    var route = $location.path().substring(1).split('/');
    $http.post(site_url('perijinan/clear_tmps'));
    $scope.route = route;
    $scope.editaceh = function(data){
		$rootScope.context_aceh = data;
		$location.path('master_data/kawil_aceh/edit');
	}
    $http.post(site_url('masterController/var_desa_aceh'),{ID:route[2]}).success(function(data){
        console.log(data.data);
        if(data.status){
            $scope.desa_aceh = data.data;
            $timeout(function(){
                jQuery(function(){
                    var m = new Masonry(jQuery('.grid').get()[0], {
                        itemSelector: ".thumbs"
                    });
                });
            },100);
        } else {
            errorToastAuto(data.message);
            $location.path("/master_data/kawil_aceh");
        }
    }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    $scope.submitaceh = function(){
			$http.post(site_url('masterController/submit_aceh'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/kawil_aceh")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
});
angular.module('koppas').controller('sub_kwl_kec_aceh',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
  //  var route = $location.path().substring(1).split('/');
    $rootScope.page_title = "Kecamatan Aceh";
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.kec_aceh = [];
	$scope.data_kec = [];
	$rootScope.page_title = "MKTREE SOURCE";
	$http.post(site_url('perijinan/clear_tmps'));
    $scope.warna = ["#bdf947","#b4f82e","#acf715","#9feb08","#8ed207","#7eba06","#6da105"];
    var route = $location.path().substring(1).split('/');
    $http.post(site_url('perijinan/clear_tmps'));
    $scope.route = route;
    $scope.editaceh = function(data){
		$rootScope.context_aceh = data;
		$location.path('master_data/kawil_aceh/edit');
	}
    $http.post(site_url('masterController/sub_var_kec_aceh'),{ID:route[2]}).success(function(data){
        console.log(data.data);
        if(data.status){
            $scope.kec_aceh = data.data;
            $scope.data_kec = route[2];
            $timeout(function(){
                jQuery(function(){
                    var m = new Masonry(jQuery('.grid').get()[0], {
                        itemSelector: ".thumbs"
                    });
                });
            },100);
        } else {
            errorToastAuto(data.message);
            $location.path("/master_data/kawil_aceh");
        }
    }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    $scope.submitaceh = function(){
			$http.post(site_url('masterController/submit_aceh'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/kawil_aceh")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
});
angular.module('koppas').controller('sub_kwl_desa_aceh',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
  //  var route = $location.path().substring(1).split('/');
    $rootScope.page_title = "Desa Aceh";
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.desa_aceh = [];
	$scope.data_desa = [];
	$rootScope.page_title = "MKTREE SOURCE";
	$http.post(site_url('perijinan/clear_tmps'));
    $scope.warna = ["#bdf947","#b4f82e","#acf715","#9feb08","#8ed207","#7eba06","#6da105"];
    var route = $location.path().substring(1).split('/');
    $http.post(site_url('perijinan/clear_tmps'));
    $scope.route = route;
    $scope.editaceh = function(data){
		$rootScope.context_aceh = data;
		$location.path('master_data/kawil_aceh/edit');
	}
    $http.post(site_url('masterController/sub_var_desa_aceh'),{ID:route[2]}).success(function(data){
        console.log(data.data);
        if(data.status){
            $scope.desa_aceh = data.data;
            $scope.data_desa = route[2];
            $timeout(function(){
                jQuery(function(){
                    var m = new Masonry(jQuery('.grid').get()[0], {
                        itemSelector: ".thumbs"
                    });
                });
            },100);
        } else {
            errorToastAuto(data.message);
            $location.path("/master_data/kawil_aceh");
        }
    }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    $scope.submitaceh = function(){
			$http.post(site_url('masterController/submit_aceh'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/kawil_aceh")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
});
