angular.module('koppas').controller('dp_key',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.key = [];
	$scope.kota = [];
	$scope.ruas_jalan = [];
	$scope.tipe_pembangunan = [];
	$scope.FormData = {};
	$scope.unitkerja = [];
	$http.post(site_url('master_data/dp_key'),{}).success(function(data){
	if(data.status){
		$scope.key = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	if($rootScope.route[1]==="key"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Unit Kerja";
			
		
			return;
		}
		$scope.submitkey = function(){
			$http.post(site_url('master_data/submit_key'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/key")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Konf. Key";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Key";
			if(typeof $rootScope.context === "undefined") $location.path("master_data/unitkerja");
			$scope.FormData.hash_id = $rootScope.context_unitkerja.hash_id;
			$scope.FormData.nama_unit = $rootScope.context_unitkerja.nama_unit;
			$scope.FormData.id_provinsi = $rootScope.context_unitkerja.id_provinsi;
		}
	}
	if($rootScope.route[1]==="ppk"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data PPK";
			$http.post(site_url('master_data/unitkerja'),{}).success(function(data){
		if(data.status){
			$scope.unitkerja = data.data;
		} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.editppk = function(data){
				$rootScope.context_ppk = data;
				$location.path('master_data/ppk/edit');
			}
			$scope.hapusppk = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus PPK "+_data.nama_ppk+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_ppk'),{
						hash_id : _data.hash_id,
						nama_ppk : _data.nama_ppk,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			$http.post(site_url('master_data/ppk'),{}).success(function(data){
				if(data.status){
				$scope.ppk = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			return;
		}
		$scope.submitppk = function(){
			$http.post(site_url('master_data/submit_ppk'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/ppk")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah PPK";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit PPK";
			if(typeof $rootScope.context_ppk === "undefined") $location.path("master_data/ppk");
			$scope.FormData.hash_id = $rootScope.context_ppk.hash_id;
			$scope.FormData.nama_ppk = $rootScope.context_ppk.nama_ppk;
			$scope.FormData.id_unit = $rootScope.context_ppk.id_unit;
			//$scope.FormData.remark = $rootScope.context_ppk.remark;
		}
	}
	if($rootScope.route[1]==="ruas_jalan"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Ruas Jalan";
			$scope.editruasjalan = function(data){
				$rootScope.context_ruas_jalan = data;
				$location.path('master_data/ruas_jalan/edit');
			}
			$http.post(site_url('master_data/ruas_jalan'),{}).success(function(data){
				if(data.status){
				$scope.ruas_jalan = data.data;
				$resource(data.data).query().$promise.then(function(persons) {
                    vm.ruas_jalan = persons;
                });
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.hapusruasjalan = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus ruas jalan "+_data.nama_ruas_jalan+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_ruas_jalan'),{
						hash_id : _data.hash_id,
						nama_ruas_jalan : _data.nama_ruas_jalan,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
		$scope.get_kota = function(){
			$http.post(site_url('master_data/kota'),{
				id_provinsi : $scope.FormData.id_provinsi
			}).success(function(data){
				if(data.status){
				$scope.kota = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		$scope.submitruasjalan = function(){
			$http.post(site_url('master_data/submit_ruas_jalan'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/ruas_jalan")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Ruas Jalan";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Ruas Jalan";
			if(typeof $rootScope.context_ruas_jalan === "undefined") $location.path("master_data/ruas_jalan");
			$scope.FormData.hash_id = $rootScope.context_ruas_jalan.hash_id;
			$scope.FormData.nama_ruas_jalan = $rootScope.context_ruas_jalan.nama_ruas_jalan;
			$scope.FormData.id_provinsi = $rootScope.context_ruas_jalan.id_provinsi;
			$scope.FormData.id_kota = $rootScope.context_ruas_jalan.id_kota;
			$scope.FormData.panjang = $rootScope.context_ruas_jalan.panjang;
			$scope.FormData.remark = $rootScope.context_ruas_jalan.remark;
			$scope.get_kota();
		}
	}

	if($rootScope.route[1]==="tipe_pembangunan"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Tipe Pembangunan";
			$scope.edittipe = function(data){
				$rootScope.context_tipe_pembangunan = data;
				$location.path('master_data/tipe_pembangunan/edit');
			}
			$http.post(site_url('master_data/tipe_search'),{}).success(function(data){
				if(data.status){
				$scope.tipe = data.data;
				$resource(data.data).query().$promise.then(function(persons) {
                    vm.tipe = persons;
                });
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.hapustipe = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus tipe pembangunan "+_data.nama_tipe+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_tipe'),{
						id_tipe : _data.id_tipe,
						nama_tipe : _data.nama_tipe,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
		$scope.get_kota = function(){
			$http.post(site_url('master_data/kota'),{
				id_provinsi : $scope.FormData.id_provinsi
			}).success(function(data){
				if(data.status){
				$scope.kota = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		$scope.submittipe = function(){
			$http.post(site_url('master_data/submit_tipe'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/tipe_pembangunan")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Tipe Pembangunan";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Tipe Pembangunan";
			if(typeof $rootScope.context_tipe_pembangunan === "undefined") $location.path("master_data/tipe_pembangunan");
			$scope.FormData.id_tipe = $rootScope.context_tipe_pembangunan.id_tipe;
			$scope.FormData.nama_tipe = $rootScope.context_tipe_pembangunan.nama_tipe;
			//$scope.get_kota();
		}
	}

});