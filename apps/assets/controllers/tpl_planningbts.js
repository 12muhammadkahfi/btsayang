angular.module('koppas').controller('tpl_planningbts',function($scope,$rootScope,$location,$http,$filter,$window,$route,$sce){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.planbts = [];
	$scope.FormData = {};
	$http.post(site_url('tpl_controller/planning_bts'),{}).success(function(data){
	if(data.status){
		$scope.planbts = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	
});
angular.module('koppas').controller('tpl_planningbts_dagri',function($scope,$rootScope,$location,$http,$filter,$window,$route,$sce){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.planbts_dagri = [];
	$scope.FormData = {};
	$http.post(site_url('tpl_controller/planning_bts_kemendagri'),{}).success(function(data){
	if(data.status){
		$scope.planbts_dagri = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	
});