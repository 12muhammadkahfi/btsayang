angular.module('koppas').controller('dpx',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,$resource,Upload){
    $rootScope.page_title = "Data Perijinan";
    $rootScope.form_title = "Formulir Pengajuan Ijin Pemanfaatan Ruang Milik Jalan | BBPJN6";
    $scope.files = [];
    $scope.files2 = [];
    $scope.files3 = [];
    $scope.files4 = [];
    $scope.files5 = [];
    $scope.files6 = [];
    $scope.files_success = {name:[],size:[],type:[],remark:[]};
    $scope.files_success2 = {name:[],size:[],type:[],remark:[]};
    $scope.files_success3 = {name:[],size:[],type:[],remark:[]};
    $scope.files_success4 = {name:[],size:[],type:[],remark:[]};
    $scope.files_success5 = {name:[],size:[],type:[],remark:[]};
    $scope.files_success6 = {name:[],size:[],type:[],remark:[]};
    $scope.onUpload = false;
    $scope.onUpload2 = false;
    $scope.onUpload3 = false;
    $scope.onUpload4 = false;
    $scope.onUpload5 = false;
    $scope.onUpload6 = false;
    $scope.progressPercentage = 0;
    $scope.progressPercentage2 = 0;
    $scope.progressPercentage3 = 0;
    $scope.progressPercentage4 = 0;
    $scope.progressPercentage5 = 0;
    $scope.progressPercentage6 = 0;
    $scope.provinsi = [];
    $scope.jenis_provinsi = [];
    $scope.tipe_perijinan = [];
    $scope.ruas_jalan = [];
    $scope.unit_kerja = [];
    $scope.ppk = [];
    $scope.inout = [];
    $http.post(site_url('perijinan/clear_tmps'));
    $http.post(site_url('master_data/tipe'),{}).success(function(data){
        if(data.status){
        $scope.tipe_perijinan = data.data;
    } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Tipe');});
    $http.post(site_url('master_data/provinsi'),{}).success(function(data){
    if(data.status){
        $scope.provinsi = data.data;
    } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Provinsi');});
    $http.post(site_url('perijinan/groupperijinan'),{}).success(function(data){
    if(data.status){
        $scope.inout = data.data;
    } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Perijinan');});
    
    
    $scope.delattach = function(index){
        var yes = $window.confirm("Anda yakin akan menghapus lampiran :\n\n"+$scope.files_success.name[index]);
        if(yes){
            delete $scope.files_success.size.splice(index,1);
            delete $scope.files_success.name.splice(index,1);
            delete $scope.files_success.type.splice(index,1);
            delete $scope.files_success.remark.splice(index,1);
        }
    }
    $scope.delattach2 = function(index){
        var yes = $window.confirm("Anda yakin akan menghapus lampiran :\n\n"+$scope.files_success2.name[index]);
        if(yes){
            delete $scope.files_success2.size.splice(index,1);
            delete $scope.files_success2.name.splice(index,1);
            delete $scope.files_success2.type.splice(index,1);
            delete $scope.files_success2.remark.splice(index,1);
        }
    }
    $scope.delattach3 = function(index){
        var yes = $window.confirm("Anda yakin akan menghapus lampiran :\n\n"+$scope.files_success3.name[index]);
        if(yes){
            delete $scope.files_success3.size.splice(index,1);
            delete $scope.files_success3.name.splice(index,1);
            delete $scope.files_success3.type.splice(index,1);
            delete $scope.files_success3.remark.splice(index,1);
        }
    }
    
    $scope.get_unit_kerja = function(){
            $http.post(site_url('master_data/unit_kerja_isi'),{
                id_provinsi : $scope.FormData.provinsi
            }).success(function(data){
                if(data.status){
                $scope.unit_kerja = data.data;
            } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Unit');});
    }
    $scope.get_ppk = function(){
            $http.post(site_url('master_data/ppk_isi'),{
                id_unit : $scope.FormData.unit_kerja
            }).success(function(data){
                if(data.status){
                $scope.ppk = data.data;
            } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection PPK');});
    }
    $scope.get_ruas = function(){
            $http.post(site_url('master_data/ruas_jalan_provinsi_ppk'),{
                id_ppk : $scope.FormData.ppk
            }).success(function(data){
                if(data.status){
                $scope.ruas_jalan = data.data;
            } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Ruas');});
    }
    $scope.alert = function(arg){
        alert(arg);
    }
    $scope.get_detailp = function(arg){
            $http.post(site_url('perijinan/data_perijinan_group'),{
                apv_status : arg
            }).success(function(data){
                if(data.status){
                $scope.data_perijinan = data.data;
                $resource(data.data).query().$promise.then(function(persons) {
                vm.data_perijinan = persons;
                 });
            } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Ruas');});
    }
    $scope.submitlaporan = function(){
                var yes = $window.confirm("Anda yakin akan Mendownload "+$scope.FormData.apv_status+"?");
                var apv_status = $scope.FormData.apv_status;
                if(yes){
                   $window.location('index.php/perijinan/export_excel_tipe/'+$scope.FormData.apv_status);
                }
                    jQuery('#modal-default').modal('hide');
                    alert(apv_status);
                    $route.reload();
    }
    $http.post(site_url('master_data/ruas_jalan'),{}).success(function(data){
    if(data.status){
        $scope.ruas_jalan = data.data;
        $http.post(site_url('perijinan/data_perijinan')).success(function(data){
            if(data.status){
                $scope.data_perijinan = data.data;
                $resource(data.data).query().$promise.then(function(persons) {
                    vm.data_perijinan = persons;
                });
            } else {}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Perijinan');});
    } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Ruas Izij');});
    $scope.uploadFiles = function (files) {
      if (files && files.length) {
        $scope.onUpload = true;
		$timeout(function(){$rootScope.NowLoading = false;},10);
		$timeout(function(){$rootScope.NowLoading = false;},50);
		$timeout(function(){$rootScope.NowLoading = false;},350);
		$timeout(function(){$rootScope.NowLoading = false;},1000);
		Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success.name.length==0){
                $scope.files_success.name = resp.data.file.name;
                $scope.files_success.size = resp.data.file.size;
                $scope.files_success.type = resp.data.file.type;
                angular.forEach($scope.files_success.name, function(value, index){
                    $scope.files_success.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success.name.indexOf(value)<0){
                        $scope.files_success.name.push(value);
                        $scope.files_success.size.push(resp.data.file.size[index]);
                        $scope.files_success.type.push(resp.data.file.type[index]);
                        $scope.files_success.remark.push('');
                    }
                });
            }
            console.log($scope.files_success);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage)==100){
				$timeout(function(){
					$scope.onUpload = false;
				},350);
			}
		});;
      }
    }
    $scope.uploadFiles2 = function (files) {
      if (files && files.length) {
        $scope.onUpload2 = true;
        $timeout(function(){$rootScope.NowLoading = false;},10);
        $timeout(function(){$rootScope.NowLoading = false;},50);
        $timeout(function(){$rootScope.NowLoading = false;},350);
        $timeout(function(){$rootScope.NowLoading = false;},1000);
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success2.name.length==0){
                $scope.files_success2.name = resp.data.file.name;
                $scope.files_success2.size = resp.data.file.size;
                $scope.files_success2.type = resp.data.file.type;
                angular.forEach($scope.files_success2.name, function(value, index){
                    $scope.files_success2.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success2.name.indexOf(value)<0){
                        $scope.files_success2.name.push(value);
                        $scope.files_success2.size.push(resp.data.file.size[index]);
                        $scope.files_success2.type.push(resp.data.file.type[index]);
                        $scope.files_success2.remark.push('');
                    }
                });
            }
            console.log($scope.files_success2);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage2 = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage2)==100){
                $timeout(function(){
                    $scope.onUpload2 = false;
                },350);
            }
        });;
      }
    }
    $scope.uploadFiles3 = function (files) {
      if (files && files.length) {
        $scope.onUpload3 = true;
        $timeout(function(){$rootScope.NowLoading = false;},10);
        $timeout(function(){$rootScope.NowLoading = false;},50);
        $timeout(function(){$rootScope.NowLoading = false;},350);
        $timeout(function(){$rootScope.NowLoading = false;},1000);
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success3.name.length==0){
                $scope.files_success3.name = resp.data.file.name;
                $scope.files_success3.size = resp.data.file.size;
                $scope.files_success3.type = resp.data.file.type;
                angular.forEach($scope.files_success3.name, function(value, index){
                    $scope.files_success3.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success3.name.indexOf(value)<0){
                        $scope.files_success3.name.push(value);
                        $scope.files_success3.size.push(resp.data.file.size[index]);
                        $scope.files_success3.type.push(resp.data.file.type[index]);
                        $scope.files_success3.remark.push('');
                    }
                });
            }
            console.log($scope.files_success3);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage3 = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage3)==100){
                $timeout(function(){
                    $scope.onUpload3 = false;
                },350);
            }
        });;
      }
    }
     $scope.uploadFiles4 = function (files) {
      if (files && files.length) {
        $scope.onUpload4 = true;
        $timeout(function(){$rootScope.NowLoading = false;},10);
        $timeout(function(){$rootScope.NowLoading = false;},50);
        $timeout(function(){$rootScope.NowLoading = false;},450);
        $timeout(function(){$rootScope.NowLoading = false;},1000);
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success4.name.length==0){
                $scope.files_success4.name = resp.data.file.name;
                $scope.files_success4.size = resp.data.file.size;
                $scope.files_success4.type = resp.data.file.type;
                angular.forEach($scope.files_success4.name, function(value, index){
                    $scope.files_success4.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success4.name.indexOf(value)<0){
                        $scope.files_success4.name.push(value);
                        $scope.files_success4.size.push(resp.data.file.size[index]);
                        $scope.files_success4.type.push(resp.data.file.type[index]);
                        $scope.files_success4.remark.push('');
                    }
                });
            }
            console.log($scope.files_success4);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage4 = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage4)==100){
                $timeout(function(){
                    $scope.onUpload4 = false;
                },450);
            }
        });;
      }
    }
     $scope.uploadFiles5 = function (files) {
      if (files && files.length) {
        $scope.onUpload5 = true;
        $timeout(function(){$rootScope.NowLoading = false;},10);
        $timeout(function(){$rootScope.NowLoading = false;},50);
        $timeout(function(){$rootScope.NowLoading = false;},550);
        $timeout(function(){$rootScope.NowLoading = false;},1000);
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success5.name.length==0){
                $scope.files_success5.name = resp.data.file.name;
                $scope.files_success5.size = resp.data.file.size;
                $scope.files_success5.type = resp.data.file.type;
                angular.forEach($scope.files_success5.name, function(value, index){
                    $scope.files_success5.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success5.name.indexOf(value)<0){
                        $scope.files_success5.name.push(value);
                        $scope.files_success5.size.push(resp.data.file.size[index]);
                        $scope.files_success5.type.push(resp.data.file.type[index]);
                        $scope.files_success5.remark.push('');
                    }
                });
            }
            console.log($scope.files_success5);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage5 = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage5)==100){
                $timeout(function(){
                    $scope.onUpload5 = false;
                },550);
            }
        });;
      }
    }
     $scope.uploadFiles6 = function (files) {
      if (files && files.length) {
        $scope.onUpload6 = true;
        $timeout(function(){$rootScope.NowLoading = false;},10);
        $timeout(function(){$rootScope.NowLoading = false;},60);
        $timeout(function(){$rootScope.NowLoading = false;},660);
        $timeout(function(){$rootScope.NowLoading = false;},1000);
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success6.name.length==0){
                $scope.files_success6.name = resp.data.file.name;
                $scope.files_success6.size = resp.data.file.size;
                $scope.files_success6.type = resp.data.file.type;
                angular.forEach($scope.files_success6.name, function(value, index){
                    $scope.files_success6.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success6.name.indexOf(value)<0){
                        $scope.files_success6.name.push(value);
                        $scope.files_success6.size.push(resp.data.file.size[index]);
                        $scope.files_success6.type.push(resp.data.file.type[index]);
                        $scope.files_success6.remark.push('');
                    }
                });
            }
            console.log($scope.files_success6);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage6 = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage6)==100){
                $timeout(function(){
                    $scope.onUpload6 = false;
                },660);
            }
        });;
      }
    }
    $scope.submitpermohonan = function(){
        if($scope.files_success.name.length<1){
            errorToastAuto('Anda harus mengupload setidaknya 1 lampiran terlebih dahulu.');
            return false;
        }
        $scope.FormData.attachments = [];
        $scope.FormData.attachments2 = [];
        $scope.FormData.attachments3 = [];
        angular.forEach($scope.files_success.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                description : $scope.files_success.remark[index]
            });
        });
        angular.forEach($scope.files_success2.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                description : $scope.files_success2.remark[index]
            });
        });
        angular.forEach($scope.files_success3.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                description : $scope.files_success3.remark[index]
            });
        });
        angular.forEach($scope.files_success4.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                description : $scope.files_success4.remark[index]
            });
        });
        angular.forEach($scope.files_success5.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                description : $scope.files_success5.remark[index]
            });
        });
        angular.forEach($scope.files_success6.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                description : $scope.files_success6.remark[index]
            });
        });
        
        $http.post(site_url('perijinan/ajukanpermohonan'),$scope.FormData).success(function(data){
        if(data.status){
            successToastAuto(data.message);
            $location.path("data_perijinan/detail/"+data.ID);
        } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.hapusijin = function(ID){
        var confirm1 = $window.confirm("Anda akan menghapus pengajuan dengan nomor "+ID);
        if(confirm1){
            var confirm2 = $window.confirm("Tindakan ini tidak dapat dibatalkan, apakah anda yakin?");
            if(confirm2){
                $http.post(site_url('perijinan/hapus_ijin'),{ID:ID}).success(function(data){
                    if(data.status){
                    successToastAuto(data.message);
                    $route.reload();
                } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
            }    
        }
    }
    delete $rootScope.terimaberkas_context;
    delete $rootScope.updatestatus_context;
    delete $rootScope.terimapermohonan_context;
    delete $rootScope.tolakpermohonan_context;
});

angular.module('koppas').controller('dp_detail',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
    $rootScope.page_title = "Detail Pengajuan";
    $scope.warna = ["#bdf947","#b4f82e","#acf715","#9feb08","#8ed207","#7eba06","#6da105"];
    var route = $location.path().substring(1).split('/');
    $http.post(site_url('perijinan/clear_tmps'));
    $scope.route = route;
    $http.post(site_url('perijinan/detail'),{ID:route[2]}).success(function(data){
        console.log(data.data);
        if(data.status){
            $scope.data = data.data;
            $timeout(function(){
                jQuery(function(){
                    var m = new Masonry(jQuery('.grid').get()[0], {
                        itemSelector: ".thumbs"
                    });
                });
            },100);
        } else {
            errorToastAuto(data.message);
            $location.path("/data_perijinan");
        }
    }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    $scope.terimaberkas = function(data){
        $rootScope.terimaberkas_context = $scope.data.detail;
        $location.path("/data_perijinan/terima_berkas/"+route[2]);
    }
    $scope.updatestatus = function(data){
        $rootScope.updatestatus_context = $scope.data.detail;
        $rootScope.updatestatus_context_tracking = $scope.data.tracking;
        $location.path("/data_perijinan/update_status/"+route[2]);
    }
    $scope.updatestatusekspose = function(data){
        $rootScope.updatestatus_context = $scope.data.detail;
        $rootScope.updatestatus_context_tracking = $scope.data.tracking;
        $location.path("/data_perijinan/update_status_ekspose/"+route[2]);
    }
    $scope.updatestatussurvei = function(data){
        $rootScope.updatestatus_context = $scope.data.detail;
        $rootScope.updatestatus_context_tracking = $scope.data.tracking;
        $location.path("/data_perijinan/update_status_survei/"+route[2]);
    }
    $scope.updatestatusrapat = function(data){
        $rootScope.updatestatus_context = $scope.data.detail;
        $rootScope.updatestatus_context_tracking = $scope.data.tracking;
        $location.path("/data_perijinan/update_status_hasil_rapat/"+route[2]);
    }
    $scope.updatestatusrekomtek = function(data){
        $rootScope.updatestatus_context = $scope.data.detail;
        $rootScope.updatestatus_context_tracking = $scope.data.tracking;
        $location.path("/data_perijinan/update_status_rekomtek/"+route[2]);
    }
    $scope.terimapermohonan = function(data){
        $rootScope.terimapermohonan_context = $scope.data.detail;
        $location.path("/data_perijinan/terima_permohonan/"+route[2]);
    }
    $scope.tolakpermohonan = function(data){
        $rootScope.tolakpermohonan_context = $scope.data.detail;
        $location.path("/data_perijinan/tolak_permohonan/"+route[2]);
    }
    $scope.doprint = function(){
        window.location = 'index.php/print/'+route[2];
    }
    delete $rootScope.terimaberkas_context;
    delete $rootScope.updatestatus_context;
    delete $rootScope.terimapermohonan_context;
    delete $rootScope.tolakpermohonan_context;
    delete $rootScope.updatestatusekspose_context;
    delete $rootScope.updatestatussurvei_context;
    delete $rootScope.updatestatusrapat_context;
    delete $rootScope.updatestatusrekomtek_context;
});

angular.module('koppas').controller('dp_terima_berkas',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
    $rootScope.page_title = "Terima Berkas (Hardcopy)";
    var route = $location.path().substring(1).split('/');
    $scope.route = route;
    $http.post(site_url('perijinan/clear_tmps'));
    if(typeof $rootScope.terimaberkas_context === "undefined"){
        $location.path("/data_perijinan/detail/"+route[2]);
        return;
    }
    if($rootScope.terimaberkas_context.status !== "PENDING"){
        $location.path("/data_perijinan/detail/"+route[2]);
        errorToastAuto('Berkas untuk nomor pengajuan '+route[2]+' sudah diterima');
        return;
    }
    $timeout(function(){
        jQuery('#pengantar_berkas').focus();
    },1000);

    $scope.submitekspose = function() {
        $scope.FormData.ID = route[2];
        $http.post(site_url('perijinan/submit_berkas_ekspose'),$scope.FormData).success(function(data){
            if(data.status){
                successToastAuto(data.message);
                $location.path("/data_perijinan/detail/"+route[2]);
            } else {errorToastAuto(data.message)}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }

    $scope.uploadFiles = function (files) {
        // return console.log(files[0]);
        if (files && files.length) {
        $scope.onUpload = true;
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if(resp.data.file.name.length==1){
                $scope.filename = "tmps/"+$rootScope.mysession.ID+"/"+resp.data.file.name[0];
                if($scope.filename.indexOf('pdf')>=0){
                    $scope.filename = "assets/pdf-flat.png";
                }
                $scope.real_name = resp.data.file.name[0];
                $scope.FormData.filename = resp.data.file.name[0];
            }
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $timeout(function(){
                $scope.onUpload = false;
            },350);
        });;
      }
    }

});

angular.module('koppas').controller('dp_update_status',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
    $rootScope.page_title = "Terima Berkas (Hardcopy)";
    var route = $location.path().substring(1).split('/');
    $scope.route = route;
    $http.post(site_url('perijinan/clear_tmps'));
    if(typeof $rootScope.updatestatus_context === "undefined"){
        $location.path("/data_perijinan/detail/"+route[2]);
        return;
    }
    if($rootScope.updatestatus_context.status !== "PROCESSING"){
        $location.path("/data_perijinan/detail/"+route[2]);
        errorToastAuto('Sudah tidak dapat mengupdate status nomor perijinan '+route[2]);
        return;
    }
    //$http.post(site_url('perijinan/clear_tmps'));
    //$scope.route = route;
    $http.post(site_url('perijinan/detail'),{ID:route[2]}).success(function(data){
        console.log(data.data);
        if(data.status){
            $scope.data = data.data;
            $timeout(function(){
                jQuery(function(){
                    var m = new Masonry(jQuery('.grid').get()[0], {
                        itemSelector: ".thumbs"
                    });
                });
            },100);
        } else {
            errorToastAuto(data.message);
            $location.path("/data_perijinan");
        }
    }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    $timeout(function(){
        jQuery('#remark').focus();
    },1000);
    $scope.submitstatussurvei = function() {
        $scope.FormData.ID = route[2];
        $http.post(site_url('perijinan/submit_status_survei'),$scope.FormData).success(function(data){
            if(data.status){
                successToastAuto(data.message);
                $location.path("/data_perijinan/detail/"+route[2]);
            } else {errorToastAuto(data.message)}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.submitstatusrapat = function() {
        $scope.FormData.ID = route[2];
        $http.post(site_url('perijinan/submit_status_rapat'),$scope.FormData).success(function(data){
            if(data.status){
                successToastAuto(data.message);
                $location.path("/data_perijinan/detail/"+route[2]);
            } else {errorToastAuto(data.message)}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.submitstatusperijinan = function() {
        $scope.FormData.ID = route[2];
        $http.post(site_url('perijinan/submit_status_perijinan'),$scope.FormData).success(function(data){
            if(data.status){
                successToastAuto(data.message);
                $location.path("/data_perijinan/detail/"+route[2]);
            } else {errorToastAuto(data.message)}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.uploadFiles = function (files) {
        // return console.log(files[0]);
        if (files && files.length) {
        $scope.onUpload = true;
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if(resp.data.file.name.length==1){
                $scope.filename = "tmps/"+$rootScope.mysession.ID+"/"+resp.data.file.name[0];
                if($scope.filename.indexOf('pdf')>=0){
                    $scope.filename = "assets/pdf-flat.png";
                }
                $scope.real_name = resp.data.file.name[0];
                $scope.FormData.filename = resp.data.file.name[0];
            }
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $timeout(function(){
                $scope.onUpload = false;
            },350);
        });;
      }
    }
});

angular.module('koppas').controller('dp_terima_permohonan',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
    $rootScope.page_title = "Terima Permohonan";
    var route = $location.path().substring(1).split('/');
    $scope.route = route;
    $http.post(site_url('perijinan/clear_tmps'));
    if(typeof $rootScope.terimapermohonan_context === "undefined"){
        $location.path("/data_perijinan/detail/"+route[2]);
        return;
    }
    if($rootScope.terimapermohonan_context.status !== "PROCESSING"){
        $location.path("/data_perijinan/detail/"+route[2]);
        errorToastAuto('Sudah tidak dapat menerima perijinan '+route[2]);
        return;
    }
    $timeout(function(){
        jQuery('#remark').focus();
    },1000);
    $scope.terimapermohonan = function() {
        $scope.FormData.ID = route[2];
        $http.post(site_url('perijinan/terima_permohonan'),$scope.FormData).success(function(data){
            if(data.status){
                successToastAuto(data.message);
                $location.path("/data_perijinan/detail/"+route[2]);
            } else {errorToastAuto(data.message)}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.uploadFiles = function (files) {
        // return console.log(files[0]);
        if (files && files.length) {
        $scope.onUpload = true;
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if(resp.data.file.name.length==1){
                $scope.filename = "tmps/"+$rootScope.mysession.ID+"/"+resp.data.file.name[0];
                if($scope.filename.indexOf('pdf')>=0){
                    $scope.filename = "assets/pdf-flat.png";
                }
                $scope.real_name = resp.data.file.name[0];
                $scope.FormData.filename = resp.data.file.name[0];
            }
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $timeout(function(){
                $scope.onUpload = false;
            },350);
        });;
      }
    }
});

angular.module('koppas').controller('dp_tolak_permohonan',function($scope,$rootScope,$location,$http,$filter,$window,$route,$timeout,$interval,$sce,Upload){
    $rootScope.page_title = "Tolak Permohonan";
    var route = $location.path().substring(1).split('/');
    $scope.route = route;
    if(typeof $rootScope.tolakpermohonan_context === "undefined"){
        $location.path("/data_perijinan/detail/"+route[2]);
        return;
    }
    $http.post(site_url('perijinan/clear_tmps'));
    if($rootScope.tolakpermohonan_context.status !== "PROCESSING"&&$rootScope.tolakpermohonan_context.status !== "PENDING"){
        $location.path("/data_perijinan/detail/"+route[2]);
        errorToastAuto('Sudah tidak dapat menolak perijinan '+route[2]);
        return;
    }
    $timeout(function(){
        jQuery('#remark').focus();
    },1000);
    $scope.tolakpermohonan = function() {
        $scope.FormData.ID = route[2];
        $http.post(site_url('perijinan/tolak_permohonan'),$scope.FormData).success(function(data){
            if(data.status){
                successToastAuto(data.message);
                $location.path("/data_perijinan/detail/"+route[2]);
            } else {errorToastAuto(data.message)}
        }).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
    }
    $scope.uploadFiles = function (files) {
        // return console.log(files[0]);
        if (files && files.length) {
        $scope.onUpload = true;
        Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if(resp.data.file.name.length==1){
                $scope.filename = "tmps/"+$rootScope.mysession.ID+"/"+resp.data.file.name[0];
                if($scope.filename.indexOf('pdf')>=0){
                    $scope.filename = "assets/pdf-flat.png";
                }
                $scope.real_name = resp.data.file.name[0];
                $scope.FormData.filename = resp.data.file.name[0];
            }
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $timeout(function(){
                $scope.onUpload = false;
            },350);
        });;
      }
    }
});