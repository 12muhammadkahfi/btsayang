angular.module('koppas').controller('md',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.provinsi = [];
	$scope.kota = [];
	$scope.ruas_jalan = [];
	$scope.tipe_pembangunan = [];
	$scope.ppk = [];
	$scope.unit_kerja = [];
	$scope.FormData = {};
	$http.post(site_url('master_data/provinsi'),{}).success(function(data){
	if(data.status){
		$scope.provinsi = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	if($rootScope.route[1]==="provinsi"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Provinsi";
			$scope.editprovinsi = function(data){
				$rootScope.context_provinsi = data;
				$location.path('master_data/provinsi/edit');
			}
			$scope.hapusprovinsi = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus provinsi "+_data.nama_provinsi+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_provinsi'),{
						hash_id : _data.hash_id,
						nama_provinsi : _data.nama_provinsi,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
		$scope.submitprovinsi = function(){
			$http.post(site_url('master_data/submit_provinsi'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/provinsi")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Provinsi";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Provinsi";
			if(typeof $rootScope.context_provinsi === "undefined") $location.path("master_data/provinsi");
			$scope.FormData.hash_id = $rootScope.context_provinsi.hash_id;
			$scope.FormData.nama_provinsi = $rootScope.context_provinsi.nama_provinsi;
			$scope.FormData.remark = $rootScope.context_provinsi.remark;
		}
	}
	if($rootScope.route[1]==="kota"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Kota";
			$scope.editkota = function(data){
				$rootScope.context_kota = data;
				//$location.path('master_data/kota/edit','_blank');
				//$location.path('master_data/kota/edit').replace();
				//$window.open(openNewTab,'_blank');
				 $location.path('master_data/kota/edit');
//				$location.path(b);
				
			}
			$scope.hapuskota = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus kota "+_data.nama_kota+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_kota'),{
						hash_id : _data.hash_id,
						nama_kota : _data.nama_kota,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			$http.post(site_url('master_data/kota'),{}).success(function(data){
				if(data.status){
				$scope.kota = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			return;
		}
		$scope.submitkota = function(){
			$http.post(site_url('master_data/submit_kota'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/kota")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Kota";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Kota";
			if(typeof $rootScope.context_kota === "undefined") $location.path("master_data/kota");
			$scope.FormData.hash_id = $rootScope.context_kota.hash_id;
			$scope.FormData.nama_kota = $rootScope.context_kota.nama_kota;
			$scope.FormData.id_provinsi = $rootScope.context_kota.id_provinsi;
			$scope.FormData.remark = $rootScope.context_kota.remark;
		}
	}
	if($rootScope.route[1]==="ruas_jalan"){
		//$http.post(site_url('master_data/ppk'),{}).success(function(data){
        //	if(data.status){
        //		$scope.ppk = data.data;
    	//	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		$http.post(site_url('master_data/unitkerja'),{}).success(function(data){
        	if(data.status){
        		$scope.unit_kerja = data.data;
    		} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Ruas Jalan";
			
			$scope.editruasjalan = function(data){
				$rootScope.context_ruas_jalan = data;
				$location.path('master_data/ruas_jalan/edit');
			}
			$http.post(site_url('master_data/ruas_jalan'),{}).success(function(data){
				if(data.status){
				$scope.ruas_jalan = data.data;
				$resource(data.data).query().$promise.then(function(persons) {
                    vm.ruas_jalan = persons;
                });
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.hapusruasjalan = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus ruas jalan "+_data.nama_ruas_jalan+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_ruas_jalan'),{
						hash_id : _data.hash_id,
						nama_ruas_jalan : _data.nama_ruas_jalan,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
		$scope.get_ppk = function(){
			$http.post(site_url('master_data/ppk_isi'),{
				id_unit : $scope.FormData.id_unit
			}).success(function(data){
				if(data.status){
				$scope.ppk = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		$scope.get_kota = function(){
			$http.post(site_url('master_data/kota'),{
				id_provinsi : $scope.FormData.id_provinsi
			}).success(function(data){
				if(data.status){
				$scope.kota = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		$scope.submitruasjalan = function(){
			$http.post(site_url('master_data/submit_ruas_jalan'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/ruas_jalan")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Ruas Jalan";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Ruas Jalan";
			if(typeof $rootScope.context_ruas_jalan === "undefined") $location.path("master_data/ruas_jalan");
			$scope.FormData.hash_id = $rootScope.context_ruas_jalan.hash_id;
			$scope.FormData.nama_ruas_jalan = $rootScope.context_ruas_jalan.nama_ruas_jalan;
			$scope.FormData.id_unit = $rootScope.context_ruas_jalan.id_unit;
			$scope.FormData.id_ppk = $rootScope.context_ruas_jalan.id_ppk;
			$scope.get_ppk();
		}
	}

	if($rootScope.route[1]==="tipe_pembangunan"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Tipe Pembangunan";
			$scope.edittipe = function(data){
				$rootScope.context_tipe_pembangunan = data;
				$location.path('master_data/tipe_pembangunan/edit');
			}
			$http.post(site_url('master_data/tipe_search'),{}).success(function(data){
				if(data.status){
				$scope.tipe = data.data;
				$resource(data.data).query().$promise.then(function(persons) {
                    vm.tipe = persons;
                });
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.hapustipe = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus tipe pembangunan "+_data.nama_tipe+"?");
				if(yes){
					$http.post(site_url('master_terima/hapus_tipe_pembangunan'),{
						ID : _data.ID,
						nama_tipe : _data.nama_tipe,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
		$scope.get_kota = function(){
			$http.post(site_url('master_data/kota'),{
				id_provinsi : $scope.FormData.id_provinsi
			}).success(function(data){
				if(data.status){
				$scope.kota = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		$scope.submittipex = function(){
			$http.post(site_url('master_data/submit_tipe_x'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/tipe_pembangunan")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Tipe Pembangunan";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Tipe Pembangunan";
			if(typeof $rootScope.context_tipe_pembangunan === "undefined") $location.path("master_data/tipe_pembangunan");
			$scope.FormData.ID = $rootScope.context_tipe_pembangunan.ID;
			$scope.FormData.nama_tipe = $rootScope.context_tipe_pembangunan.nama_tipe;
			//$scope.get_kota();
		}
	}

});