angular.module('koppas').controller('bts_wrap)tps',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.tiang_bts = [];
	$scope.frx = [];
	$http.post(site_url('master_data/bts'),{}).success(function(data){
	if(data.status){
		$scope.tiang_bts = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	if($rootScope.route[1]==="bts"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data BTS";
			return;
		}
		$scope.submitbts = function(){
			$http.post(site_url('master_data/submit_bts'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/bts")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Wrap BTS";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit BTS";
			if(typeof $rootScope.context === "undefined") $location.path("master_data/wrap_bts");
			$scope.FormData.kdDep = $rootScope.context_bts.kdDep;
			$scope.Desa = $rootScope.context_bts.Desa;
			$scope.nprov = $rootScope.context_bts.nprov;
			$scope.sumber_data = $rootScope.context_bts.sumber_data;
			// $scope.FormData.hash_id = $rootScope.context_running.hash_id;
			// $scope.FormData.ket = $rootScope.context_running.nama_unit;
		}
	}
	
});
angular.module('koppas').controller('bts_wrap_single',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.provinsi = [];
	$scope.list_bts = [];
	$http.post(site_url('masterController/prov_data'),{}).success(function(data){
	if(data.status){
		$scope.provinsi = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	$scope.get_kab = function(){
		$http.post(site_url('masterController/kab_data'),{
			ID : $scope.FormData.id_provinsi
		}).success(function(data){
		if(data.status){
			$scope.kabupaten = data.data;
		} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
	$scope.get_kec = function(){
		$http.post(site_url('masterController/kec_data'),{
			ID : $scope.FormData.id_kabupaten
		}).success(function(data){
		if(data.status){
			$scope.kecamatan = data.data;
		} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
	$scope.get_desa = function(){
		$http.post(site_url('masterController/desa_data'),{
			ID : $scope.FormData.id_kecamatan
		}).success(function(data){
		if(data.status){
			$scope.desa = data.data;
		} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
	$scope.check_freq = function(){
		$http.post(site_url('master_data/cek_data_bts'),{
			ID : $scope.FormData.id_desa
		}).success(function(data){
		if(data.status){
			$scope.fre = data.data;
			//$scope.FormData.freq = data.data.freq;
		} else { errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
	$scope.list_freq = function(){
		$http.post(site_url('master_data/list_data_bts'),$scope.FormData).success(function(data){
		if(data.status){
			$scope.list_bts = data.data;
			$location.path("master_data/bts_single_list");
			//$scope.FormData.freq = data.data.freq;
		} else { errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	}
	$scope.submitbts = function(){
			$http.post(site_url('master_data/submit_bts'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/bts_single")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}	
	if($rootScope.route[1]==="bts_single"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data BTS";
			return;
		}
		
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Wrap BTS";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit BTS";
			if(typeof $rootScope.context === "undefined") $location.path("master_data/wrap_bts");
			$scope.FormData.kdDep = $rootScope.context_bts.kdDep;
			$scope.Desa = $rootScope.context_bts.Desa;
			$scope.nprov = $rootScope.context_bts.nprov;
			$scope.sumber_data = $rootScope.context_bts.sumber_data;
			// $scope.FormData.hash_id = $rootScope.context_running.hash_id;
			// $scope.FormData.ket = $rootScope.context_running.nama_unit;
		}
	}
	
});
angular.module('koppas').controller('bts_wrap_list',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.list = [];
	$scope.frx = [];
	$http.post(site_url('master_data/list_data_bts'),{}).success(function(data){
	if(data.status){
		$scope.list = data.data;
//		pv_data(data.data.bts.id_desa);
		//Inline
		//  $http.post(site_url('master_data/get_fails'),{ID: data.data.bts.id_desa}).success(function(data){
  //           if(data.status){
  //           	errorToastAuto(val);
  //               $scope.frx = data.data;
  //           } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Ruas');});
		// //End LIne
	//	$scope.frx = data.data.prov;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	$scope.pv_data = function(arg){
        $http.post(site_url('master_data/get_fails'),{ID: arg}).success(function(data){
            if(data.status){
            	//errorToastAuto(val);
                $scope.frx = data.data;
                console.log('Selected Item:', data.data);
                console.log(val);
            } else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection Ruas');});
    }

	if($rootScope.route[1]==="bts"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data BTS";
			return;
		}
		$scope.submitbts = function(){
			$http.post(site_url('master_data/submit_bts'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/bts")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Wrap BTS";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit BTS";
			if(typeof $rootScope.context === "undefined") $location.path("master_data/wrap_bts");
			$scope.FormData.kdDep = $rootScope.context_bts.kdDep;
			$scope.Desa = $rootScope.context_bts.Desa;
			$scope.nprov = $rootScope.context_bts.nprov;
			$scope.sumber_data = $rootScope.context_bts.sumber_data;
			// $scope.FormData.hash_id = $rootScope.context_running.hash_id;
			// $scope.FormData.ket = $rootScope.context_running.nama_unit;
		}
	}
	
});