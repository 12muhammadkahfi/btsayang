angular.module('koppas').controller('mst',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.instruksi = [];
	$scope.kota = [];
	$scope.ruas_jalan = [];
	$scope.tipe_pembangunan = [];
	$scope.FormData = {};
	$scope.files = [];
	$scope.files_success = {name:[],size:[],type:[],remark:[]};
	$scope.onUpload = false;
	$scope.progressPercentage = 0;
	
	$http.post(site_url('master_data/instruksi'),{}).success(function(data){
	if(data.status){
		$scope.instruksi = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	if($rootScope.route[1]==="instruksi"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Instruksi";
			
			return;
		}


		$scope.submitinstruksi = function(){
			
			//var ket : CKEDITOR.replace($scope.FormData.ket);
			$http.post(site_url('master_data/submit_instruksi'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/instruksi")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Instruksi";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Insturksi";
			if(typeof $rootScope.context_instruksi === "undefined") $location.path("master_data/instruksi");
			$scope.FormData.ID = $rootScope.context_instruksi.ID;
			$scope.FormData.ket = $rootScope.context_instruksi.ket;
			
		}
	}
	if($rootScope.route[1]==="upload"){
		if($rootScope.route.length==2){
		$scope.delattach = function(index){
        var yes = $window.confirm("Anda yakin akan menghapus lampiran :\n\n"+$scope.files_success.name[index]);
        if(yes){
            delete $scope.files_success.size.splice(index,1);
            delete $scope.files_success.name.splice(index,1);
            delete $scope.files_success.type.splice(index,1);
            delete $scope.files_success.remark.splice(index,1);
        	}
    	}
    	$scope.uploadFiles = function (files) {

      if (files && files.length) {
        $scope.onUpload = true;
		$timeout(function(){$rootScope.NowLoading = false;},10);
		$timeout(function(){$rootScope.NowLoading = false;},50);
		$timeout(function(){$rootScope.NowLoading = false;},350);
		$timeout(function(){$rootScope.NowLoading = false;},1000);
		Upload.upload({url: 'index.php/perijinan/uploadfiles',data: {file: files}}).then(function (resp) {
            if($scope.files_success.name.length==0){
                $scope.files_success.name = resp.data.file.name;
                $scope.files_success.size = resp.data.file.size;
                $scope.files_success.type = resp.data.file.type;
                angular.forEach($scope.files_success.name, function(value, index){
                    $scope.files_success.remark.push('');
                });
            } else {
                angular.forEach(resp.data.file.name, function(value, index){
                    if($scope.files_success.name.indexOf(value)<0){
                        $scope.files_success.name.push(value);
                        $scope.files_success.size.push(resp.data.file.size[index]);
                        $scope.files_success.type.push(resp.data.file.type[index]);
                        $scope.files_success.remark.push('');
                    }
                });
            }
            console.log($scope.files_success);
        }, function (resp) {
            console.log();
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            if(parseInt($scope.progressPercentage)==100){
				$timeout(function(){
					$scope.onUpload = false;
				},350);
			}
		});;
      }
    }
	}
		$scope.submitupload = function(){
		
			if($scope.files_success.name.length<1){
            errorToastAuto('Anda harus mengupload setidaknya 1 lampiran terlebih dahulu.');
            return false;
        }
			$scope.FormData.attachments = [];
			angular.forEach($scope.files_success.name, function(value, index){
            $scope.FormData.attachments.push({
                name : value,
                description : $scope.files_success.remark[index]
            	});
        	});
			$http.post(site_url('master_data/submit_upload'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/upload")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Upload";
		}

	}


	if($rootScope.route[1]==="tipe_pembangunan"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Tipe Pembangunan";
			$scope.edittipe = function(data){
				$rootScope.context_tipe_pembangunan = data;
				$location.path('master_data/tipe_pembangunan/edit');
			}
			$http.post(site_url('master_data/tipe_search'),{}).success(function(data){
				if(data.status){
				$scope.tipe = data.data;
				$resource(data.data).query().$promise.then(function(persons) {
                    vm.tipe = persons;
                });
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.hapustipe = function(_data){
				var yes = $window.confirm("Anda yakin akan menghapus tipe pembangunan "+_data.nama_tipe+"?");
				if(yes){
					$http.post(site_url('master_data/hapus_tipe'),{
						id_tipe : _data.id_tipe,
						nama_tipe : _data.nama_tipe,
						status : 0
					}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
		$scope.get_kota = function(){
			$http.post(site_url('master_data/kota'),{
				id_provinsi : $scope.FormData.id_provinsi
			}).success(function(data){
				if(data.status){
				$scope.kota = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		$scope.submittipe = function(){
			$http.post(site_url('master_data/submit_tipe'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/tipe_pembangunan")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Tipe Pembangunan";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Tipe Pembangunan";
			if(typeof $rootScope.context_tipe_pembangunan === "undefined") $location.path("master_data/tipe_pembangunan");
			$scope.FormData.id_tipe = $rootScope.context_tipe_pembangunan.id_tipe;
			$scope.FormData.nama_tipe = $rootScope.context_tipe_pembangunan.nama_tipe;
			//$scope.get_kota();
		}
	}

});