angular.module('koppas').controller('tpl_realisasibts',function($scope,$rootScope,$location,$http,$filter,$window,$route,$sce){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.realbts = [];
	$scope.FormData = {};
	$http.post(site_url('tpl_controller/realisasi_bts'),{}).success(function(data){
	if(data.status){
		$scope.realbts = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	
});
angular.module('koppas').controller('tpl_realisasibts_dagri',function($scope,$rootScope,$location,$http,$filter,$window,$route,$sce){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.realbts_dagri = [];
	$scope.FormData = {};
	$http.post(site_url('tpl_controller/realisasi_bts_kemendagri'),{}).success(function(data){
	if(data.status){
		$scope.realbts_dagri = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	
});