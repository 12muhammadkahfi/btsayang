angular.module('koppas').controller('penjualan_dan_jasa',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	$rootScope.page_title = "Penjualan Dan Jasa";
	var route = $location.path().substring(1).split('/');
	$scope.route = route;
	$scope.FormData = {};
	$scope.kategori_barang = [];
	$scope.data_barang = [];
	$http.post(site_url('penjualan_dan_jasa/kategori_barang'),{}).success(function(data){
		if(data.status){
		$scope.kategori_barang = data.data;
		$http.post(site_url('penjualan_dan_jasa/data_barang'),{}).success(function(data){
			if(data.status){
			angular.forEach(data.data, function(value, index){
				console.log(value, index);
				data.data[index].custom_title = $rootScope.idbarang(value.ID) +" - "+value.nama_barang;
			});
			$scope.data_barang = data.data;
		} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	if(route[1]==="kategori_barang"){
		if(route.length==2){
			$rootScope.page_title = "Kategori Barang";
			$scope.hapuskategori = function(kategori){
				var yes = $window.confirm("Anda yakin akan menghapus kategori "+kategori.nama_kategori+"?");
				if(yes){
					$http.post(site_url('penjualan_dan_jasa/hapus_kategori_barang'),{hash_id:kategori.hash_id}).success(function(data){
						if(data.status){
						successToastAuto(data.message);
						$route.reload();
					} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
				}
			}
			return;
		}
		if(route[2]==="tambah"){
			$scope.form_title = 'Buat Kategori Baru';
			$scope.submitkategoribarang = function(){
				$http.post(site_url('penjualan_dan_jasa/submit_kategori_barang'),$scope.FormData).success(function(data){
					if(data.status){
						successToastAuto(data.message);
						$location.path("penjualan_dan_jasa/kategori_barang");
				} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			}
		}
		if(route[2]==="edit"){
			$scope.form_title = 'Edit Kategori';
			$http.post(site_url('penjualan_dan_jasa/detail_kategori_barang'),{hash_id:route[3]}).success(function(data){
				if(data.status){
				$scope.FormData.nama_kategori = data.data.nama_kategori;
				$scope.FormData.remark = data.data.remark;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.submitkategoribarang = function(){
				$scope.FormData.hash_id = route[3];
				$http.post(site_url('penjualan_dan_jasa/submit_kategori_barang'),$scope.FormData).success(function(data){
					if(data.status){
						successToastAuto(data.message);
						$location.path("penjualan_dan_jasa/kategori_barang");
				} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			}
		}
	}
	if(route[1]==="data_barang"){
		if(route.length==2){
			$rootScope.page_title = "Master Data Barang";
			$scope.hapusbarang = function(barang){
				if(parseInt(barang.stock)>0){
					errorToastAuto("Tidak diijinkan menghapus data barang "+barang.nama_barang+" karena masih tersisa stock sebanyak "+barang.stock+" unit.");
				} else {
					var yes = $window.confirm("Anda yakin akan menghapus barang "+barang.nama_barang+"?");
					if(yes){
						$http.post(site_url('penjualan_dan_jasa/hapus_data_barang'),{hash_id:barang.hash_id}).success(function(data){
							if(data.status){
							successToastAuto(data.message);
							$route.reload();
						} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
					}
				}
			}
			return;
		}
		if(route[2]==="tambah"){
			$rootScope.page_title = "Tambah Data Barang";
			$scope.submitdatabarang = function(){
				$http.post(site_url('penjualan_dan_jasa/submit_data_barang'),$scope.FormData).success(function(data){
					if(data.status){
					successToastAuto(data.message);
					$location.path("penjualan_dan_jasa/data_barang");
				} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			}
		}
		if(route[2]==="edit"){
			$rootScope.page_title = "Edit Data Barang";
			$http.post(site_url('penjualan_dan_jasa/detail_data_barang'),{hash_id:route[3]}).success(function(data){
				if(data.status){
				$scope.FormData.nama_barang = data.data.nama_barang;
				$scope.FormData.id_kategori = data.data.id_kategori;
				$scope.FormData.hpp = data.data.hpp;
				$scope.FormData.stock = parseInt(data.data.stock);
				$scope.FormData.remark = data.data.remark;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			$scope.submitdatabarang = function(){
				$scope.FormData.hash_id = route[3];
				$http.post(site_url('penjualan_dan_jasa/submit_data_barang'),$scope.FormData).success(function(data){
					if(data.status){
					successToastAuto(data.message);
					$location.path("penjualan_dan_jasa/data_barang");
				} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			}
		}
	}
	if(route[1]==="beli_barang"){
		if(route.length==2){
			$rootScope.page_title = "Data Pembelian Barang";
			$scope.pembelian_barang = [];
			$http.post(site_url('penjualan_dan_jasa/pembelian_barang')).success(function(data){
				if(data.status){
				$scope.pembelian_barang = data.data;
			} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			return;
		}
		if(route[2]==="tambah"){
			$rootScope.page_title = "Pembelian Barang";
			$scope.submitpembelianbarang = function(){
				if(typeof $scope.anguSelected === "undefined"){
					errorToastAuto("Anda belum memilih barang yang akan didata");
					return;
				}
				$scope.FormData.id_barang = $scope.anguSelected.originalObject.ID;
				$http.post(site_url('penjualan_dan_jasa/submit_pembelian_barang'),$scope.FormData).success(function(data){
					if(data.status){
					successToastAuto(data.message);
					$location.path("penjualan_dan_jasa/beli_barang");
				} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
			}
		}
	}
});