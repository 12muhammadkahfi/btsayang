angular.module('koppas').controller('mdy',function($scope,$rootScope,$location,$http,$filter,$window,$route){
	var route = $location.path().substring(1).split('/');
	$rootScope.route = route;
	$scope.FormData = {};
	$scope.running = [];
	$http.post(site_url('master_data/running'),{}).success(function(data){
	if(data.status){
		$scope.running = data.data;
	} else {errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
	if($rootScope.route[1]==="running"){
		if($rootScope.route.length==2){
			$rootScope.page_title = "Data Running";
			return;
		}
		$scope.submitrunning = function(){
			$http.post(site_url('master_data/submit_running'),$scope.FormData).success(function(data){
				if(data.status){
				successToastAuto(data.message);
				$location.path("master_data/running")
			} else {console.log(data);errorToastAuto(data.message)}}).error(function(error){alert('Unknown error, maybe this caused by missing internet connection');});
		}
		if($rootScope.route[2]==="tambah"){
			$rootScope.page_title = "Tambah Running Teks";
		}
		if($rootScope.route[2]==="edit"){
			$rootScope.page_title = "Edit Running";
			if(typeof $rootScope.context === "undefined") $location.path("master_data/running");
			$scope.FormData.hash_id = $rootScope.context_running.hash_id;
			$scope.FormData.ket = $rootScope.context_running.nama_unit;
		}
	}
	
});