﻿# Host: localhost  (Version 5.5.5-10.1.35-MariaDB)
# Date: 2018-11-05 00:35:51
# Generator: MySQL-Front 6.1  (Build 1.23)


#
# Structure for table "pcr_menus"
#

DROP TABLE IF EXISTS `pcr_menus`;
CREATE TABLE `pcr_menus` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `level` int(1) NOT NULL DEFAULT '5',
  `parent` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

#
# Data for table "pcr_menus"
#

INSERT INTO `pcr_menus` VALUES (1,'Dashboard','#/',5,0,0),(2,'Master Data','',3,0,0),(3,'Data Entitas','#/master_data/kawil_aceh',3,2,0),(4,'Data BTS Wrap','#/master_data/bts_wrap',1,2,0),(5,'Data BTS Single','#/master_data/bts_single',1,2,0),(6,'Configuration','',1,0,0),(7,'Configuration Header','',1,6,0),(8,'Configuration Remark History','',1,0,0),(9,'Administrator','#/administrator',1,0,0),(10,'Profil','#/profil',5,0,0),(15,'Transaction','#',3,0,0),(19,'Data Perijinan','#/data_perijinan',5,0,0),(21,'Reporting','',1,0,0),(22,'Report Counting Entitas','#/report_data/count_entitas',1,21,0),(23,'Report Summary Entitas','#/report_data/summary_entitas',1,21,0),(24,'Report Log History','#/report_data/history',1,21,0),(25,'Report Wraper BTS','',1,21,0),(26,'Report Signal Desa','',1,21,0),(27,'Data Lokasi Desa','',1,2,0),(28,'Data Lokasi BTS','',1,2,0),(29,'Data Signal','',1,2,0),(30,'Master Provinsi','#/master_data/listprovinsi',3,2,0),(31,'Master Kabupaten','#/master_data/listkabupaten',3,2,0),(33,'Master Kecamatan','#/master_data/listkecamatan',3,2,0),(34,'Master Kelurahan','#/master_data/listkelurahan',3,2,0),(36,'Realisasi BTS','#/transaction/real_bts',3,15,0),(37,'Planning BTS','#/transaction/plan_bts',5,15,0),(38,'Wrap Realisasi BTS','#/transaction/real_btswrap',5,15,0),(39,'Wrap Planning BTS','#/transaction/plan_btswrap',5,15,0),(40,'Pengguna','#/pengguna',1,0,0);
