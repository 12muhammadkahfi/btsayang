﻿# Host: localhost  (Version 5.5.5-10.1.35-MariaDB)
# Date: 2018-11-05 00:36:39
# Generator: MySQL-Front 6.1  (Build 1.23)


#
# Structure for table "pcr_user"
#

DROP TABLE IF EXISTS `pcr_user`;
CREATE TABLE `pcr_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `level` int(1) NOT NULL DEFAULT '5',
  `created_time` datetime NOT NULL,
  `pin` int(8) NOT NULL DEFAULT '0',
  `blacklist` int(1) NOT NULL DEFAULT '0',
  `forgot_password` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

#
# Data for table "pcr_user"
#

INSERT INTO `pcr_user` VALUES (2,'mktree@hotmail.com','7cff4c14e19b5db65c448b46acc72492','Super Admin',1,'0000-00-00 00:00:00',0,0,''),(61,'12.muhammadkahfi@gmail.com','2425c876b23150c248d6a3c79e3efc0b','SIAPA ANADA',5,'0000-00-00 00:00:00',0,0,''),(62,'mail.mksgroup@gmail.com','deac17bbcbd56b118bdd3e20bd4e211e','Super MKTREE',1,'2018-08-06 01:15:33',0,0,''),(67,'kahfi.sahroni@gmail.com','2425c876b23150c248d6a3c79e3efc0b','Muhammad Kahfi Sahroni',1,'2018-10-16 01:37:43',0,0,'');
